# Build LASK

Here are the instructions to build LASK.

## Requirements

### build
* CMAKE
* vala
* gcc
* gtk+-3.0-dev
* sqlite3-dev
* libarchive-dev
* shapelib-dev

### run
* gtk+-3.0
* sqlite3
* libarchive
* shapelib

## Build
in the main directory of the sources type:
    cd build
    cmake ..
    make

## Install    
    make install

