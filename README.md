### LASK ist eine Ackerschlagkartei für Linux.

Es dient zum Dokumentieren von Arbeiten auf landwirtschaftlichen Feldern.  
* Dabei könne auch mehrere Betriebe verwaltet werden.
* Jeder Betrieb kann beliebig viele und beliebig große Felder enthalten.
* Es können Saat, Bodenbearbeitung, Pflanzenschutz, Düngung und Ernte als Aktionen verwaltet werden.
* Als Betriebsmittel können Sattgut, Pflanzenschutzmittel, organische und mineralische Dünger, Maschinen und Personen verwaltet werden.
* Zu jeder Aktion werden die Kosten zur Auswertung erfasst.
* Alle Daten könne zur schriftlichen Dokumentation ausgedruckt werden.
* Der Import von Feldern aus [iBALIS](https://www.stmelf.bayern.de/ibalis/) ist möglich(nur Bayern).
* Für die mobile Dateneingabe während der Arbeit gibt es mit [LASKmobile](https://codeberg.org/ibinshoid/LASKmobile) eine App für
  Android-Handys.



Binärpakete für verschiedene Distributionen stehen unter:  
http://software.opensuse.org/download?package=lask&project=home:ihoid  
zum Download bereit.


<img src="https://codeberg.org/attachments/0748f66e-eeef-4bf1-bdc5-6b0c457ac720" width="30%"></img> <img src="https://codeberg.org/attachments/556a06dc-5b39-49c4-85ed-21195a5de345" width="30%"></img>
