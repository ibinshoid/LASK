Beim Starten von LASK erscheint als erstes das Fenster zum Verwalten der Betriebe.  
Links werden die Betriebe angezeigt, rechts Schaltflächen zum Öffnen, Anlegen oder Löschen von Betrieben. Mit Abbrechen wird das Programm beendet.  
Wenn noch kein Betrieb angelegt wurde wird nur ein Musterbetrieb angezeigt.  

* **Öffnen:** Der links ausgewählte Betrieb wid geladen und das [Hauptfenster](Hauptfenster) angezeigt. Wenn ein Betrieb zum ersten mal geöffnet wird erscheint ein Dialog zum Anlegen eines Erntejahres. Ein Doppelklick auf den Betrieb geht auch.
* **Neu anlegen:** Es erscheint Ein Dialog zum Anlegen eines Neuen Betriebes. Siehe auch [Betrieb anlegen](Betrieb-anlegen).
* **Löschen:** Der links ausgewählte Betrieb wird nach einem Bestätigungsdialog gelöscht. Dabei werden alle Daten des Betriebes unwiederbringlich gelöscht!
  



