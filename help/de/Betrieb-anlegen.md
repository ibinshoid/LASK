Wenn im [Startfenster](Startfenster) auf die Schaltfläche "**Neu anlegen**" geklickt wird erscheint dieses Fenster.

Zuerst müssen der Name und eine Betriebsnummer eingegeben werden.  
Der Name wird auch als Dateiname mit der Endung ".lask" verwendet.  
Es kann auch die Adresse des Betriebes angegeben werden.  
Diese Daten können später im Menüpunkt "**Betrieb|Betriebsdaten**" geändert werden.  

Nach dem Klicken auf "**Weiter**" kann ausgewählt werden wie der Betrieb angelegt werden soll:
* **Leeren Betieb anlegen:** Es werden keine Felder angelegt. Diese können später im Menüpunkt "**Betrieb|Felder verwalten**" angelegt
  werden.
* **Bestehenden Betrieb importieren:** Es werden die Felder von einer bestehenden "*.lask" Datei importiert.
* **Felder von iBALIS importieren:** Es werden die Felder aus der iBALIS Exportdatei "feldsücke.zip" importiert.
  (iBALIS ist nur in Bayern verfügbar)
  
Beim Klicken auf "**Anwenden**" kommt bei den beiden letzten Punkten ein Datei Dialog in dem die "*.lask" oder "feldstücke.zip" Datei geladen werden kann. Der Import kann einige Zeit dauern.  

Danach erscheint wieder das [Startfenster](Startfenster) mit dem neuen Betrieb. Wenn der Betrieb zum ersten mal geöffnet wird erscheint ein Dialog zum Anlegen eines Erntejahres.
