Wenn im [Hauptfenster](Hauptfenster) beim Aufklappmenü unter den Aktionen eine Aktionsart gewählt wird erscheint dieses Fenster zum Anlegen einer neuen Aktion.    

Wenn im [Hauptfenster](Hauptfenster) der Knopf "**Ändern**" gewählt wird oder ein Doppelklick auf eine Aktion gemacht wird erscheint dieses Fenster zum Ändern der aktuellen Aktion.


