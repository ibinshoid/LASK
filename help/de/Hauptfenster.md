Wenn im [Startfenster](Startfenster) der Knopf "**Öffnen**" gewählt wird  oder ein Doppelklick auf einen Betrieb gemacht wird erscheint dieses Fenster.

Links ist eine Liste mit allen Feldern, Diese kann oben gefiltert werden. 
Wenn ein Feld ausgewählt ist werden in der Mitte die Aktionen dazu angezeigt. 
Wenn eine Aktion ausgewählt wird werden darunter Informationen dazu angezeigt. 
Unter der Liste mit den Aktionen können Aktionen angelegt, geändert oder gelöscht werden. 


