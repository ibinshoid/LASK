Wenn im [Hauptfenster](Hauptfenster) der Menüpunkt "**Betrieb|Felder verwalten**" gewählt wird erscheint dieses Fenster.    

Auf der linken Seite werden die einzelnen Felder angezeigt

* **Neu:** Es erscheint ein Fenster zum Anlegen eines neuen Feldes.
* **Ändern:** Es erscheint ein Fenster zum Ändern des links ausgewählten Feldes. Es kann auch ein Doppelklick auf ein Feld im [Hauptfenster](Hauptfenster) gemacht werden.
* **Löschen:** Das links ausgewählte Feld wird nach einem Bestätigungsdialog gelöscht. Dabei werden alle Daten des Feldes uder seiner Aktionen unwiederbringlich gelöscht!
