Wenn im [Hauptfenster](Hauptfenster) der Menüpunkt "**Mobilgerät|Betrieb anlegen**" gewählt wird nach einem Mobilgerät gesucht, das über USB angeschlossen ist und auf dem LASKmobile installiert ist.
Bei Erfolg wird nach einem Bestätigungsdialog der aktuelle Betrieb auf das Mobilgerät kopiert.

Wenn im [Hauptfenster](Hauptfenster) der Menüpunkt "**Mobilgerät|Betrieb abgleichen**" gewählt wird nach einem Mobilgerät gesucht, das über USB angeschlossen ist und auf dem LASKmobile installiert ist.
Bei Erfolg und wenn auf dem Mobilgerät der aktuelle Betrieb installiert ist, erscheint ein Fenster zum Abgleich.
Hier werden die auf dem Mobilgerät eingegebenen Aktionen angezeigt. Mit einem Dopelklick können einzelne Aktionen entfernt werden.
Nach dem Bestätigen werden die Daten abgeglichen.


    
