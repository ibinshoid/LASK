Wenn im [Hauptfenster](Hauptfenster) der Menüpunkt "**Betrieb|Mittel verwalten**" gewählt wird erscheint dieses Fenster.    

Auf der linken Seite kann oben die Art der Betriebsmittel gewählt werden.
Unten werden die einzelnen Mittel angezeigt

* **Neu:** Es erscheint ein Fenster zum Anlegen eines neuen Betriebsmittels.
* **Ändern:** Es erscheint ein Fenster zum Ändern des links ausgewählten Betriebsmittels. Bei Saatgut kann auch Daten zur Fruchtart geändert werden.
* **Löschen:** Das links ausgewählte Betriebsmittel wird nach einem Bestätigungsdialog gelöscht. Dabei werden alle Daten des Betriebsmittels unwiederbringlich gelöscht!

