## LASK ist eine Ackerschlagkartei für Linux.

Es dient zum Dokumentieren von Arbeiten auf landwirtschaftlichen Feldern.    
* Dabei könne auch mehrere Betriebe verwaltet werden.
* Jeder Betrieb kann beliebig viele und beliebig große Felder enthalten.
* Es können Saat, Bodenbearbeitung, Pflanzenschutz, Düngung und Ernte als Aktionen verwaltet werden.
* Auch Weidehaltung wird unterstützt
* Als Betriebsmittel können Sattgut, Pflanzenschutzmittel, organische und mineralische Dünger, Maschinen und Personen verwaltet werden.
* Zu jeder Aktion werden die Kosten zur Auswertung erfasst.
* Alle Daten könne zur schriftlichen Dokumentation ausgedruckt werden.
* Der Import von Feldern aus [iBALIS](https://www.stmelf.bayern.de/ibalis/) ist in Bayern möglich.
* Für die mobile Dateneingabe während der Arbeit gibt es mit [LASKmobile](http://codeberg.org/ibinshoid/laskmobile) eine App für Android-Handys.
