[CCode (lower_case_cprefix = "LIBMTP_", cheader_filename = "libmtp.h")]
namespace LibMTP {
	public static void Init();
	public static int Get_Supported_Devices_List (out DeviceEntry[] entries);
	public static int Detect_Raw_Devices (out RawDevice[] devices);
	
	public delegate bool ProgressFunc (uint64 sent, uint64 total);
	
	[CCode (cname = "LIBMTP_device_entry_t", has_copy_function = false, has_destroy_function = false)]
	public struct DeviceEntry {
		public string vendor;
		public ushort vendor_id;
		public string product;
		public ushort product_id;
		public ushort device_flags;
	}
	
	public static Device* Open_Raw_Device (RawDevice* raw);
	public static Device* Open_Raw_Device_Uncached (RawDevice* raw);
	
	[CCode (cname = "LIBMTP_raw_device_t", has_copy_function = false, has_destroy_function = false)]
	public struct RawDevice {
		DeviceEntry device_entry;
		public uint bus_location;
		public uint8 devnum;
	}
	
	[CCode (cname = "LIBMTP_mtpdevice_t", has_copy_function = false, has_destroy_function = false)]
	public struct Device {
		[CCode (cname = "LIBMTP_Get_Batterylevel")]
		public int get_battery_level (out uint8 cur, out uint8 total);
		[CCode (cname = "LIBMTP_Get_Storage")]
		public int get_storage (int sortby);
		[CCode (cname = "LIBMTP_Get_Filelisting_With_Callback")]
		public File get_file_listing (ProgressFunc func);
		[CCode (cname = "LIBMTP_Get_Files_And_Folders")]
		public File* get_files_and_folders(uint storage, uint parent);
		[CCode (cname = "LIBMTP_Get_Folder_List")]
		public Folder* get_folder_list();
		[CCode (cname = "LIBMTP_Get_Folder_List_For_Storage")]
		public Folder* get_folder_list_for_storage (uint id);
		[CCode (cname = "LIBMTP_Get_File_To_File")]
		public int get_file_to_file(uint id, string path, ProgressFunc? func);
		[CCode (cname = "LIBMTP_Send_File_From_File")]
		public int send_file_from_file(string path, File filedata, ProgressFunc? func);
		[CCode (cname = "LIBMTP_Set_File_Name")]
		public int set_file_name(File file, string newname);
		[CCode (cname = "LIBMTP_Delete_Object")]
		public int delete_object(uint32 id);
		[CCode (cname = "LIBMTP_Get_Tracklisting_With_Callback")]
		public Track* get_track_listing (ProgressFunc func);
		[CCode (cname = "LIBMTP_Get_Tracklisting_With_Callback_For_Storage")]
		public Track* get_track_listing_for_storage (uint id, ProgressFunc func);
		[CCode (cname = "LIBMTP_Set_Folder_Name")]
		public int set_folder_name (Folder folder, string name);
		[CCode (cname = "LIBMTP_Dump_Device_Info")]
		public void dump_device_info();
		[CCode (cname = "LIBMTP_Get_Modelname")]
		public string get_modelname();
        [CCode (cname = "LIBMTP_Release_Device")]
		public void release();
		
		public DeviceStorage* storage;
		
		public string device_version {
			[CCode (cname = "LIBMTP_Get_Deviceversion")]
			get;
		}
		public string friendly_name {
			[CCode (cname = "LIBMTP_Get_Friendlyname")]
			get;
		}
		public string serial_number {
			[CCode (cname = "LIBMTP_Get_Serialnumber")]
			get;
		}
		public string manufacturer_name {
			[CCode (cname = "LIBMTP_Get_Manufacturername")]
			get;
		}
		public string model_name {
			[CCode (cname = "LIBMTP_Get_Modelname")]
			get;
		}
	}
	
	[CCode (cname = "LIBMTP_devicestorage_t", has_copy_function = false, has_destroy_function = false)]
	public struct DeviceStorage {
		public uint id;
		public string StorageDescription;
		public string VolumeIdentifier;
		
		public DeviceStorage* next;
		public DeviceStorage* prev;
	}
	
	[CCode (cname = "LIBMTP_file_t", ref_function = "", unref_function = "LIBMTP_destroy_file_t")]
	public class File {
        [CCode (cname = "LIBMTP_new_file_t")]
        public File ();
		public uint item_id;
        public uint parent_id;
		public string filename;
		public uint64 filesize;
		public File? next;
        public FileType filetype;
	}
	
	public static Folder* new_folder_t();
	
	[CCode (cname = "LIBMTP_folder_t", has_copy_function = false, destroy_function = "LIBMTP_destroy_folder_t")]
	public struct Folder {
		public uint folder_id;
		public uint parent_id;
		public uint storage_id;
		public string name;
		public Folder* sibling;
		public Folder* child;
		
		[CCode (cname = "LIBMTP_Find_Folder")]
		public Folder* find (uint id);
	}
	
	public static Track* new_track_t();
	
	[CCode (cname = "LIBMTP_track_t", has_copy_function = false, destroy_function = "LIBMTP_destroy_track_t")]
	public struct Track {
		public uint item_id;
		public uint parent_id;
		public string title; /**< Track title */
		public string artist; /**< Name of recording artist */
		public string composer; /**< Name of recording composer */
		public string genre; /**< Genre name for track */
		public string album; /**< Album name for track */
		public string date; /**< Date of original recording as a string */
		public string filename; /**< Original filename of this track */
		public ushort tracknumber; /**< Track number (in sequence on recording) */
		public uint duration; /**< Duration in milliseconds */
		public uint samplerate; /**< Sample rate of original file, min 0x1f80 max 0xbb80 */
		public ushort nochannels; /**< Number of channels in this recording 0 = unknown, 1 or 2 */
		public uint wavecodec; /**< FourCC wave codec name */
		public uint bitrate; /**< (Average) bitrate for this file min=1 max=0x16e360 */
		public ushort bitratetype; /**< 0 = unused, 1 = constant, 2 = VBR, 3 = free */
		public ushort rating; /**< User rating 0-100 (0x00-0x64) */
		public uint usecount; /**< Number of times used/played */
		public uint64 filesize; /**< Size of track file in bytes */
		public time_t modificationdate; /**< Date of last alteration of the track */
		public FileType filetype; /**< Filetype used for the current track */
		public Track* next; /**< Next track in list or NULL if last track */	
	}
	
	[CCode (cname = "LIBMTP_filetype_t", cprefix = "LIBMTP_FILETYPE_")]
	public enum FileType {
		FOLDER,
		WAV,
		MP3,
		WMA,
		OGG,
		AUDIBLE,
		MP4,
		UNDEF_AUDIO,
		WMV,
		AVI,
		MPEG,
		ASF,
		QT,
		UNDEF_VIDEO,
		JPEG,
		JFIF,
		TIFF,
		BMP,
		GIF,
		PICT,
		PNG,
		VCALENDAR1,
		VCALENDAR2,
		VCARD2,
		VCARD3,
		WINDOWSIMAGEFORMAT,
		WINEXEC,
		TEXT,
		HTML,
		FIRMWARE,
		AAC,
		MEDIACARD,
		FLAC,
		MP2,
		M4A,
		DOC,
		XML,
		XLS,
		PPT,
		MHT,
		JP2,
		JPX,
		ALBUM,
		PLAYLIST,
		UNKNOWN;
		
		public bool IS_AUDIO();
		public bool IS_VIDEO();
		public bool IS_AUDIOVIDEO();
		public bool IS_TRACK();
		public bool IS_IMAGE();
	}
	
    [CCode (cname = "LIBMTP_error_number_t", cprefix = "LIBMTP_ERROR_")]
	public enum Error {
        NONE,
        GENERAL,
        PTP_PLAYER,
        USB_PLAYER,
        MEMORY_ALLOCATION,
        NO_DEVICE_ATTACHED,
        STORAGE_FULL,
        CONNECTING,
        CANCELLED
    }
}
