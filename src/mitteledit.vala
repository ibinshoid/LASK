using Gtk;
using lask;

public class cmittelEdit : GLib.Object{
//Betriebsdaten bearbeiten    
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window4;
    private Button button9;
    private Button button10;
    private Notebook notebook1;
    private Entry entry3;
    private Label label90;
    private ComboBoxText combobox2;
    private ComboBoxText combobox3;
    private ComboBoxText combobox5;
    private SpinButton spinbutton1;
    private SpinButton spinbutton2;
    private SpinButton spinbutton3;
    private SpinButton spinbutton4;
    private SpinButton spinbutton5;
    private SpinButton spinbutton6;
    private SpinButton spinbutton7;
    private SpinButton spinbutton8;
    private SpinButton spinbutton9;
    private SpinButton spinbutton10;
    private SpinButton spinbutton11;
    private SpinButton spinbutton12;
    private SpinButton spinbutton13;
    private SpinButton spinbutton49;
    private Entry entryMittel1;
    private MainLoop loop = new MainLoop ();
   
    public cmittelEdit(){
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von mitteledit.vala geladen"),0);
        window4 = builder.get_object ("window4") as Window;
        button9 = builder.get_object ("button9") as Button;
        button10 = builder.get_object ("button10") as Button;
        notebook1 = builder.get_object ("notebook1") as Notebook;
        entry3 = builder.get_object ("entry3") as Entry;
        label90 = builder.get_object ("label90") as Label;
        combobox2 = builder.get_object ("combobox2") as ComboBoxText;
        combobox3 = builder.get_object ("combobox3") as ComboBoxText;
        combobox5 = builder.get_object ("combobox5") as ComboBoxText;
        spinbutton1 = builder.get_object ("spinbutton1") as SpinButton;
        spinbutton2 = builder.get_object ("spinbutton2") as SpinButton;
        spinbutton3 = builder.get_object ("spinbutton3") as SpinButton;
        spinbutton4 = builder.get_object ("spinbutton4") as SpinButton;
        spinbutton5 = builder.get_object ("spinbutton5") as SpinButton;
        spinbutton6 = builder.get_object ("spinbutton6") as SpinButton;
        spinbutton7 = builder.get_object ("spinbutton7") as SpinButton;
        spinbutton8 = builder.get_object ("spinbutton8") as SpinButton;
        spinbutton9 = builder.get_object ("spinbutton9") as SpinButton;
        spinbutton10 = builder.get_object ("spinbutton10") as SpinButton;
        spinbutton11 = builder.get_object ("spinbutton11") as SpinButton;
        spinbutton12 = builder.get_object ("spinbutton12") as SpinButton;
        spinbutton13 = builder.get_object ("spinbutton13") as SpinButton;
        spinbutton49 = builder.get_object ("spinbutton49") as SpinButton;
        entryMittel1 = builder.get_object ("entryMittel1") as Entry;
        this.window4.set_modal(true);
        //Ereignisse verbinden
        button9.clicked.connect(()=>{this.loop.quit();});
        button10.clicked.connect(()=>{this.window4.destroy();});
        combobox5.changed.connect(()=>{this.label90.set_text(_("Preis(€/") + this.combobox5.get_active_text() + ")");});
    }
    
    public void fensterBauen(int art){
    //Fenster bauen
        string tmp = "";

        this.notebook1.set_show_tabs(false);
        if (art == 0){
            //Saatgut
            this.window4.set_title(_("neues Saatgut"));
            this.label90.set_text(_("Preis(€/dt)"));
            this.spinbutton49.set_value(40);
            this.notebook1.set_current_page(1);
        }else if (art == 1){
            //Pflanzenschutzmittel
            this.window4.set_title(_("neues Pflanzenschutzmittel"));
            this.label90.set_text(_("Preis(€/Liter)"));
            this.spinbutton49.set_value(60);
            this.entryMittel1.set_text("");
            this.notebook1.set_current_page(0);
        }else if (art == 2){
            //organische Dünger
            this.window4.set_title(_("neuer organischer Dünger"));
            this.label90.set_text(_("Preis(€/m³)"));
            this.spinbutton49.set_value(1.00);
            this.notebook1.set_current_page(2);
            this.combobox5.append_text("m³");
            this.combobox5.append_text("to");
            this.combobox5.set_active(0);
        }else if (art == 3){
            //mineralische Dünger
            this.window4.set_title(_("neuer mineralischer Dünger"));
            this.label90.set_text(_("Preis(€/kg)"));
            this.spinbutton49.set_value(23);
            this.notebook1.set_current_page(2);
            this.combobox5.append_text("kg");
            this.combobox5.append_text("dt");
            this.combobox5.append_text("Liter");
            this.combobox5.set_active(0);
        }else if (art == 4){
            //Maschine
            this.window4.set_title(_("neue Maschine"));
            this.label90.set_text(_("Preis(€/ha)"));
            this.spinbutton49.set_value(40);
            this.notebook1.set_current_page(5);
        }else if (art == 5){
            //Person
            this.window4.set_title(_("neue Person"));
            this.label90.hide();
            this.spinbutton49.hide();
            this.notebook1.set_current_page(5);
        }else if (art == 6){
            //Fruchtart
            this.notebook1.set_current_page(3);
            this.label90.set_text(_("Kosten für Saat(€/ha)"));
        }
        //Fruchtarten bei Saatgut (Combobox3) füllen
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 0){
                if (m.par3 in tmp){}
                else{
                    tmp += m.par3;
                    this.combobox3.append_text(m.par3);
                }
            }
        }
                    
    }
    public mittel mittelNeu(int art){
    //Neues Betriebsmittel erzeugen
        mittel rueckMittel = mittel();
        mittel rueckMittel2 = mittel();
        
        fensterBauen(art);
        //rueckMittel bauen
        this.loop.run();
        rueckMittel.id = -1;
        rueckMittel.art = art;
        rueckMittel.name = this.entry3.get_text();
        rueckMittel.jahr = (int)(this.spinbutton49.get_value()*100);
        if(art == 0){
            //Saatgut
            if (this.combobox3.get_active_text() in mittelFenster.dbArten){}else{
                //Wenn Fruchtart noch nicht existiert
                rueckMittel2.id = -1;
                rueckMittel2.name = this.combobox3.get_active_text();
                rueckMittel2.art = 6;
                rueckMittel2.jahr = (int)(config.kosten.par4*100);
                rueckMittel2.par4 = 160;
                rueckMittel2.par5 = 70;
                rueckMittel2.par6 = 54;
                rueckMittel2.par7 = 18;
                rueckMittel2.par8 = 20;
                rueckMittel2.par9 = 15;
                laskDb.mittelHinzufuegen(rueckMittel2);
            }
            rueckMittel.par3 = this.combobox3.get_active_text();
            rueckMittel.par4 = this.spinbutton13.get_value();
            //Datenbankleichen verhindern
            if (rueckMittel.par3 == ""){rueckMittel.par3 = _("Sonstiges");}
        }else if(art == 1){
            //Pflanzenschutzmittel
            rueckMittel.par3 = this.combobox2.get_active_text();
            rueckMittel.par2 = this.entryMittel1.get_text();
        }else if(art == 2 ||art == 3){
            //Dünger
            rueckMittel.par3 = this.combobox5.get_active_text();
            rueckMittel.par4 = this.spinbutton1.get_value();
            rueckMittel.par5 = this.spinbutton2.get_value();
            rueckMittel.par6 = this.spinbutton3.get_value();
            rueckMittel.par7 = this.spinbutton4.get_value();
            rueckMittel.par8 = this.spinbutton5.get_value();
            rueckMittel.par9 = this.spinbutton6.get_value();
        }else if (art == 6){
            //Fruchtart
            rueckMittel.art = 6;
            rueckMittel.jahr = 4000;
            rueckMittel.par4 = this.spinbutton7.get_value();
            rueckMittel.par5 = this.spinbutton8.get_value();
            rueckMittel.par6 = this.spinbutton9.get_value();
            rueckMittel.par7 = this.spinbutton10.get_value();
            rueckMittel.par8 = this.spinbutton11.get_value();
            rueckMittel.par9 = this.spinbutton12.get_value();
        }
        this.window4.destroy();    
    return rueckMittel;
    }
    
    public mittel mittelAendern(mittel m){
    //Betriebsmittel bearbeiten
        mittel rueckMittel = mittel();
        mittel rueckMittel2 = mittel();
        Entry entry;
        
        fensterBauen(m.art);
        //Felder füllen
        this.entry3.set_sensitive(true);
        this.button9.set_label(_("Ändern"));
        this.entry3.set_text(m.name);
        this.spinbutton49.set_value(m.jahr/100);
        if(m.art == 0){
            //Saatgut
            this.window4.set_title(_("Saatgut ändern"));
            entry = this.combobox3.get_child() as Entry;
            entry.set_text(m.par3);
            this.spinbutton13.set_value(m.par4);
        }else if(m.art == 1){
            //Pflanzenschutzmittel
            this.window4.set_title(_("Pflanzenschutzmittel ändern"));
            entry = this.combobox2.get_child() as Entry;
            entry.set_text(m.par3);
            entryMittel1.set_text(m.par2);
        }else if(m.art == 2 ||m.art == 3){
            //Dünger
            this.window4.set_title(_("Dünger ändern"));
            entry = this.combobox5.get_child() as Entry;
            entry.set_text(m.par3);
            this.spinbutton1.set_value(m.par4);
            this.spinbutton2.set_value(m.par5);
            this.spinbutton3.set_value(m.par6);
            this.spinbutton4.set_value(m.par7);
            this.spinbutton5.set_value(m.par8);
            this.spinbutton6.set_value(m.par9);
        }else if(m.art == 4){
            //Maschine
            this.window4.set_title(_("Maschine ändern"));
        }else if(m.art == 5){
            //Person
            this.window4.set_title(_("Person ändern"));
        }else if(m.art == 6){
            //Fruchtart
            this.window4.set_title(_("Fruchtart ändern"));
            this.spinbutton7.set_value(m.par4);
            this.spinbutton8.set_value(m.par5);
            this.spinbutton9.set_value(m.par6);
            this.spinbutton10.set_value(m.par7);
            this.spinbutton11.set_value(m.par8);
            this.spinbutton12.set_value(m.par9);
        }
        //rueckMittel bauen
        this.loop.run();
        rueckMittel.id = m.id;
        rueckMittel.art = m.art;
        rueckMittel.name = this.entry3.get_text();
        rueckMittel.jahr = int.parse(doubleparse(this.spinbutton49.get_value()*100));
        if(m.art == 0){
            //Saatgut
            if (this.combobox3.get_active_text() in mittelFenster.dbArten){}else{
                //Wenn Fruchtart noch nicht existiert
                rueckMittel2.id = -1;
                rueckMittel2.name = this.combobox3.get_active_text();
                rueckMittel2.art = 6;
                rueckMittel2.jahr = 4000;
                rueckMittel2.par4 = 160;
                rueckMittel2.par5 = 70;
                rueckMittel2.par6 = 54;
                rueckMittel2.par7 = 18;
                rueckMittel2.par8 = 20;
                rueckMittel2.par9 = 15;
                laskDb.mittelHinzufuegen(rueckMittel2);
            }
            rueckMittel.par3 = this.combobox3.get_active_text();
            rueckMittel.par4 = this.spinbutton13.get_value();
            //Datenbankleichen verhindern
            if (rueckMittel.par3 == ""){rueckMittel.par3 = _("Sonstiges");}
        }else if(m.art == 1){
            //Pflanzenschutzmittel
            rueckMittel.par3 = this.combobox2.get_active_text();
            rueckMittel.par2 = this.entryMittel1.get_text();
        }else if(m.art == 2 ||m.art == 3){
            //Dünger
            rueckMittel.par3 = this.combobox5.get_active_text();
            rueckMittel.par4 = this.spinbutton1.get_value();
            rueckMittel.par5 = this.spinbutton2.get_value();
            rueckMittel.par6 = this.spinbutton3.get_value();
            rueckMittel.par7 = this.spinbutton4.get_value();
            rueckMittel.par8 = this.spinbutton5.get_value();
            rueckMittel.par9 = this.spinbutton6.get_value();
        }else if (m.art == 6){
            //Fruchtart
            rueckMittel.art = 6;
            rueckMittel.par4 = this.spinbutton7.get_value();
            rueckMittel.par5 = this.spinbutton8.get_value();
            rueckMittel.par6 = this.spinbutton9.get_value();
            rueckMittel.par7 = this.spinbutton10.get_value();
            rueckMittel.par8 = this.spinbutton11.get_value();
            rueckMittel.par9 = this.spinbutton12.get_value();
        }
        this.window4.destroy();    
    return rueckMittel;
    }
}

