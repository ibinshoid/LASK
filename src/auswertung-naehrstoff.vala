using Sqlite;
using Gtk;
using lask;
using Cairo;
using Pango;

public class causwertungNaehrstoff : causwertung{
    private CheckButton checkbutton1;
    private CheckButton checkbutton2;
    private CheckButton checkbutton3;
    private CheckButton checkbutton4;
    private ComboBoxText combobox20;

    private druckdaten[] druckfelder = {};
    private druckdaten druckfeld = druckdaten();
    
    public causwertungNaehrstoff(){
    //Konstructor
        this.window7.set_title(_("Auswertung Nährstoffe"));
        seitenLeiste();
        aendern();
        //Ereignisse verbinden
        checkbutton1.toggled.connect(aendern);
        checkbutton2.toggled.connect(aendern);
        checkbutton3.toggled.connect(aendern);
        checkbutton4.toggled.connect(aendern);
        combobox20.changed.connect(aendern);
    }

    public void seitenLeiste(){
    //rechte Seitenleiste bauen
        combobox20 = new ComboBoxText();
        combobox20.append_text(_("Alle"));
        combobox20.append_text(_("Ausgewählte"));
        combobox20.set_active(0);
        grid33.attach(new Label(_("Felder:")),0 ,0, 1, 1);
        grid33.get_child_at(0, 0).set_halign(Align.END);
        grid33.get_child_at(0, 0).set_hexpand(false);
        grid33.attach(new Label(_("Drucken:")),0 ,1, 1, 1);
        grid33.get_child_at(0, 1).set_halign(Align.END);
        grid33.get_child_at(0, 1).set_hexpand(false);
        grid33.attach(checkbutton1 = new CheckButton.with_label(_("Deckblatt")), 1, 1, 1, 1);
        checkbutton1.set_active(true);
        grid33.attach(new Gtk.Separator(Gtk.Orientation.HORIZONTAL), 0, 2, 2, 1);
        grid33.attach(checkbutton2 = new CheckButton.with_label(_("Felder")), 1, 3, 1, 1);
        checkbutton2.set_active(true);
        grid33.attach(checkbutton3 = new CheckButton.with_label(_("Summe")), 1, 4, 1, 1);
        checkbutton3.set_active(true);
        grid33.attach(checkbutton4 = new CheckButton.with_label(_("Summe Kalenderjahr")), 1, 5, 1, 1);
        checkbutton4.set_active(true);
    }
    
    public override void aendern(){
    //Auswertung neu malen
        feld[] felder = {};
        aktion[] aktionen = {};
        string art = "";
        
        druckfelder.length = 0;
        this.druckfelder.resize(0);
        this.seiten = 0;

        //Wenn deckblatt=0 Deckblatt malen sonst nicht
        if (this.checkbutton2.get_active() == false && this.checkbutton3.get_active() == false && this.checkbutton4.get_active() == false){
            this.checkbutton1.set_active(true);
        }
        if (this.checkbutton1.get_active()){
            this.deckblatt = 0;
        } else {
           this.deckblatt = 1;
           if (this.seite == 0){
                this.seite = 1;
            }
        }
        //Welche Felder sollen verwendet werden?
        if (this.combobox20.get_active() == 0){
            felder = felderliste;
        }else if  (this.combobox20.get_active() == 1){
            felder = aktFelderListe;
        }
        //Sollen einzelne Felder gedruckt werden?
        if (this.checkbutton2.get_active() == true){
            foreach (feld f in felder) {
                //Fruchtart ermitteln
                aktionen.resize(0);
                foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                    if(f.art == "Grünland"){
                        art = _("Grünland");
                    }else if(a.aktion == "Saat"){
                        if(a.par4 == 1){
                            f.frucht = a.par2;
                        }
                        art = a.par2;
                    }
                    if (a.aktion == "Ernte"){
                        a.par2 = art;
                        art = "";
                        a.par9 = a.par3;
                        a.par3 += int.parse(a.par1.split(";")[0]);
                        a.par4 += int.parse(a.par1.split(";")[1]);
                        a.par5 += int.parse(a.par1.split(";")[2]);
                    }
                    aktionen += a;
                }
                this.druckfeld.druckfeld = f;
                this.druckfeld.druckaktionen = aktionen;
                this.druckfelder += this.druckfeld;
            }
        }
        //Sollen Summen gedruckt werden?
        if (this.checkbutton3.get_active() == true){
            aktionen.resize(0);
            aktionen += aktion();
            aktionen += aktion();
            aktionen += aktion();
            this.druckfeld.druckfeld = feld();
            this.druckfeld.druckfeld.name = _("Summen");
            foreach (feld f in felder) {
                this.druckfeld.druckfeld.groesse += f.groesse;
                foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                    if(a.aktion == "Mineralische Düngung"){
                        if (a.par2 == "dt"){
                            a.par9 = a.par9 * 100;
                        }
                        aktionen[0].aktion = "Mineralische Düngung";
                        aktionen[0].flaeche = -1 ;
                        aktionen[0].par3 += a.par3 * a.par9 * a.flaeche;
                        aktionen[0].par4 += a.par4 * a.par9 * a.flaeche;
                        aktionen[0].par5 += a.par5 * a.par9 * a.flaeche;
                        aktionen[0].par6 += a.par6 * a.par9 * a.flaeche;
                        aktionen[0].par7 += a.par7 * a.par9 * a.flaeche;
                        aktionen[0].par8 += a.par8 * a.par9 * a.flaeche;
                        aktionen[0].par9 = 1;
                    }
                    if(a.aktion == "Organische Düngung"){
                        aktionen[1].aktion = "Organische Düngung";
                        aktionen[1].flaeche = -1 ;
                        aktionen[1].par3 += a.par3 * a.par9 * a.flaeche;
                        aktionen[1].par4 += a.par4 * a.par9 * a.flaeche;
                        aktionen[1].par5 += a.par5 * a.par9 * a.flaeche;
                        aktionen[1].par6 += a.par6 * a.par9 * a.flaeche;
                        aktionen[1].par7 += a.par7 * a.par9 * a.flaeche;
                        aktionen[1].par8 += a.par8 * a.par9 * a.flaeche;
                        aktionen[1].par9 = 1;
                    }
                    if(a.aktion == "Ernte"){
                        aktionen[2].aktion = "Ernte";
                        aktionen[2].flaeche = -1 ;
                        aktionen[2].par9 = a.par3;
                        aktionen[2].par3 += int.parse(a.par1.split(";")[0]) * a.flaeche;
                        aktionen[2].par4 += int.parse(a.par1.split(";")[1]) * a.flaeche;
                        aktionen[2].par5 += int.parse(a.par1.split(";")[2]) * a.flaeche;
                        aktionen[2].par6 += int.parse(a.par1.split(";")[3]) * a.flaeche;
                        aktionen[2].par7 += int.parse(a.par1.split(";")[4]) * a.flaeche;
                        aktionen[2].par8 += int.parse(a.par1.split(";")[5]) * a.flaeche;
                        aktionen[2].par1 = aktionen[2].par3.to_string() + ";" + aktionen[2].par4.to_string() + ";" + aktionen[2].par5.to_string() + ";" + aktionen[2].par6.to_string() + ";" + aktionen[2].par7.to_string() + ";" + aktionen[2].par8.to_string();
                    }
                }
            }
            this.druckfeld.druckaktionen = aktionen;
            this.druckfelder += this.druckfeld;
        }
        //Sollen Summen für Kalenderjahr wegen Düngeverordnung gedruckt werden?
        if (this.checkbutton4.get_active() == true){
            aktionen.resize(0);
            aktionen += aktion();
            aktionen += aktion();
            aktionen += aktion();
            this.druckfeld.druckfeld = feld();
            this.druckfeld.druckfeld.name = _("Summen Kalenderjahr ") + erntejahr.to_string();
            aktion[] tmpAktionen = {};
            foreach (aktion a in laskDb.aktionLesen("", erntejahr)){
                if(a.datum.get_year() == erntejahr){
                    tmpAktionen += a;
                }
            }
            foreach (aktion a in laskDb.aktionLesen("", erntejahr-1)){
                if(a.datum.get_year() == erntejahr){
                    tmpAktionen += a;
                }
            }
            foreach (aktion a in laskDb.aktionLesen("", erntejahr+1)){
                if(a.datum.get_year() == erntejahr){
                    tmpAktionen += a;
                }
            }
            foreach (aktion a in tmpAktionen){
                if(a.aktion == "Mineralische Düngung"){
                    if (a.par2 == "dt"){
                        a.par9 = a.par9 * 100;
                    }
                    aktionen[0].aktion = _("Mineralische Düngung");
                    aktionen[0].flaeche = -1 ;
                    aktionen[0].par3 += a.par3 * a.par9 * a.flaeche;
                    aktionen[0].par4 += a.par4 * a.par9 * a.flaeche;
                    aktionen[0].par5 += a.par5 * a.par9 * a.flaeche;
                    aktionen[0].par6 += a.par6 * a.par9 * a.flaeche;
                    aktionen[0].par7 += a.par7 * a.par9 * a.flaeche;
                    aktionen[0].par8 += a.par8 * a.par9 * a.flaeche;
                    aktionen[0].par9 = 1;
                }else if(a.aktion == "Organische Düngung"){
                    aktionen[1].aktion = _("Organische Düngung");
                    aktionen[1].flaeche = -1 ;
                    aktionen[1].par3 += a.par3 * a.par9 * a.flaeche;
                    aktionen[1].par4 += a.par4 * a.par9 * a.flaeche;
                    aktionen[1].par5 += a.par5 * a.par9 * a.flaeche;
                    aktionen[1].par6 += a.par6 * a.par9 * a.flaeche;
                    aktionen[1].par7 += a.par7 * a.par9 * a.flaeche;
                    aktionen[1].par8 += a.par8 * a.par9 * a.flaeche;
                    aktionen[1].par9 = 1;
                }
             }
            this.druckfeld.druckaktionen = aktionen;
            this.druckfelder += this.druckfeld;
        }
        this.seiten = this.druckfelder.length;
        if (this.seite > this.seiten){
            this.seite = this.seiten;
        }
		this.drawingarea1.queue_draw();
    }

    public override Cairo.Context draw_page(int breite, int page_nr, Cairo.Context context){
    //Seite zusammenbauen Cairo.Context
        int width = breite;
        int heigh = int.parse(doubleparse(width * 1.41, 0));
        int hoehe = 80;
        int zeilen = 0;
        int zeilenhoehe = 12;
        int dpi = 1024;
        string name = "";
        string adresse = "";
        string betriebsnr = "";
        string einheit = _("/ha");
        var cr = context;
        var layout1 = Pango.cairo_create_layout (cr);
        var layout2 = Pango.cairo_create_layout (cr);
        aktion bilanz = aktion();
        
        //Betriebsname lesen
        name = betrieb.name.to_string();
        adresse = betrieb.art.replace("\n", ", ");
        betriebsnr = "0" + betrieb.groesse.to_string()[0:11];
        //Seite weiß malen
        cr.set_source_rgb (1, 1, 1);
        cr.rectangle(0, 0, width, heigh);
        cr.fill();
        //Layouts setzen
        cr.set_source_rgb (0, 0, 0);
        layout1.set_alignment(Pango.Alignment.CENTER);
        layout1.set_width(breite*dpi);
        layout2.set_width(breite*dpi);
        layout2.set_wrap(Pango.WrapMode.WORD);
        layout2.set_font_description(FontDescription.from_string("sans 10"));
        if (page_nr == 0){
        // Deckblatt drucken
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup(_("<b><u>Närhstoffbilanz</u></b>"), -1);
            cr.move_to(0, 30);
            cairo_layout_path (cr, layout1);
            layout1.set_markup("<b><u>" + _("für ")+ name+"</u></b>", -1);
            cr.move_to(0, 80);
            cairo_layout_path (cr, layout1);
            layout1.set_markup("<b><u>" + _("Erntejahr ")+ felderliste[0].jahr.to_string()+"</u></b>", -1);
            cr.move_to(0, 130);
            cairo_layout_path (cr, layout1);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_markup(_("Erstellt mit LASK ") + config.LASK_VERSION, -1);
            cr.move_to(0,  heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.fill();
            
        }else{
            //Adresse und erntejahr oben anzeigen   
            cr.set_source_rgb(0, 0, 0);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_alignment(Pango.Alignment.LEFT);
            layout1.set_markup(betriebsnr + "         <b><u>" + name + "</u></b>, " + adresse + _("        <b><u>Erntejahr: ") + felderliste[0].jahr.to_string() + "</u></b>", -1);
            cr.move_to(30, 10);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Seitennummer unten anzeigen
            layout1.set_markup(_("Seite ") + (page_nr).to_string() + " / " + druckfelder.length.to_string(), -1);
            layout1.set_alignment(Pango.Alignment.CENTER);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            cr.move_to(0, heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Feldname und größe anzeigen
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup("<b><u>" + druckfelder[page_nr -1].druckfeld.name + "</u></b> (" + doubleparse(druckfelder[page_nr -1].druckfeld.groesse) + " ha)" + " " + druckfelder[page_nr -1].druckfeld.frucht, -1);
            cr.move_to(0, 35);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0, 0);
            cr.fill();

            layout1.set_font_description(FontDescription.from_string("sans 20"));
            //Eintrag malen
            layout1.set_markup(_("<b>Eintrag</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            zeilen = zeilen + 2;        
            hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
            foreach (aktion ak in druckfelder[page_nr -1].druckaktionen){
                zeilen = 0;
                //Masseinheiten umrechen
                if(ak.aktion == "Mineralische Düngung" && ak.par2 == "dt"){
                    ak.par9 = ak.par9 * 100;
                }
                if(ak.aktion == "Organische Düngung"){
                    ak.par9 = ak.par9 * 100;
                }
                if(ak.aktion == "Organische Düngung" || ak.aktion == "Mineralische Düngung"){
                    if(ak.flaeche == -1){
                        layout2.set_markup(ak.aktion, -1);
                        einheit = "";
                    }else if(ak.par2 == "kg"){
                        layout2.set_markup(ak.aktion + "(" + ak.par1 + " " + doubleparse(ak.par9) + " " + ak.par2 + einheit + _(") am ") + ak.datum.format("%d.%m.%Y"), -1);
                    }else{
                        layout2.set_markup(ak.aktion + "(" + ak.par1 + " " + doubleparse(ak.par9 / 100) + " " + ak.par2 + einheit + _(") am ") + ak.datum.format("%d.%m.%Y"), -1);
                    }
                    cr.move_to(100 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 2;        
                    bilanz.par3 += ak.par3 * ak.par9 / 100;
                    layout2.set_markup(doubleparse(ak.par3 * ak.par9 / 100) + " kg N" + einheit, -1);
                    cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    bilanz.par4 += ak.par4 * ak.par9 / 100;
                    layout2.set_markup(doubleparse(ak.par4 * ak.par9 / 100) + " kg P" + einheit, -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    bilanz.par5 += ak.par5 * ak.par9 / 100;
                    layout2.set_markup(doubleparse(ak.par5 * ak.par9 / 100) + " kg K" + einheit, -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    bilanz.par8 += ak.par8 * ak.par9 / 100;
                    layout2.set_markup(doubleparse(ak.par8 * ak.par9 / 100) + " kg NH4" + einheit, -1);
                    cr.move_to(700 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                    //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
                }
            }
            //Entzug malen
            layout1.set_markup(_("<b>Entzug</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            hoehe = (zeilen + 3)* zeilenhoehe + hoehe;
            foreach (aktion ak in druckfelder[page_nr -1].druckaktionen){
                zeilen = 0;
                if(ak.aktion == "Ernte"){
                    if(ak.flaeche == -1){
                        layout2.set_markup(_("Ernte"), -1);
                        einheit = "";
                    }else{
                        layout2.set_markup(_("Ernte(") + ak.par2 + _(") am ") + ak.datum.format("%d.%m.%Y"), -1);
                    }
                    cr.move_to(100 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 2;        
                    bilanz.par3 -= int.parse(ak.par1.split(";")[0]);
                    layout2.set_markup(ak.par1.split(";")[0] + " kg N" + einheit, -1);
                    cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    bilanz.par4 -= int.parse(ak.par1.split(";")[1]);
                    layout2.set_markup(ak.par1.split(";")[1] + " kg P" + einheit, -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    bilanz.par5 -= int.parse(ak.par1.split(";")[2]);
                    layout2.set_markup(ak.par1.split(";")[2] + " kg K" + einheit, -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                    //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
                }
            }
            //Bilanz malen
            layout1.set_markup(_("<b>Bilanz</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            hoehe = (zeilen + 3)* zeilenhoehe + hoehe;
            layout2.set_markup(doubleparse(bilanz.par3) + " kg N" + einheit, -1);
            cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup(doubleparse(bilanz.par4) + " kg P" + einheit, -1);
            cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup(doubleparse(bilanz.par5) + " kg K" + einheit, -1);
            cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup(doubleparse(bilanz.par8) + " kg NH4" + einheit, -1);
            cr.move_to(700 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            zeilen = zeilen + 1;        
            cr.fill();
            //Rechteck um Aktion malen
            if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
            cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
            cr.stroke();
            hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
        }
        return cr;
    }
}
