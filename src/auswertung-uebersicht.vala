using Sqlite;
using Gtk;
using lask;
using Cairo;
using Pango;

public class causwertungUebersicht : causwertung{
    private CheckButton checkbutton2;
    private CheckButton checkbutton3;
    private CheckButton checkbutton4;
    private CheckButton checkbutton5;
    private CheckButton checkbutton6;
    private CheckButton checkbutton7;
    private CheckButton checkbutton8;
    private ComboBoxText combobox20;
    
    private druckdaten[] druckfelder = {};
    private druckdaten druckfeld = druckdaten();
    
    public causwertungUebersicht(){
    //Konstructor
        this.window7.set_title(_("Auswertung Übersicht"));
        seiten = aktFelderListe.length;
        foreach (feld f in aktFelderListe) {
            druckfeld.druckfeld = f;
            druckfeld.druckaktionen = laskDb.aktionLesen(f.id.to_string(), erntejahr);
            druckfelder += druckfeld;
        }
        seitenLeiste();
        aendern();
        //Ereignisse verbinden
        combobox20.changed.connect(aendern);
        checkbutton2.toggled.connect(aendern);
        checkbutton3.toggled.connect(aendern);
        checkbutton4.toggled.connect(aendern);
        checkbutton5.toggled.connect(aendern);
        checkbutton6.toggled.connect(aendern);
        checkbutton7.toggled.connect(aendern);
        checkbutton8.toggled.connect(aendern);
    }
    public void seitenLeiste(){
        //Seitenleiste voll machen
        combobox20 = new ComboBoxText();
        combobox20.append_text(_("Alle"));
        combobox20.append_text(_("Ausgewählte"));
        combobox20.set_active(0);
        grid33.attach(combobox20, 1, 0, 1, 1);
        grid33.attach(new Label(_("Felder:")),0 ,0, 1, 1);
        grid33.get_child_at(0, 0).set_halign(Align.END);
        grid33.get_child_at(0, 0).set_hexpand(false);
        grid33.attach(new Label(_("Drucken:")),0 ,1, 1, 1);
        grid33.get_child_at(0, 1).set_halign(Align.END);
        grid33.get_child_at(0, 1).set_hexpand(false);
        grid33.attach(checkbutton2 = new CheckButton.with_label(_("Deckblatt")), 1, 1, 1, 1);
        checkbutton2.set_active(true);
        grid33.attach(new Gtk.Separator(Gtk.Orientation.HORIZONTAL), 0, 2, 2, 1);
        grid33.attach(new Label(_("Aktionen:")),0 ,3, 1, 1);
        grid33.get_child_at(0, 2).set_hexpand(false);
        grid33.get_child_at(0, 2).set_halign(Align.END);
        grid33.attach(checkbutton3 = new CheckButton.with_label(_("Saat")), 1, 3, 1, 1);
        checkbutton3.set_active(true);
        grid33.attach(checkbutton4 = new CheckButton.with_label(_("Bodenbearbeitung")), 1, 4, 1, 1);
        checkbutton4.set_active(true);
        grid33.attach(checkbutton5 = new CheckButton.with_label(_("organische Düngung")), 1, 5, 1, 1);
        checkbutton5.set_active(true);
        grid33.attach(checkbutton6 = new CheckButton.with_label(_("mineralische Düngung")), 1, 6, 1, 1);
        checkbutton6.set_active(true);
        grid33.attach(checkbutton7 = new CheckButton.with_label(_("Pflanzenschutz")), 1, 7, 1, 1);
        checkbutton7.set_active(true);
        grid33.attach(checkbutton8 = new CheckButton.with_label(_("Ernte")), 1, 8, 1, 1);
        checkbutton8.set_active(true);
    }

    
    public override void aendern(){
    //Auswertung neu malen
        feld[] felder = {};
        aktion[] aktionen = {};
        //Wenn deckblatt=0 Deckblatt male sonst nicht
        if (this.checkbutton2.get_active()){
            this.deckblatt = 0;
        } else {
           this.deckblatt = 1;
           if (this.seite == 0){
                this.seite = 1;
            }
        }
        //Welche Felder sollen verwendet werden
       if(this.combobox20.get_active() == 0){
            felder = felderliste;
        }else if(this.combobox20.get_active() == 1){
            felder = aktFelderListe;
        }
        this.seiten = felder.length;
        if (this.seite > this.seiten){
            this.seite = this.seiten;
        }
        this.druckfelder.resize(0);
        foreach (feld f in felder) {
            //Welche Aktionen sollen verwendet werden
            aktionen.resize(0);
            foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                if(f.art == "Grünland"){
                    f.frucht = "Grünland";
                }
                if(a.aktion == "Saat"){
                    if(a.par4 == 1){
                        f.frucht = a.par2;
                    }
                    if(this.checkbutton3.get_active()){
                        aktionen += a;
                    }
                }else if(a.aktion == "Bodenbearbeitung" & this.checkbutton4.get_active()){
                    aktionen += a;
                }else if(a.aktion == "Organische Düngung" & this.checkbutton5.get_active()){
                    aktionen += a;
                }else if(a.aktion == "Mineralische Düngung" & this.checkbutton6.get_active()){
                    aktionen += a;
                }else if(a.aktion == "Pflanzenschutz" & this.checkbutton7.get_active()){
                    aktionen += a;
                }else if(a.aktion == "Ernte" & this.checkbutton8.get_active()){
                    aktionen += a;
                }
            }
            this.druckfeld.druckfeld = f;
            this.druckfeld.druckaktionen = aktionen;
            this.druckfelder += this.druckfeld;
        }
		this.drawingarea1.queue_draw();
    }

    public override Cairo.Context draw_page(int breite, int page_nr, Cairo.Context context){
    //Seite zusammenbauen Cairo.Context
        int width = breite;
        int heigh = int.parse(doubleparse(width * 1.41, 0));
        int hoehe = 120;
        int zeilen = 0;
        int zeilenhoehe = 12;
        int dpi = 1024;
        string name = "";
        string adresse = "";
        string betriebsnr = "";
        string saatmenge = "";
        var cr = context;
        var layout1 = Pango.cairo_create_layout (cr);
        var layout2 = Pango.cairo_create_layout (cr);
        
        //Betriebsname lesen
        name = betrieb.name.to_string();
        adresse = betrieb.art.replace("\n", ", ");
        betriebsnr = "0" + betrieb.groesse.to_string()[0:11];
        //Seite weiß malen
        cr.set_source_rgb (1, 1, 1);
        cr.rectangle(0, 0, width, heigh);
        cr.fill();
        //Layouts setzen
        cr.set_source_rgb (0, 0, 0);
        layout1.set_alignment(Pango.Alignment.CENTER);
        layout1.set_width(breite*dpi);
        layout2.set_width(breite*dpi);
        layout2.set_wrap(Pango.WrapMode.WORD);
        layout2.set_font_description(FontDescription.from_string("sans 10"));
        if (page_nr == 0){
        // Deckblatt drucken
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup(_("<b><u>Ackerschlagkartei</u></b>"), -1);
            cr.move_to(0, 30);
            cairo_layout_path (cr, layout1);
            layout1.set_markup(_("<b><u>für ")+ name+"</u></b>", -1);
            cr.move_to(0, 80);
            cairo_layout_path (cr, layout1);
            layout1.set_markup(_("<b><u>Erntejahr ")+ druckfelder[0].druckfeld.jahr.to_string()+"</u></b>", -1);
            cr.move_to(0, 130);
            cairo_layout_path (cr, layout1);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_markup(_("Erstellt mit LASK ") + config.LASK_VERSION, -1);
            cr.move_to(0,  heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.fill();
            
        }else{
            //Adresse und erntejahr oben anzeigen   
            cr.set_source_rgb(0, 0, 0);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_alignment(Pango.Alignment.LEFT);
            layout1.set_markup(betriebsnr + "         <b><u>" + name + "</u></b>, " + adresse + _("        <b><u>Erntejahr: ") + druckfelder[0].druckfeld.jahr.to_string() + "</u></b>", -1);
            cr.move_to(30, 10);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Seitennummer unten anzeigen
            layout1.set_markup("Seite " + (page_nr).to_string() + " / " + druckfelder.length.to_string(), -1);
            layout1.set_alignment(Pango.Alignment.CENTER);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            cr.move_to(0, heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Feldname, Größe  und Vorfrucht anzeigen
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup("<b><u>" + druckfelder[page_nr -1].druckfeld.name + "</u></b> (" + doubleparse(druckfelder[page_nr -1].druckfeld.groesse) + " ha)" + " " + druckfelder[page_nr -1].druckfeld.frucht, -1);
            cr.move_to(0, 35);
            cairo_layout_path (cr, layout1);
            layout1.set_font_description(FontDescription.from_string("sans 15"));
            if (druckfelder[page_nr -1].druckfeld.vorfrucht == null){
                druckfelder[page_nr -1].druckfeld.vorfrucht = _("unbekannt");
            }
            if(druckfelder[page_nr -1].druckfeld.art == "Feld"){
                layout1.set_markup(_("<b>Vorfrucht: ") + druckfelder[page_nr -1].druckfeld.vorfrucht + "</b>", -1);
                cr.move_to(0, 70);
                cairo_layout_path (cr, layout1);
                cr.set_source_rgb(0, 0, 0);
                cr.fill();
            }

            foreach (aktion ak in druckfelder[page_nr -1].druckaktionen){
                //linke Spalte schreiben
                zeilen = 0;
                layout2.set_width(300*dpi);
                layout2.set_markup(ak.datum.format("%d.%m.%Y"), -1);
                cr.move_to(30 ,hoehe);
                cairo_layout_path (cr, layout2);
                //Wegen Übersetzung
                if (ak.aktion == "Bodenbearbeitung"){
                    layout2.set_markup(_("<b><u>Bodenbearbeitung</u></b>"), -1);
                }else if (ak.aktion == "Saat"){
                    layout2.set_markup(_("<b><u>Saat</u></b>"), -1);
                }else if (ak.aktion == "Mineralische Düngung"){
                    layout2.set_markup(_("<b><u>Mineralische Düngung</u></b>"), -1);
                }else if (ak.aktion == "Organische Düngung"){
                    layout2.set_markup(_("<b><u>Organische Düngung</u></b>"), -1);
                }else if (ak.aktion == "Ernte"){
                    layout2.set_markup(_("<b><u>Ernte</u></b>"), -1);
                }else if (ak.aktion == "Pflanzenschutz"){
                    layout2.set_markup(_("<b><u>Pflanzenschutz</u></b>"), -1);
                }
                cr.move_to(30 ,hoehe + zeilenhoehe);
                cairo_layout_path (cr, layout2);
                
                //mittlere Spalte schreiben
                layout2.set_width(400*dpi);
                layout2.set_wrap(Pango.WrapMode.WORD);
                if (ak.aktion == "Bodenbearbeitung"){
                    layout2.set_markup("<b>" + ak.par1 + "</b>", -1);
                    cr.move_to(300 ,hoehe + zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                }else if (ak.aktion == "Saat"){
                    if (ak.par3 <= 0){
                        saatmenge = doubleparse(0 - ak.par3, 2) + " kg/ha";
                    }else{
                        saatmenge = doubleparse(ak.par3, 0) + _(" Kö/m²");
                    }
                    layout2.set_markup("<b>" + ak.par2 + "</b> (" +  ak.par1 + ")  ", -1);
                    cr.move_to(300 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(saatmenge, -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                }else if (ak.aktion == "Mineralische Düngung"){
                    layout2.set_markup("<b>" + ak.par1 + "</b> " + doubleparse(ak.par9) + " " + ak.par2 + "/ha", -1);
                    cr.move_to(300 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par3 * ak.par9 / 100) + " kg N/ha ", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par6 * ak.par9 / 100) + " kg M/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par4 * ak.par9 / 100) + " kg P/ha", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par7 * ak.par9 / 100) + " kg S/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par5 * ak.par9 / 100) + " kg K/ha", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par8 * ak.par9 / 100) + " kg C/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                }else if (ak.aktion == "Organische Düngung"){
                    layout2.set_markup("<b>" + ak.par1 + "</b> " + doubleparse(ak.par9) + " " + ak.par2 + "/ha", -1);
                    cr.move_to(300 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par3 * ak.par9) + " kg N/ha ", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par6 * ak.par9) + " kg M/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par4 * ak.par9) + " kg P/ha", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par7 * ak.par9) + " kg S/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    layout2.set_markup(doubleparse(ak.par5 * ak.par9) + " kg K/ha", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup(doubleparse(ak.par8 * ak.par9) + " kg C/ha", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                }else if (ak.aktion == "Ernte"){
                    layout2.set_markup("<b>" + doubleparse(ak.par3) + " dt/ha</b>", -1);
                    cr.move_to(300 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                }else if (ak.aktion == "Pflanzenschutz"){
                    foreach (psMittel psm in ak.psMittel){
                        layout2.set_markup("<b>" + psm.mittel + "</b><small> (" + psm.id + ")</small>", -1);
                        cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        layout2.set_markup(doubleparse(psm.menge, 3) + " l/ha", -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        zeilen = zeilen + 1;
                    }
                }
                //rechte Spalte schreiben
                layout2.set_width(250*dpi);
                layout2.set_markup("<b><u>" + ak.anwender + "</u></b>", -1);
                cr.move_to(700 ,hoehe);
                cairo_layout_path (cr, layout2);
                cr.fill();
                layout2.set_wrap(Pango.WrapMode.WORD);
                layout2.set_markup(ak.kommentar, -1);
                cr.move_to(700 ,hoehe + zeilenhoehe);
                cairo_layout_path (cr, layout2);
                cr.fill();
                //Rechteck um Aktion malen
                if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                cr.stroke();
                hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
            }
            
        }
        return cr;
    }


}
