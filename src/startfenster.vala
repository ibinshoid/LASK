using Gtk;
using lask;
public class cstartFenster : GLib.Object{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window1;
    private TreeView treeview1;
    private Gtk.ListStore liststore1;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    
    public cstartFenster() {    
    //Startfenster zusammenbauen

    err(uiVerzeichnis + _("/lask.ui wird von startfenster.vala geladen"), 0);
        builder.set_translation_domain ("lask");
        window1 = builder.get_object ("window1") as Window;
        treeview1 = builder.get_object ("treeview1") as TreeView;
		liststore1 = builder.get_object ("liststore1") as Gtk.ListStore;
        button1 = builder.get_object ("button1") as Button;
        button2 = builder.get_object ("button2") as Button;
        button3 = builder.get_object ("button3") as Button;
        button4 = builder.get_object ("button4") as Button;
        builder.connect_signals (this);
        //Ereignisse verbinden
        window1.destroy.connect (Gtk.main_quit);
        button1.clicked.connect(on_button1_clicked);
        button2.clicked.connect(on_button2_clicked);
        button3.clicked.connect(on_button3_clicked);
        button4.clicked.connect(Gtk.main_quit);
        treeview1.row_activated.connect(on_button1_clicked);
        
        update();
        hauptFenster = new chauptFenster();

    }

    public void update(){
    //Dateiliste holen
        liststore1.clear();
        Dir dir = Dir.open(datenVerzeichnis , 0);
        while ((dbDatei = dir.read_name()) != null) {
            liststore1.insert_with_values(null, -1, 0, 0, 0, dbDatei[0:-5], -1);
        }
    }

    public void on_button1_clicked(){
        //Betrieb öffnen
        TreeModel model;
        this.treeview1.get_selection().get_selected(out model, out iter);
        model.get(iter, 0, out dbDatei);
        laskDb.open(datenVerzeichnis + dbDatei + ".lask");
        
        hauptFenster.betriebLesen();
        startFenster.window1.hide();
        hauptFenster.window2.show_all();
    }
    
    public void on_button2_clicked(){
        //Betrieb löschen       
        TreeModel model;
        this.treeview1.get_selection().get_selected(out model, out iter);
        startFenster.liststore1.get(iter, 0, out dbDatei);
        
        Dialog dialog = new Gtk.Dialog.with_buttons(_("Frage"), window1,
                                                    DialogFlags.MODAL,
                                                    _("Abbrechen"), Gtk.ResponseType.CANCEL,
                                                    _("Ok"), Gtk.ResponseType.OK);
        dialog.get_content_area().add(new Label(_("Betrieb '") + dbDatei + _("' wirklich Löschen? \nAlle Daten gehen verloren\n")));            
        dialog.show_all();
        
        if (dialog.run() == ResponseType.OK) {
            File.new_for_path (datenVerzeichnis + dbDatei + ".lask").delete();  
            dialog.close();
            startFenster.update();          
        }else{dialog.close();}
    }
    
    public void on_button3_clicked(){
        anlegenFenster = new canlegenFenster();
        anlegenFenster.assistant1.show_all();
        startFenster.window1.hide();
    }
        
}   
