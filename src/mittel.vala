using Sqlite;
using Gtk;
using lask;

public class cmittelFenster : GLib.Object{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window5;
    public string aktionArt = "";
    private string[] arten = {};
    public string[] dbArten = {};
    public int aktionId = -1;
    private TreeStore treestore1;
    private TreeView treeview4;
    private Button button11;
    private Button button12;
    private Button button13;
    private Button button14;
    private ComboBoxText combobox6;
    private mittel aktMittel;

    public cmittelFenster() {
    //Constructor
    err(uiVerzeichnis + _("lask.ui wird von mittel.vala geladen"),0);
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        window5 = builder.get_object ("window5") as Window;
        treestore1 = builder.get_object ("treestore1") as TreeStore;
        treeview4 = builder.get_object ("treeview4") as TreeView;
        button11 = builder.get_object ("button11") as Button;
        button12 = builder.get_object ("button12") as Button;
        button13 = builder.get_object ("button13") as Button;
        button14 = builder.get_object ("button14") as Button;
        combobox6  = builder.get_object ("combobox6") as ComboBoxText;
        this.window5.set_modal(true);
        //Ereignisse verbinden
        treeview4.cursor_changed.connect(treeview4_changed);
        treeview4.row_activated.connect(mittelAendern);
        combobox6.changed.connect(combobox6_changed);
        button11.clicked.connect(mittelHinzufuegen);
        button12.clicked.connect(mittelAendern);
        button13.clicked.connect(mittelEntfernen);
        button14.clicked.connect(()=>{this.window5.destroy();});
    }

    public void combobox6_changed(){
    //Mittelart wurde gewählt
        mittel[] Mittel = {};
        TreeIter iter =  TreeIter();    
        TreeIter iter2 = TreeIter();   
        string[] arten = {};
        mittelFenster.arten.resize(0);
        mittelFenster.treestore1.clear();
        mittelFenster.aktMittel.art = mittelFenster.combobox6.get_active();
        mittelFenster.button12.set_sensitive(false);
        mittelFenster.button13.set_sensitive(false);
        mittelFenster.dbArten.resize(0);
        
        Mittel = laskDb.mittelLesen(erntejahr);
        foreach (mittel m in Mittel){
            if (m.art == mittelFenster.combobox6.get_active()){
                    if (m.art == 0){
                    //Wenn Saatgut
                        if (m.par3 in mittelFenster.arten){}
                        else{
                            mittelFenster.arten += m.par3;
                            mittelFenster.treestore1.append(out iter, null);
                            foreach (mittel m2 in  laskDb.mittelLesen(erntejahr)){
                                if (m2.art == 6 && m2.name == m.par3){ 
                                    mittelFenster.treestore1.set (iter, 0, m2.id, -1);
                                }
                            }
                            mittelFenster.treestore1.set (iter, 1, m.par3, -1);
                        }
                        mittelFenster.treestore1.append(out iter2, iter);
                        mittelFenster.treestore1.set (iter2, 0,m.id, -1);
                        mittelFenster.treestore1.set (iter2, 1,m.name, -1);
                        
                    }
                    else if (m.art == 1){
                    //Wenn Pflanzenschutzmittel
                        if (m.par3 in mittelFenster.arten){}
                        else{
                            mittelFenster.arten += m.par3;
                            mittelFenster.treestore1.append(out iter, null);
                            mittelFenster.treestore1.set (iter, 1,m.par3, -1);
                        }
                        mittelFenster.treestore1.append(out iter2, iter);
                        mittelFenster.treestore1.set (iter2, 0,m.id, -1);
                        mittelFenster.treestore1.set (iter2, 1,m.name, -1);
                        
                    }
                    
                    else{
                        mittelFenster.treestore1.append(out iter, null);
                        mittelFenster.treestore1.set (iter, 0,m.id, -1);
                        mittelFenster.treestore1.set (iter, 1,m.name, -1);
                    }
                }
                //Wenn Fruchtart
                if (m.art == 6){
                    if (m.name in mittelFenster.dbArten){}else{
                        arten += m.name;
                    }
                }
            }
            mittelFenster.dbArten = arten;
        }
     
    public void treeview4_changed(){
    //Mittel wurde in Listbox1 gewählt
        TreeSelection selection;
        TreeModel model;
        GLib.Value wert;
        
        mittelFenster.aktMittel.art = -1;
        selection = mittelFenster.treeview4.get_selection();
        selection.get_selected(out model, out iter);
        model.get_value(iter, 0, out wert);
        mittelFenster.button12.set_sensitive(false);
        mittelFenster.button13.set_sensitive(false);
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.id == wert.get_int()){
                mittelFenster.aktMittel = m;
                if (m.art != 6){
                    mittelFenster.button13.set_sensitive(true);
                }
            }else if (wert.get_int() > -1){
                mittelFenster.button12.set_sensitive(true);
            }
        }
    }

    public void mittelEntfernen(){
    //Mittel entfernen
        Dialog dialog = new Dialog.with_buttons(_("Frage"),this.window5,Gtk.DialogFlags.MODAL,
                                                        _("Abbrechen"), Gtk.ResponseType.CANCEL,
                                                        _("OK"), Gtk.ResponseType.OK);
        dialog.get_content_area().add(new Label(_("Betriebsmittel wirklich Löschen? \nAlle Daten gehen verloren\n")));           
        dialog.show_all();
        
        if (dialog.run() == Gtk.ResponseType.OK) {
            laskDb.mittelEntfernen(mittelFenster.aktMittel.id);
            this.combobox6_changed();
            dialog.close();
        }else{dialog.close();}
    }
    
    public void mittelHinzufuegen(){
    //Fenster zum Mittel anlegen bauen und zeigen   
        mittelEdit = new cmittelEdit();
        mittelEdit.window4.set_transient_for(this.window5);
        mittelEdit.window4.show_all();
        laskDb.mittelHinzufuegen(mittelEdit.mittelNeu(this.combobox6.get_active()));
        this.combobox6.changed();
    }
    
        
    public void mittelAendern(){
    //Fenster zum Mittel ändern bauen und zeigen    
        if (this.aktMittel.art >= 0){
            mittelEdit = new cmittelEdit();
            mittelEdit.window4.set_transient_for(this.window5);
            mittelEdit.window4.show_all();
            laskDb.mittelAendern(mittelEdit.mittelAendern(this.aktMittel));
            this.combobox6.changed();
        }
    }
}
