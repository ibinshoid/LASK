using Sqlite;
using Gtk;
using lask;
using Cairo;
using Pango;

public abstract class causwertung : GLib.Object{
    protected Builder builder = new Builder.from_string(config.UI_FILE, -1);
    protected Window tmpfenster;
    public Window window7;
    protected ScrolledWindow scrolledwindow10;
    protected DrawingArea drawingarea1;
    protected ToolButton toolbutton1;
    protected ToolButton toolbutton2;
    protected ToolButton toolbutton4;
    protected ToolButton toolbutton5;
    protected ToolButton toolbutton6;
    protected ToolButton toolbutton16;
    protected Label label82;
    protected Grid grid33;
    protected int groesse = 0;
    protected int seite = 0;     
    protected int seiten = 0;     
    protected int deckblatt = 0;
    
    protected causwertung() {
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von auswertung.vala geladen"),0);
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        seiten = aktFelderListe.length;
        window7 = builder.get_object("window7") as Window;
        scrolledwindow10 = builder.get_object("scrolledwindow10") as ScrolledWindow;
        drawingarea1 = builder.get_object("drawingarea1") as DrawingArea;
        toolbutton1 = builder.get_object("toolbutton1") as ToolButton;
        toolbutton2 = builder.get_object("toolbutton2") as ToolButton;
        toolbutton4 = builder.get_object("toolbutton4") as ToolButton;
        toolbutton5 = builder.get_object("toolbutton5") as ToolButton;
        toolbutton6 = builder.get_object("toolbutton6") as ToolButton;
        toolbutton16 = builder.get_object("toolbutton16") as ToolButton;
        label82 = builder.get_object("label82") as Label;
        grid33 = builder.get_object("grid33") as Grid;
        this.window7.set_modal(true);
        //Ereignisse verbinden
        drawingarea1.draw.connect(maleSeite);
        toolbutton1.clicked.connect(vorherige_seite);
        toolbutton2.clicked.connect(naechste_seite);
        toolbutton4.clicked.connect(drucken);
        toolbutton5.clicked.connect(kleiner);
        toolbutton6.clicked.connect(groesser);
        toolbutton16.clicked.connect(()=>{this.window7.destroy();});
    }

    public bool maleSeite(Cairo.Context cr){
    //Seite in Drawingarea malen    
        if (this.groesse == 1){
        //Groß
           if(scrolledwindow10.get_allocated_width() >= scrolledwindow10.get_allocated_height()/1.41){
                drawingarea1.set_size_request(scrolledwindow10.get_allocated_width() -20, int.parse(doubleparse(scrolledwindow10.get_allocated_width() * 1.41 -20, 0)));
                cr.scale((drawingarea1.get_allocated_width()-20)/1000.0, (drawingarea1.get_allocated_width()-20)/1000.0);
            }else{
                drawingarea1.set_size_request(int.parse(doubleparse(scrolledwindow10.get_allocated_height()/1410.0 -20)), scrolledwindow10.get_allocated_height()-20);
                cr.scale((drawingarea1.get_allocated_height()-20)/1410.0, (drawingarea1.get_allocated_height()-20)/1410.0);
			}
		}else{
        //Klein
           if((scrolledwindow10.get_allocated_width()-20) >= (scrolledwindow10.get_allocated_height()-20)/1.41){
                drawingarea1.set_size_request(int.parse(doubleparse(scrolledwindow10.get_allocated_height()/1.41 -20)), scrolledwindow10.get_allocated_height() -20);
                cr.scale((drawingarea1.get_allocated_height()-20)/1410.0, (drawingarea1.get_allocated_height()-20)/1410.0);
            }else{
                drawingarea1.set_size_request((scrolledwindow10.get_allocated_width() -20), int.parse(doubleparse((scrolledwindow10.get_allocated_width()-20)*1.41)));
                cr.scale((drawingarea1.get_allocated_width()-20)/1000.0, (drawingarea1.get_allocated_width()-20)/1000.0);
			}
		}
		draw_page((1000), seite, cr);
        this.label82.set_text(seite.to_string() + "/" + seiten.to_string());
		return true;
	}
    
    public void groesser(){
    //In Seitenbreite einpassen
        this.groesse = 1;
        this.drawingarea1.queue_draw();
    }

    public void kleiner(){
    //In Seitenhöhe einpassen
        this.groesse = 0;
        this.drawingarea1.queue_draw();
    }

    public void naechste_seite(){
    //Eine Seite vor blättern
        if (this.seite < this.seiten){
            this.seite += 1;
        }        this.drawingarea1.queue_draw();
    }
 
    public void vorherige_seite(){
    //Eine Seite zurück blättern
        if (this.seite > this.deckblatt){
            this.seite -= 1;
        }
        this.drawingarea1.queue_draw();
    }

    public void drucken(){
    //Drucken
        PrintSettings settings = null;
        PrintOperation printop;
        PrintOperationResult res;
        //Drucksystem initialisieren
        printop = new PrintOperation();
        printop.set_n_pages(this.seiten + 1 - this.deckblatt);
        printop.set_print_settings(settings);
        printop.draw_page.connect (print_pages);
        res = printop.run (PrintOperationAction.PRINT_DIALOG, this.tmpfenster);
//        res = printop.run (PrintOperationAction.PREVIEW, this.tmpfenster);
    }       

    public void print_pages(PrintOperation printop, PrintContext context, int page_nr){
    //Holt Seiten von draw_page und gibt sie scalliert an Drucksystem weiter
        context.get_cairo_context().scale(context.get_width()/1000, context.get_width()/1000);
        this.draw_page(1000, page_nr + this.deckblatt, context.get_cairo_context());
    }

    public abstract Cairo.Context draw_page(int breite, int page_nr, Cairo.Context context);
    public abstract void aendern();
}
