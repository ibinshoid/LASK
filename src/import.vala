using Gtk;
using lask;
using Sqlite;
using ShapeLib;

private class protokollEintrag:Box{
    private Label was;
    private CheckButton echt;
    public bool i = false;
    public int id;
    
    public protokollEintrag(int idd, string text){
        id = idd;
        was = new Label(text);
        was.set_line_wrap(true);
        echt = new CheckButton();
        this.add(echt);
        this.add(was);
        echt.toggled.connect(()=>{i = echt.get_active();});
   }
}

public class cimport{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    private Window window9;
    private Button button20;
    private Button button21;
    private TreeView treeview5;
    private TreeView treeview6;
    private TreeView treeview7;
    private Gtk.ListStore liststore5;
    private Gtk.ListStore liststore6;
    private Gtk.ListStore liststore7;
    private CellRendererToggle toggle1;
    private TreeIter iter;
    private Grid grid39;
    private GLib.Value v; 
    private GLib.Value v2; 
    private MainLoop loop = new MainLoop();
    private int rc = -1;
    private int arbeit = 0;
    public cimport(){
    //Konstruktor
    err(uiVerzeichnis + _("/lask.ui wird von import.vala geladen"),0);
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        window9 = builder.get_object ("window9") as Window;
        button20 = builder.get_object ("button20") as Button;
        button21 = builder.get_object ("button21") as Button;
        treeview5 = builder.get_object ("treeview5") as TreeView;
        treeview6 = builder.get_object ("treeview6") as TreeView;
        treeview7 = builder.get_object ("treeview7") as TreeView;
        liststore5 = builder.get_object ("liststore5") as Gtk.ListStore;
        liststore6 = builder.get_object ("liststore6") as Gtk.ListStore;
        liststore7 = builder.get_object ("liststore7") as Gtk.ListStore;
        toggle1 = builder.get_object ("cellrenderertoggle1") as CellRendererToggle;
        grid39 = builder.get_object ("grid39") as Grid;
        this.window9.set_modal(true);
        grid39.show_all();
        //Signale verbinden
        button20.clicked.connect(()=>{this.arbeit = 1;
                                      this.loop.quit();
                                      this.window9.destroy();});            
        button21.clicked.connect(()=>{this.arbeit = 0;
                                      this.loop.quit();
                                      this.window9.destroy();});            
        treeview5.row_activated.connect((view, path, column) => {
                    Gtk.ListStore liststore = view.model as Gtk.ListStore;
                    view.model.get_iter(out iter, path);view.model.get_value(iter, 9, out v); liststore.set(iter, 9, !(v.get_boolean()));});
        treeview6.row_activated.connect((view, path, column) => {
                    Gtk.ListStore liststore = view.model as Gtk.ListStore;
                    view.model.get_iter(out iter, path);view.model.get_value(iter, 9, out v); liststore.set(iter, 9, !(v.get_boolean()));});
        treeview7.row_activated.connect((view, path, column) => {
                    Gtk.ListStore liststore = view.model as Gtk.ListStore;
                    view.model.get_iter(out iter, path);view.model.get_value(iter, 9, out v); liststore.set(iter, 9, !(v.get_boolean()));});
    }
    
    
    public bool mobilGeraet(Database dbMobile){
    err(_("Lese protokoll"), 0);
        Statement stmt;
        int i = 0;
        int i2 = 0;
        int[] geloescht = {};
        
        this.window9.show_all();
        rc = dbMobile.prepare_v2("SELECT * FROM protokoll ORDER BY 'id' DESC", -1, out stmt, null);
        if ( rc == 1 ) {
            err(_("SQL error protokollLesen: ") + rc.to_string() + dbMobile.errmsg(), 1);
            rc = 0;
        }
        //listviews vollmachen
        do{
            rc = stmt.step();
            if(stmt.column_int(6) == 1){
                liststore5.append(out iter);
                liststore5.set(iter, 1, stmt.column_int(1), 3, stmt.column_text(3), 4, stmt.column_int(4), 5, stmt.column_text(5), 9, true);
            }else if(stmt.column_int(6) == 2){
                liststore6.append(out iter);
                liststore6.set(iter, 1, stmt.column_int(1), 3, stmt.column_text(3), 4, stmt.column_int(4), 5, stmt.column_text(5), 9, true);
           }else if(stmt.column_int(6) == 3){
                liststore7.append(out iter);
                liststore7.set(iter, 1, stmt.column_int(1), 3, stmt.column_text(3), 4, stmt.column_int(4), 5, stmt.column_text(5), 9, true);
            }
        }while(rc == Sqlite.ROW);
        loop.run();
        //Alle abgehakten Einträge abarbeiten
        err(_("Importiere Aktionen"), 0);
        if(this.arbeit == 1){
            this.liststore7.foreach((model, path, iter) => {
                //Wenn gelöscht
                i = 0;
                model.get_value(iter, 9, out v);
                if (v.get_boolean() == true){
                    model.get_value(iter, 2, out v);
                    model.get_value(iter, 4, out v2);
                    rc = laskDb.db.prepare_v2("DELTE FROM '" + v.get_int().to_string() + "' WHERE id = '" + v2.get_int().to_string() + "'", -1, out stmt, null);
                    geloescht += v2.get_int();
                }
                i += 1;
                return false;
            });
            this.liststore5.foreach((model, path, iter) => {
                //Wenn hizugefügt
                i = 0;
                i2 = 0;
                model.get_value(iter, 9, out v);
                if (v.get_boolean() == true){
                    //v ist Erntejahr
                    model.get_value(iter, 1, out v);
                    //v2 ist Aktion id
                    model.get_value(iter, 4, out v2);
                    foreach (int i3 in geloescht){
                        if (i3 == v2.get_int()){
                            i2 = 1;
                        }
                    }
                    if (i2 == 0){
                        //Wenn nicht gelöscht anlegen
                        rc = dbMobile.prepare_v2("SELECT * FROM '" + v.get_int().to_string() + "' WHERE id = '" + v2.get_int().to_string() + "'" , -1, out stmt, null);
                        rc = stmt.step();
                        rc = laskDb.db.prepare_v2("INSERT INTO '" + v.get_int().to_string() + "' VALUES ("
                                            + "(SELECT 1 + max(ID) FROM '" + v.get_int().to_string() + "'),'"
                                            + stmt.column_text(1) + "','"
                                            + stmt.column_text(2) + "','"
                                            + stmt.column_text(3) + "','"
                                            + stmt.column_text(4) + "','"
                                            + stmt.column_text(5) + "','"
                                            + stmt.column_text(6) + "','"
                                            + stmt.column_text(7) + "','"
                                            + stmt.column_text(8) + "','"
                                            + stmt.column_text(9) + "','"
                                            + stmt.column_text(10) + "','"
                                            + stmt.column_text(11) + "','"
                                            + stmt.column_text(12) + "','"
                                            + stmt.column_text(13) + "','"
                                            + stmt.column_text(14) + "','"
                                            + stmt.column_text(15) + "','"
                                            + stmt.column_text(16) + "','"
                                            + stmt.column_text(17) + "','"
                                            + stmt.column_text(18) + "'  )", -1, out stmt, null);
                        rc = stmt.step();
                    }
                }
                i += 1;
                return false;
            });
            this.liststore6.foreach((model, path, iter) => {
                //Wenn geändert
                i = 0;
                i2 = 0;
                model.get_value(iter, 9, out v);
                if (v.get_boolean() == true){
                    model.get_value(iter, 2, out v);
                    model.get_value(iter, 4, out v2);
                    foreach (int i3 in geloescht){
                        if (i3 == v2.get_int()){
                            i2 = 1;
                        }
                    }
                    if (i2 == 0){
                        //Wenn nicht gelöscht ändern
                        rc = dbMobile.prepare_v2("SELECT * FROM '" + v.get_int().to_string() + "' WHERE id = '" + v2.get_int().to_string() + "'" , -1, out stmt, null);
                        rc = stmt.step();
                        rc = laskDb.db.prepare_v2("UPDATE '" + v.get_int().to_string()
                                            + "' SET datum = '" + stmt.column_text(3) + "'," 
                                                + "kosten = '"+ stmt.column_text(4) + "' ," 
                                                + "par1 = '" + stmt.column_text(5) + "' ,"
                                                + "par2 = '" + stmt.column_text(6) + "' ,"
                                                + "par3 = '" + stmt.column_text(7) + "' ,"
                                                + "par4 = '" + stmt.column_text(8) + "' ,"
                                                + "par5 = '" + stmt.column_text(9) + "' ,"
                                                + "par6 = '" + stmt.column_text(10) + "' ,"
                                                + "par7 = '" + stmt.column_text(11) + "' ,"
                                                + "par8 = '" + stmt.column_text(12) + "' ,"
                                                + "par9 = '" + stmt.column_text(13) + "' ,"
                                                + "bbch = '" + stmt.column_text(14) + "' ,"
                                                + "bbch = '" + stmt.column_text(15) + "' ,"
                                                + "flaeche = '" + stmt.column_text(16) + "' ,"
                                                + "kommentar = '" + stmt.column_text(17) + "' ,"
                                                + "anwender = '" + stmt.column_text(18) + "' WHERE id = '" + v2.get_int().to_string() + "' ", -1, out stmt, null);
                                            rc = stmt.step();
                    }
                }
                i += 1;
                return false;
            });
            return true;
        }
        return false;
    }
    
    
    public feld[] iBalis(string shapeDatei){
        string alt = "";
        var a = new Archive.Read();
        feld[] felder = {};
        feld tmpFeld = feld();
        int i = 0;
        
        //feldstuecke.zip öffnen
        Environment.set_current_dir("/tmp");
        a.support_filter_all ();
        a.support_format_all ();
        a.open_filename (shapeDatei, 10240);
        unowned Archive.Entry entry;
        //feldstuecke.dbf herausholen
        while (a.next_header (out entry) == Archive.Result.OK) {
            if (entry.pathname().has_suffix(".dbf")){
                err(_("Öffne Datei: ") + entry.pathname(), 0);
                a.extract(entry);
                //feldstuecke.dbf öffnen
                DBFHandle handle = new DBFHandle("/tmp/"+ entry.pathname(),"rb");
                while(i < handle.get_record_count()){
                    err(_("Importiere Feld: ") + handle.read_string_attribute(i, 2), 0);
                    tmpFeld = feld();
                    tmpFeld.id = -1;
                    tmpFeld.jahr = handle.read_integer_attribute(i, 3);
                    tmpFeld.art = "Feld";
                    alt = handle.read_string_attribute(i, 2);
                    tmpFeld.name = convert(alt, -1, "utf-8", "ISO-8859-1");
                    tmpFeld.groesse = double.parse(handle.read_string_attribute(i, 6));
                    felder += tmpFeld;
                    i+=1;
                }
            handle.close();
            try {File.new_for_path("/tmp/feldstuecke.dbf").delete();}catch (Error e) {err(e.message);}
            }
        }
        a.close();
        Environment.set_current_dir(uiVerzeichnis);
        return felder;
    }
    
    public void datei(){
        string neuBetrieb = "";
        string betrieb = "";
        feld rueckFeld = feld();
        mittel[] Mittel = {};
        int[] jahre = {};
        aktion[] aktionen = {};
        feld[] felder = {};
        claskDb importDb = new claskDb();
        
        var dialog = new FileChooserDialog (_("Betrieb importieren"), anlegenFenster.assistant1,
                                            FileChooserAction.OPEN,
                                            _("Abbrechen"), ResponseType.CANCEL,
                                            _("Öffnen"), ResponseType.OK,
                                            null);
        dialog.set_current_folder(datenVerzeichnis);
        
        if (dialog.run() == ResponseType.OK) {
            dialog.hide();
            startFenster.update();          
            betrieb = dialog.get_filename();
            neuBetrieb = datenVerzeichnis + anlegenFenster.entry6.get_text() + ".lask";
            dialog.close();
        }else{dialog.close();}
        if(betrieb != ""){
            importDb.open(betrieb);
            laskDb.betriebAnlegen(neuBetrieb);
            //Namen mit id 0 einfügen
            rueckFeld.id = 0;
            rueckFeld.name=anlegenFenster.entry6.get_text();
            rueckFeld.art=anlegenFenster.textview3.get_buffer().text;
            rueckFeld.groesse=double.parse(anlegenFenster.entry7.get_text());
            laskDb.feldHinzufuegen(rueckFeld);
            
            //Mittel importieren
            err(_("Mittel werden importiert"), 0);
            Mittel = importDb.mittelLesen(0);
            foreach (mittel m in Mittel){
                //Datenbankleichen verhindern
                if (m.art == 0 || m.art == 1){
                    if (m.par3 == ""){m.par3 = "Sonstiges";}
                }
                laskDb.mittelHinzufuegen(m);
            }
            
            //Felder importieren
            err(_("Felder werden importiert"), 0);
            felder = importDb.felderLesen(0);
            foreach (feld f in felder){
                laskDb.feldHinzufuegen(f);
            }
            
            //Jahre importieren 
            err(_("Jahre werden importiert"), 0);
            jahre = importDb.jahreLesen();
            foreach (int jahr in jahre){
                laskDb.erntejahrHinzufuegen(jahr);
            }
            
            //Aktionen importieren  
            err(_("Aktionen werden importiert"), 0);
            foreach (int j in jahre) {
                aktionen = importDb.aktionLesen("", j);
                foreach (aktion a in aktionen){
                    laskDb.aktionHinzufuegen(a, j);
                }
            }
        }
    }
}

