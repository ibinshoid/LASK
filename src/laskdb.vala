using Sqlite;
using Gtk;
using lask;

public class claskDb{
    public Database db;
    private int rc = 0;

        
    public void open(string dbdatei) {
    //Datenbank öffnen
    err(_("DB Datei wird geladen: ") + dbdatei);
        rc=db.open (dbdatei, out db);
        if ( rc == 1){
            err(_("Kann Datenbank nicht öffnen: ") + db.errmsg(), 1);
        }
    }
    
    public mittel[] mittelLesen (int jahr) {
    //Mittel aus Datenbank lesen
    err(_("DB Mittel werden gelesen"), 0);
        Statement stmt;
        mittel[] Mittel = {};
        mittel rueckMittel = mittel();
        rc = db.prepare_v2("SELECT * FROM 'mittel' ORDER BY par3", -1, out stmt, null);
        
        if ( rc == 1 ) {
            err(_("SQL Fehler mittelLesen: ") + db.errmsg(), 1);
            rc = 0;
        }
        do {
            rc = stmt.step();
            switch ( rc ) {
            case Sqlite.DONE:
                break;
                case Sqlite.ROW:
                    rueckMittel.id = stmt.column_int(0);
                    rueckMittel.par2 = stmt.column_text(1);
                    rueckMittel.jahr = stmt.column_double(2);
                    rueckMittel.art = stmt.column_int(3);
                    rueckMittel.name=stmt.column_text(4);
                    rueckMittel.par3=stmt.column_text(5);
                    rueckMittel.par4=stmt.column_double(6);
                    rueckMittel.par5=stmt.column_double(7);
                    rueckMittel.par6=stmt.column_double(8);
                    rueckMittel.par7=stmt.column_double(9);
                    rueckMittel.par8=stmt.column_double(10);
                    rueckMittel.par9=stmt.column_double(11);
                    Mittel += rueckMittel;
                    break;
                default:
                    err(_("Fehler beim Lesen der Mittel"), 1);
                    break;
                }
            } 
        while ( rc == Sqlite.ROW );
        return Mittel;  
        
    }

    
    public feld[] felderLesen (int jahr) {
    err(_("DB Felder werden gelesen"), 0);
    //Felder aus Datenbank lesen
        Statement stmt;
        string ejahr = "";
        feld[] felder = {};
        feld rueckFeld = feld();
        
        if (jahr > 0){
            ejahr =  " WHERE jahr = " + jahr.to_string() + " ORDER BY substr(art, 1, 4), 'name'";
        }
        rc = db.prepare_v2("SELECT * FROM 'feldjahr'" + ejahr, -1, out stmt, null);
        if ( rc == 1 ) {
            err(_("SQL Fehler felderLesen: ") + db.errmsg(), 0);
            rc = 0;
        }
        do {
            rc = stmt.step();
            switch ( rc ) {
            case Sqlite.DONE:
                break;
            case Sqlite.ROW:
                rueckFeld.id=stmt.column_int(0);
                rueckFeld.jahr=stmt.column_int(1);
                rueckFeld.name=stmt.column_text(2);
                rueckFeld.art=stmt.column_text(3).split(";")[0];
                rueckFeld.vorfrucht=stmt.column_text(3).split(";")[1];
                rueckFeld.groesse=stmt.column_double(4);
                rueckFeld.pacht=stmt.column_double(5);
                rueckFeld.dungn=stmt.column_double(6);
                rueckFeld.dungp=stmt.column_double(7);
                rueckFeld.dungk=stmt.column_double(8);
                rueckFeld.dungm=stmt.column_double(9);
                rueckFeld.dungs=stmt.column_double(10);
                rueckFeld.dungc=stmt.column_double(11);
                felder += rueckFeld;
                break;
            default:
                err(_("Fehler bein Lesen der Felder"), 1);
                break;
            }
        } 
        while ( rc == Sqlite.ROW );
        return felder;  
    }
    
    public int[] jahreLesen () {
    //Jahre aus Datenbank lesen
    err(_("DB Jahre werden gelesen"), 0);
        Statement stmt;
        int[] jahre = {};
        
        rc = db.prepare_v2("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name", -1, out stmt, null);
        if ( rc == 1 ) {
            err(_("SQL error jahreLesen: ") + rc.to_string() + db.errmsg(), 1);
            rc = 0;
        }
        do {
            rc = stmt.step();
            switch ( rc ) {
            case Sqlite.DONE:
                break;
            case Sqlite.ROW:
                if (stmt.column_int(0).to_string() == stmt.column_text(0)){
                    jahre += stmt.column_int(0);
                }
                break;
            default:
                err(_("Fehler beim Einlesen der Jahre"), 1);
                break;
            }
        } 
        while ( rc == Sqlite.ROW );
        return jahre;
    
    }
    
    public aktion[] aktionLesen(string feld, int jahr) {
    //Aktionen aus Datenbank lesen
    err(_("DB Aktionen werden gelesen"), 0);
        Statement stmt;
        aktion[] aktionen = {};
        aktion rueckAktion = aktion();
        string efeld = "";
        psMittel[] psm = {};
        double[] kosten = {};
        int i = 0;
        
        if (feld != ""){efeld =  " where feld = " + feld;}
        rc = db.prepare_v2("SELECT * FROM '"+ jahr.to_string() + "'" + efeld + " ORDER BY datum", -1, out stmt, null);
        if ( rc == 1 ) {
            err(_("SQL error aktionLesen: %d, %s\n") + db.errmsg(), 1);
            rc = 0;
        }
        do {
            rc = stmt.step();
            switch ( rc ) {
            case Sqlite.DONE:
                break;
                case Sqlite.ROW:
                    rueckAktion.id = stmt.column_int(0);
                    rueckAktion.feld = int.parse(stmt.column_text(1));
                    rueckAktion.aktion = stmt.column_text(2);
                    rueckAktion.datum = new DateTime.utc (int.parse(stmt.column_text(3)[0:4]), int.parse(stmt.column_text(3)[5:7]), int.parse(stmt.column_text(3)[8:10]), 0, 0, 0);
                    kosten.length = 0;
                    foreach(string s in stmt.column_text(4).split(";")){
                        kosten += double.parse(s.replace(",", "."));
                    }
                    rueckAktion.kosten = kosten;
                    rueckAktion.par1 = stmt.column_text(5);
                    if (stmt.column_text(2) == "Pflanzenschutz"){
                    //Wenn Pflanzenschutz
                    psm.length = 0;
                    i = 0;
                        if (stmt.column_text(5).split(";").length <= 0){
                            foreach (string pm in stmt.column_text(6).split(";")){
                                if (pm.split("|").length >= 3){
                                    psm += psMittel.from_string(pm);
                                }
                            }
                        }else{
                        //Wenn alte Daten
                            foreach(string pm in stmt.column_text(5).split(";")){
                                if(pm != ""){
                                    psm += psMittel.from_string(pm + "||" + stmt.column_text(i + 7));
                                    i += 1;
                                }
                            }
                        }
                    }
                    rueckAktion.psMittel = psm;
                    rueckAktion.par2 = stmt.column_text(6);
                    rueckAktion.par3 = stmt.column_double(7);
                    rueckAktion.par4 = stmt.column_double(8);
                    rueckAktion.par5 = stmt.column_double(9);
                    rueckAktion.par6 = stmt.column_double(10);
                    rueckAktion.par7 = stmt.column_double(11);
                    rueckAktion.par8 = stmt.column_double(12);
                    rueckAktion.par9 = stmt.column_double(13);
                    rueckAktion.bbch = stmt.column_int(14);
                    rueckAktion.schalter = stmt.column_int(15);
                    rueckAktion.flaeche = stmt.column_double(16);
                    rueckAktion.kommentar = stmt.column_text(17);
                    rueckAktion.anwender = stmt.column_text(18);
                    //Wenn kein Entzug bei Ernte vorliegt (alte Daten)
                    if (rueckAktion.aktion == "Ernte" & rueckAktion.par1 == ""){rueckAktion.par1 = "0;0;0;0;0;0";}
                    //Wenn keine Masseinheit für Dünger vorliegt (alte Daten)
                    if (rueckAktion.aktion == "Organische Düngung" & rueckAktion.par2 == ""){rueckAktion.par2 = "m³";}
                    if (rueckAktion.aktion == "Mineralische Düngung" & rueckAktion.par2 == ""){rueckAktion.par2 = "kg";}
                    //Wenn kein Wert für Hauptfrucht vorliegt (alte Daten)
                    if (rueckAktion.aktion == "Saat" & rueckAktion.par4 < 0){rueckAktion.par4 = 1;}
                    
                    aktionen += rueckAktion;
                    break;
                default:
                    err(_("Fehler beim Einlesen der Aktionen"), 1);
                    break;
            }
        } 
        while ( rc == Sqlite.ROW );
        return aktionen;
    }
    
    public void betriebAnlegen(string dbdatei){
    //Neuen leeren Betrieb anlegen
    err(_("DB Neuer Betrieb wird angelegt: ") + dbdatei, 0);
        Statement stmt;
        var e = File.new_for_path(dbdatei);
        if (e.query_exists ()) {
            Dialog dialog = new MessageDialog(null,
                                                Gtk.DialogFlags.MODAL,
                                                Gtk.MessageType.ERROR,
                                                Gtk.ButtonsType.OK, 
                                                _("Ein Betrieb mit diesem Namen existiert bereits"));
            dialog.run();
            dialog.destroy();
        }else{   
            rc = db.open (dbdatei, out db);
            if ( rc == 1){
                err(_("Datenbank konnte nicht angelegt werden: ") + db.errmsg(), 1);
            }
            
            rc = db.prepare_v2("CREATE TABLE IF NOT EXISTS feldjahr (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                                     jahr INTEGER,
                                                                     name TEXT,
                                                                     art TEXT,
                                                                     groesse DOUBLE,
                                                                     pacht DOUBLE,
                                                                     dungn DOUBLE, 
                                                                     dungp DOUBLE, 
                                                                     dungk DOUBLE, 
                                                                     dungm DOUBLE, 
                                                                     dungs DOUBLE, 
                                                                     dungc DOUBLE)" , -1, out stmt);
            rc = stmt.step();
            rc = db.prepare_v2("CREATE TABLE IF NOT EXISTS mittel (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                                                   feldid TEXT,
                                                                   jahr INTEGER, 
                                                                   art INTEGER, 
                                                                   name TEXT, 
                                                                   par3 TEXT, 
                                                                   par4 DOUBLE, 
                                                                   par5 DOUBLE, 
                                                                   par6 DOUBLE, 
                                                                   par7 DOUBLE, 
                                                                   par8 DOUBLE, 
                                                                   par9 DOUBLE)" , -1, out stmt);
            rc = stmt.step();
            
            if ( rc == 1){
                err(_("Datentabellen konnten nicht angelegt werden: ") + db.errmsg());
            }
        }
    }
    public void mittelHinzufuegen(mittel name){
    //Neues Mittel hinzufügen
    err(_("DB Neues Mittel wird angelegt: ") + name.name,0);
        Statement stmt;
        //Wenn keine id mitgegeben wird nächstgrößere berechnen
        string eid = "(SELECT 1 + max(id) FROM 'mittel')";
        if (name.id >-1){
            eid= "'" + name.id.to_string()+ "'";
        }
        
        rc = db.prepare_v2("INSERT INTO 'mittel' VALUES ("
                            + eid + ",'"
                            + name.par2 + "','"
                            + name.jahr.to_string() + "','"
                            + name.art.to_string()  + "' ,'"
                            + name.name + "' ,'"
                            + name.par3 + "' ,'"
                            + name.par4.to_string() + "' ,'"
                            + name.par5.to_string() + "' ,'"
                            + name.par6.to_string() + "' ,'"
                            + name.par7.to_string() + "' ,'"
                            + name.par8.to_string() + "' ,'"
                            + name.par9.to_string() + "'  )", -1, out stmt, null);
        rc = stmt.step();
        if ( rc != 0 ) {
            err(_("SQL error mittelHinzufuegen: ") + db.errmsg(), 1);
            rc = 0;
        }
    }
    
    public void feldHinzufuegen(feld name){
    //Neues Feld hinzufügen
    err(_("DB Neues Feld wird angelegt: ") + name.name, 0);
        Statement stmt;
        string artVorfrucht = "";
        
        //Wenn keine id mitgegeben wird nächstgrößere berechnet
        string eid = "(SELECT 1 + max(id) FROM 'feldjahr')";
        if (name.id > -1){
            eid= "'" + name.id.to_string()+ "'";
        }
        //Art und Vorfrucht zusammenschreiben
        artVorfrucht = name.art + ";" + name.vorfrucht;
        rc = db.prepare_v2("INSERT INTO 'feldjahr' VALUES ("
                            + eid + ",'"
                            + name.jahr.to_string() + "','"
                            + name.name + "' ,'"
                            + artVorfrucht  + "' ,'"
                            + name.groesse.to_string() + "' ,'"
                            + name.pacht.to_string() + "' ,'"
                            + name.dungn.to_string() + "' ,'"
                            + name.dungp.to_string() + "' ,'"
                            + name.dungk.to_string() + "' ,'"
                            + name.dungm.to_string() + "' ,'"
                            + name.dungs.to_string() + "' ,'"
                            + name.dungc.to_string() + "'  )", -1, out stmt, null);
        rc = stmt.step();
        
        
        if ( rc != 0 ) {
            err(_("SQL error feldHinzufuegen: ") + db.errmsg(), 1);
            rc = 0;
        }
    }
    
    public void aktionHinzufuegen(aktion name, int jahr){
    //Neue Aktion hinzufügen
    err(_("DB Neue Aktion wird angelegt: ") + name.aktion, 0);
        Statement stmt;
        string[] ps = {};
        
        
        //Wenn keine id mitgegeben wird nächstgrößere berechnen
        string eid = "(SELECT 1 + max(ID) FROM '" + jahr.to_string() + "')";
        if (name.id >0){
            eid= "'" + name.id.to_string()+ "'";
        }
        //Wenn Pflanzenschutz psMittel als string in par2 schreiben
        if (name.aktion == "Pflanzenschutz"){
            name.par2 = "";
            foreach(psMittel psm in name.psMittel){
                ps += psm.to_string();
            }
            name.par2 = string.joinv(";", ps);
        }
        
        rc = db.prepare_v2("INSERT INTO '" + jahr.to_string() + "' VALUES ("
                            + eid + ",'"
                            + name.feld.to_string() + "','"
                            + name.aktion + "' ,'"
                            + name.datum.to_string() + "' ,'"
                            + doubleparse(name.kosten[0]) + ";" + doubleparse(name.kosten[1]) + ";" + doubleparse(name.kosten[2]) + "' ,'"
                            + name.par1 + "' ,'"
                            + name.par2 + "' ,'"
                            + name.par3.to_string() + "' ,'"
                            + name.par4.to_string() + "' ,'"
                            + name.par5.to_string() + "' ,'"
                            + name.par6.to_string() + "' ,'"
                            + name.par7.to_string() + "' ,'"
                            + name.par8.to_string() + "' ,'"
                            + name.par9.to_string() + "' ,'"
                            + name.bbch.to_string() + "' ,'"
                            + name.schalter.to_string() + "' ,'"
                            + name.flaeche.to_string() + "' ,'"
                            + name.kommentar + "' ,'"
                            + name.anwender + "'  )", -1, out stmt, null);
        rc = stmt.step();
        if ( rc == 1 ) {
            err(_("SQL error: ") + db.errmsg());
            rc = 0;
        }
    }
    
    public void aktionAendern(aktion name, int id, int jahr){
    err(_("DB Aktion wird geändert: ") + name.aktion, 0);
        Statement stmt;
        string[] ps = {};

        //Wenn Pflanzenschutz psMittel als string in par2 schreiben
        if (name.aktion == "Pflanzenschutz"){
            name.par2 = "";
            foreach(psMittel psm in name.psMittel){
                ps += psm.to_string();
            }
            name.par2 = string.joinv(";", ps);
        }
        
        rc = db.prepare_v2("UPDATE '" + jahr.to_string() + "' SET id = '" + id.to_string() + "',"
                                                        + "feld = '" + name.feld.to_string() + "',"
                                                        + "aktion = '" + name.aktion + "' ,"
                                                        + "datum = '" + name.datum.to_string() + "' ,"
                                                        + "kosten = '"+ doubleparse(name.kosten[0]) + ";" + doubleparse(name.kosten[1]) + ";" + doubleparse(name.kosten[2]) + "' ," 
                                                        + "par1 = '" + name.par1 + "' ,"
                                                        + "par2 = '" + name.par2 + "' ,"
                                                        + "par3 = '" + name.par3.to_string() + "' ,"
                                                        + "par4 = '" + name.par4.to_string() + "' ,"
                                                        + "par5 = '" + name.par5.to_string() + "' ,"
                                                        + "par6 = '" + name.par6.to_string() + "' ,"
                                                        + "par7 = '" + name.par7.to_string() + "' ,"
                                                        + "par8 = '" + name.par8.to_string() + "' ,"
                                                        + "par9 = '" + doubleparse(name.par9) + "' ,"
                                                        + "bbch = '" + name.bbch.to_string() + "' ,"
                                                        + "bbch = '" + name.schalter.to_string() + "' ,"
//                                                      + "flaeche = '" + doubleparse(name.flaeche) + "' ,"
                                                        + "flaeche = '" + name.flaeche.to_string() + "' ,"
                                                        + "kommentar = '" + name.kommentar + "' ,"
                                                        + "anwender = '" + name.anwender + "' WHERE id = '" + id.to_string() + "' ", -1, out stmt, null);
                                                          
    rc = stmt.step();
    if ( rc == 1){
            err(_("Datentabellen konnten nicht angelegt werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    
    }
    
    public void feldAendern(feld name){
    err(_("DB Feld wird geändert: ") + name.name, 0);
        Statement stmt;
        string artVorfrucht = "";
        //Art und Vorfrucht zusammenschreiben
        artVorfrucht = name.art + ";" + name.vorfrucht;
        rc = db.prepare_v2("UPDATE 'feldjahr' SET id = '" + name.id.to_string() + "',"
                                                        + "jahr = '" + name.jahr.to_string() + "',"
                                                        + "name = '" + name.name + "' ,"
                                                        + "art = '" + artVorfrucht + "' ,"
                                                        + "groesse = '"+ name.groesse.to_string() +"' ," 
                                                        + "pacht = '" + name.pacht.to_string() + "' ,"
                                                        + "dungn = '" + name.dungn.to_string() + "' ,"
                                                        + "dungp = '" + name.dungp.to_string() + "' ,"
                                                        + "dungk = '" + name.dungk.to_string() + "' ,"
                                                        + "dungm = '" + name.dungm.to_string() + "' ,"
                                                        + "dungs = '" + name.dungs.to_string() + "' ,"
                                                        + "dungc = '" + name.dungc.to_string() + "' WHERE id = '" + name.id.to_string() + "' ", -1, out stmt, null);
    rc = stmt.step();
    if ( rc == 1){
            err(_("Feld konnten nicht geändert werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    
    }
    
    public void mittelAendern(mittel name){
    err(_("DB Mittel wird geändert") + name.name, 0);
        
        Statement stmt;
        rc = db.prepare_v2("UPDATE 'mittel' SET id = '" + name.id.to_string() + "',"
                                                        + "feldid = '" + name.par2 + "',"
                                                        + "jahr = '" + name.jahr.to_string() + "' ,"
                                                        + "art = '" + name.art.to_string() + "' ,"
                                                        + "name = '"+ name.name +"' ," 
                                                        + "par3 = '" + name.par3 + "' ,"
                                                        + "par4 = '" + name.par4.to_string() + "' ,"
                                                        + "par5 = '" + name.par5.to_string() + "' ,"
                                                        + "par6 = '" + name.par6.to_string() + "' ,"
                                                        + "par7 = '" + name.par7.to_string() + "' ,"
                                                        + "par8 = '" + name.par8.to_string() + "' ,"
                                                        + "par9 = '" + name.par9.to_string() + "' WHERE id = '" + name.id.to_string() + "' ", -1, out stmt, null);
                                                          
    rc = stmt.step();
    if ( rc == 1){
            err(_("DB Mittel konnte nicht geändert werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    
    }
    
    public void erntejahrHinzufuegen(int ejahr){
    //Neue Erntejahrtabelle anlegen
    err(_("DB Neues Erntejahr wird angelegt: ") + ejahr.to_string(), 0);

        int rc = 0;
        Statement stmt;
        
        rc = db.prepare_v2("CREATE TABLE IF NOT EXISTS '" + ejahr.to_string() + "' (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                                                    feld TEXT,
                                                                                    aktion TEXT, 
                                                                                    datum TEXT, 
                                                                                    kosten TEXT, 
                                                                                    par1 TEXT, 
                                                                                    par2 TEXT, 
                                                                                    par3 DOUBLE, 
                                                                                    par4 DOUBLE, 
                                                                                    par5 DOUBLE, 
                                                                                    par6 DOUBLE,
                                                                                    par7 DOUBLE, 
                                                                                    par8 DOUBLE, 
                                                                                    par9 DOUBLE, 
                                                                                    bbch INTEGER, 
                                                                                    schalter INTEGER, 
                                                                                    flaeche DOUBLE, 
                                                                                    kommentar TEXT, 
                                                                                    anwender TEXT)" , -1, out stmt);
        rc = stmt.step();
        if ( rc == 1){
            err(_("DB Datentabellen konnten nicht angelegt werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    }
    
    public void feldEntfernen(int feldid){
    //Feld entfernen
    err(_("DB Feld wird entfernt"), 0);

        Statement stmt;
        rc = db.prepare_v2("DELETE FROM 'feldjahr' WHERE id = '" + feldid.to_string() + "'", -1, out stmt, null);
        rc = stmt.step();
        if ( rc == 1 ) {
            err(_("DB Feld konnte nicht entfernt werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    }

    public void mittelEntfernen(int mittelid){
    //Mittel entfernen
    err(_("DB Mittel wird entfernt"), 0);

        Statement stmt;
        rc = db.prepare_v2("DELETE FROM 'mittel' WHERE id = '" + mittelid.to_string() + "'", -1, out stmt, null);
        rc = stmt.step();
        if ( rc == 1 ) {
            err(_("DB Mittel konnte nicht entfernt werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    }
    
    public void aktionEntfernen(int aktionid){
    //Aktion entfernen
    err(_("DB Aktion wird entfernt"), 0);

        Statement stmt;
        rc = db.prepare_v2("DELETE FROM '" + erntejahr.to_string() + "' WHERE id = '" + aktionid.to_string() + "'", -1, out stmt, null);
        rc = stmt.step();
        if ( rc == 1 ) {
            err(_("DB Aktion konnte nicht entfernt werden: ") + db.errmsg(), 1);
            rc = 0;
        }
    }


}
