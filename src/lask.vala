using Gtk;

namespace lask{
    public struct feld{
        public int id;
        public int jahr;
        public string name;
        public string art;
        public string frucht;
        public string vorfrucht;
        public double groesse;
        public double pacht;
        public double dungn;
        public double dungp;
        public double dungk;
        public double dungm;
        public double dungs;
        public double dungc;
    }
    
    public struct aktion{
        public int id;
        public int feld;
        public string aktion;
        public DateTime datum;
        public double[] kosten;
        public psMittel[] psMittel;
        public string par1;
        public string par2;
        public double par3;
        public double par4;
        public double par5;
        public double par6;
        public double par7;
        public double par8;
        public double par9;
        public int bbch;
        public int schalter;
        public double flaeche;
        public string kommentar;
        public string anwender; 

        public string to_string(){
        //Als String ausgeben
            var builder = new StringBuilder ();
            builder.append (id.to_string() + "¶");
            builder.append (feld.to_string() + "¶");
            builder.append (aktion + "¶");
            builder.append (datum.to_string() + "¶");
            builder.append (doubleparse(kosten[0], 4) + "|" + doubleparse(kosten[1], 4) + "|" + doubleparse(kosten[2], 4) + "¶");
            builder.append (par1 + "¶");
            //Wenn Pflanzenschutz dan in par2 schreiben
            if (aktion == "Pflanzenschutz"){
                foreach (psMittel psm in psMittel){
                    builder.append (psm.to_string() + ";");
                }
                builder.overwrite(builder.len - 1, "¶");
            }else{
                builder.append (par2 + "¶");
            }
            builder.append (doubleparse(par3, 4) + "¶");
            builder.append (doubleparse(par4, 4) + "¶");
            builder.append (doubleparse(par5, 4) + "¶");
            builder.append (doubleparse(par6, 4) + "¶");
            builder.append (doubleparse(par7, 4) + "¶");
            builder.append (doubleparse(par8, 4) + "¶");
            builder.append (doubleparse(par9, 4) + "¶");
            builder.append (bbch.to_string() + "¶");
            builder.append (schalter.to_string() + "¶");
            builder.append (doubleparse(flaeche) + "¶");
            builder.append (kommentar + "¶");
            builder.append (anwender + "¶");
           
        return builder.str;
        }

        public aktion.from_string(string str){
        //Aus String erzeugen
            string[] tmpMittel = str.split("¶");
            double[] tmpKosten = {};
            psMittel[] tmpPs = {};
            
            id = int.parse(tmpMittel[0]);
            feld = int.parse(tmpMittel[1]);
            aktion = tmpMittel[2];
            datum = new DateTime.utc (int.parse(tmpMittel[3][0:4]), int.parse(tmpMittel[3][5:7]), int.parse(tmpMittel[3][8:10]), 0, 0, 0);
            foreach (string s in tmpMittel[4].split("|")){
                tmpKosten += double.parse(s.replace(",", "."));
            }
            kosten = tmpKosten;
            par1 = tmpMittel[5];
            if (aktion == "Pflanzenschutz"){
                foreach (string s in tmpMittel[6].split(";")){
                    tmpPs += lask.psMittel.from_string(s);
                }
                this.psMittel = tmpPs;
            }else{
                par2 = tmpMittel[6];
            }
            par3 = double.parse(tmpMittel[7].replace(",", "."));
            par4 = double.parse(tmpMittel[8].replace(",", "."));
            par5 = double.parse(tmpMittel[9].replace(",", "."));
            par6 = double.parse(tmpMittel[10].replace(",", "."));
            par7 = double.parse(tmpMittel[11].replace(",", "."));
            par8 = double.parse(tmpMittel[12].replace(",", "."));
            par9 = double.parse(tmpMittel[13].replace(",", "."));
            bbch = int.parse(tmpMittel[14]);
            schalter = int.parse(tmpMittel[15]);
            flaeche = double.parse(tmpMittel[16].replace(",", "."));
            kommentar = tmpMittel[17];
            anwender = tmpMittel[18];
        }
        
        public double[] get_kosten(){
        //Kosten der Aktion pro ha für Auswertung
            double[] haKosten = {0, 0, 0};
            
            if (aktion == "Saat"){
                haKosten[0] = psMittel[0].preis;
                if (psMittel[1].preis <= 0){
                    haKosten[1] = config.kosten.par4;
                }else{
                    haKosten[1] = psMittel[1].preis;
                }
            }else if(aktion == "Pflanzenschutz"){
                foreach (psMittel p in psMittel){
                    haKosten[0] += p.menge * p.preis;
                    haKosten[1] = config.kosten.par5;
                }
            }else if(aktion == "Organische Düngung" || aktion == "Mineralische Düngung"){
                if (par2 == "dt"){
                    haKosten[0] = psMittel[0].preis;
                }else{
                    haKosten[0] = psMittel[0].preis;
                }
                if (aktion == "Organische Düngung"){
                    haKosten[1] = config.kosten.par5;
                }else if (aktion == "Mineralische Düngung"){
                    haKosten[1] = config.kosten.par6;
                }
            }else if(aktion == "Bodenbearbeitung"){
                haKosten[1] = psMittel[0].preis;
            }else if(aktion == "Ernte"){
                haKosten[1] = config.kosten.par7;
            }
        return haKosten;
        }
    }
    
    public struct mittel{
        public int id;
        public double jahr;
        public int art;
        public string name;
        public string par2;
        public string par3;
        public double par4;
        public double par5;
        public double par6;
        public double par7;
        public double par8;
        public double par9;
    }
    
    public struct druckdaten{
        public feld druckfeld;
        public aktion[] druckaktionen;
    }

    public struct psMittel{
    //Pflantenschutzmittel
        public string mittel;
        public string art;
        public double menge;
        public double preis;
        public string id;
        
        public psMittel(){
            mittel = "";
            art = "";
            menge = 0.0;
            preis = 0.0;
            id = "";
        }
        
        public psMittel.from_string(string str){
        //Aus string erzeugen
            if (str.split("|").length == 5){
                mittel = str.split("|")[0];
                art = str.split("|")[1];
                menge = double.parse(str.split("|")[2].replace(",", "."));
                preis = double.parse(str.split("|")[3].replace(",", "."));
                id = str.split("|")[4];
            }else{
                mittel = str.split("|")[0];
                art = str.split("|")[1];
                menge = double.parse(str.split("|")[2].replace(",", "."));
                preis = double.parse(str.split("|")[3].replace(",", "."));
                id = "";
                err(_("psMittel hat zu wenig Einträge"), 0);
            }
        }

        public string to_string(){
        //Als string für Datenbank ausgeben
            return mittel + "|" + art + "|" + doubleparse(menge, 3) + "|" + doubleparse(preis, 2) + "|" + id.to_string();
        }
    }
    
    public enum mobilArt{
        NEU,
        ANDERS,
        WEG;
    }

    
public string doubleparse(double dble, int nk=2){
    string wert;
	string rueckwert;
    int minus = 0;
    
    if(dble <= -1){
        minus = 1;
        wert = GLib.Math.round(0 - (dble * (Math.pow(10, nk)))).to_string();
    }else{
        wert = GLib.Math.round(dble * (Math.pow(10, nk))).to_string();
    }
	if(wert.length <= nk-1){
		wert = GLib.Math.round(dble * (Math.pow(10, nk + 1))).to_string();
		wert += string.nfill(nk - wert.length, '0');
		rueckwert = wert.splice(-nk, -nk, "0,");
	}else if(wert.length == nk){
		rueckwert = wert.splice(-nk, -nk, "0,");
	}else{
		rueckwert = wert.splice(-nk, -nk, ",");
	}
	if(nk == 0){
		rueckwert= GLib.Math.round(dble).to_string();
	}
    if(minus == 1){
        rueckwert = "-" + rueckwert;
    }
	return rueckwert;
}
    
    public void err(string message="", int stufe=1){
        if(debuging == 1){
            string[] art = {_("Meldung"),
                            _("Fehler")};
            print(art[stufe] +": " + message + "\n");
        }
    }
    
    public string datum_berechnen(string d){
    //Sinnvolles Datum aus d bilden    
        string datum = d + "          ";
        string[] ds = d.split_set(".,-:/;", 4);
        
        if (int.parse(datum) <= 0){
            datum = new DateTime.now_local().add_days(int.parse(datum)).format("%d.%m.%Y");
        }else if (d[0:1] == "+"){
            datum = new DateTime.now_local().add_days(int.parse(ds[0])).format("%d.%m.%Y");
        }else if (int.parse(ds[0]) <= 31 && int.parse(ds[1]) <= 12 && int.parse(ds[2]) <= erntejahr + 1){
            datum = new DateTime.local(int.parse(ds[2]),int.parse(ds[1]),int.parse(ds[0]),0,0,0).format("%d.%m.%Y");
        }else{
            datum = new DateTime.now_local().format("%d.%m.%Y");
        }
    return datum;
    }
}
