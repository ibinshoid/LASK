using Gtk;
using lask;

public class cfeldEdit : GLib.Object{
//Betriebsdaten bearbeiten    
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window4;
    private Button button9;
    private Button button10;
    private Notebook notebook1;
    private Label label38;
    private Entry entry3;
    private Entry entry4;
    private Grid grid21;
    private Grid grid41;
    private CheckButton checkbutton1;
    private SpinButton spinbutton14;
    private SpinButton spinbutton15;
    private SpinButton spinbutton16;
    private SpinButton spinbutton17;
    private SpinButton spinbutton18;
    private SpinButton spinbutton19;
    private SpinButton spinbutton20;
    private SpinButton spinbutton21;
    private ComboBoxText combobox7;
    private MainLoop loop = new MainLoop ();

    public cfeldEdit(){
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von feldedit.vala geladen"),0);
        window4 = builder.get_object ("window4") as Window;
        button9 = builder.get_object ("button9") as Button;
        button10 = builder.get_object ("button10") as Button;
        notebook1 = builder.get_object ("notebook1") as Notebook;
        label38 = builder.get_object ("label38") as Label;
        entry3 = builder.get_object ("entry3") as Entry;
        entry4 = builder.get_object ("entry4") as Entry;
        grid21 = builder.get_object ("grid21") as Grid;
        grid41 = builder.get_object ("grid41") as Grid;
        checkbutton1 = new CheckButton.with_label("Weidehaltung");
        spinbutton14 = builder.get_object ("spinbutton14") as SpinButton;
        spinbutton15 = builder.get_object ("spinbutton15") as SpinButton;
        spinbutton16 = builder.get_object ("spinbutton16") as SpinButton;
        spinbutton17 = builder.get_object ("spinbutton17") as SpinButton;
        spinbutton18 = builder.get_object ("spinbutton18") as SpinButton;
        spinbutton19 = builder.get_object ("spinbutton19") as SpinButton;
        spinbutton20 = builder.get_object ("spinbutton20") as SpinButton;
        spinbutton21 = builder.get_object ("spinbutton21") as SpinButton;
        combobox7 = builder.get_object ("combobox7") as ComboBoxText;
        this.window4.set_modal(true);
        this.checkbutton1.show();
        //Ereignisse verbinden
        button9.clicked.connect(()=>{this.loop.quit();});
        button10.clicked.connect(()=>{this.window4.destroy();});
        combobox7.changed.connect(combobox7_changed);
    }
    
    public void fensterBauen(){
    //Fenster bauen
        this.notebook1.set_show_tabs(false);
        this.notebook1.set_current_page(4);
        this.grid41.destroy();
    }
    
    public feld feldNeu(){
    //Neues Feld bauen
        feld rueckFeld = feld();
        
        this.window4.set_title(_("neues Feld"));
        fensterBauen();
        this.loop.run();
        rueckFeld.id = -1;
        rueckFeld.jahr = erntejahr;
        rueckFeld.name=entry3.get_text();
        rueckFeld.art=combobox7.get_active_text();
        rueckFeld.vorfrucht=entry4.get_text();
        rueckFeld.groesse=spinbutton14.get_value();
        rueckFeld.pacht=spinbutton15.get_value();
        rueckFeld.dungn=spinbutton16.get_value();
        rueckFeld.dungp=spinbutton17.get_value();
        rueckFeld.dungk=spinbutton18.get_value();
        rueckFeld.dungm=spinbutton19.get_value();
        rueckFeld.dungs=spinbutton20.get_value();
        rueckFeld.dungc=spinbutton21.get_value();
        this.window4.destroy();
    return rueckFeld;
    }
    
    public feld feldAendern(feld f){
    //Feld ändern
        feld rueckFeld = feld();
        Entry entry;

        this.window4.set_title(_("Feld ändern"));
        fensterBauen();
        //Fenster füllen
        this.entry3.set_text(f.name);
        if (f.art == "Grünland"){
            checkbutton1.set_active(bool.parse(f.vorfrucht));
            combobox7_changed();
        }else{
            this.entry4.set_text(f.vorfrucht);
        }
        entry = this.combobox7.get_child() as Entry;
        entry.set_text(f.art);
        this.spinbutton14.set_value(f.groesse);
        this.spinbutton15.set_value(f.pacht);
        this.spinbutton16.set_value(f.dungn);
        this.spinbutton17.set_value(f.dungp);
        this.spinbutton18.set_value(f.dungk);
        this.spinbutton19.set_value(f.dungm);
        this.spinbutton20.set_value(f.dungs);
        this.spinbutton21.set_value(f.dungc);
        //rueckFeld bauen
        this.loop.run();
        rueckFeld.id = f.id;
        rueckFeld.jahr = erntejahr;
        rueckFeld.name=entry3.get_text();
        rueckFeld.art=combobox7.get_active_text();
        if (rueckFeld.art == "Grünland"){
            rueckFeld.vorfrucht=checkbutton1.get_active().to_string();
        }else{
            rueckFeld.vorfrucht=entry4.get_text();
        }
        rueckFeld.groesse=spinbutton14.get_value();
        rueckFeld.pacht=spinbutton15.get_value();
        rueckFeld.dungn=spinbutton16.get_value();
        rueckFeld.dungp=spinbutton17.get_value();
        rueckFeld.dungk=spinbutton18.get_value();
        rueckFeld.dungm=spinbutton19.get_value();
        rueckFeld.dungs=spinbutton20.get_value();
        rueckFeld.dungc=spinbutton21.get_value();
        this.window4.destroy();
    return rueckFeld;
    }

    public void combobox7_changed(){
    //Feldart wurde geändert
        if (combobox7.get_active_text() == "Grünland"){
            this.label38.set_label("");
            this.grid21.remove(this.entry4);
            this.grid21.attach_next_to(this.checkbutton1, this.label38, PositionType.RIGHT, 1, 1);
        }else{
            this.label38.set_label(_("Vorfrucht:"));
            this.grid21.remove(this.checkbutton1);
            this.grid21.attach_next_to(this.entry4, this.label38, PositionType.RIGHT, 1, 1);
        }
    }
        
}
