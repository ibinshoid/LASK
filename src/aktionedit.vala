using Gtk;
using lask;

public class caktionEdit : GLib.Object{
//Betriebsdaten bearbeiten    
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window6;
    public Window window8;
    public Window windowpsm;
    private Button button15;
    private Button button16;
    private Button button17;
    private Button button18;
    private Button button19;
    private Button buttonpsm1;
    private Button buttonpsm2;
    private Button buttonpsm3;
    private Button buttonpsm4;
    private Entry entry5;
    private Entry entry8;
    private Entry entrypsm1;
    private Label label74;
    private Label label83;
    private Label label100;
    private Label label101;
    private Label label102;
    private ComboBox comboboxpsm1;
    private ComboBoxText combobox14;
    private ComboBoxText combobox15;
    private ComboBoxText combobox16;
    private ComboBoxText combobox17;
    private ComboBoxText combobox18;
    private ComboBoxText combobox19;
    private SpinButton spinbutton23;
    private SpinButton spinbutton24;
    private SpinButton spinbutton25;
    private SpinButton spinbutton26;
    private SpinButton spinbutton27;
    private SpinButton spinbutton28;
    private SpinButton spinbutton29;
    private SpinButton spinbutton30;
    private SpinButton spinbutton31;
    private SpinButton spinbutton32;
    private SpinButton spinbutton33;
    private SpinButton spinbutton39;
    private SpinButton spinbutton40;
    private SpinButton spinbutton41;
    private SpinButton spinbutton42;
    private SpinButton spinbutton43;
    private SpinButton spinbutton44;
    private SpinButton spinbutton45;
    private SpinButton spinbutton46;
    private SpinButton spinbutton47;
    private SpinButton spinbutton48;
    private SpinButton spinbutton54;
    private SpinButton spinbuttonpsm1;
    private SpinButton spinbuttonpsm2;
    private TreeStore treestore2;
    private TextView textview2;
    private CheckButton checkbutton1;
    private Notebook notebook2;
    private MainLoop loop = new MainLoop ();
    private MainLoop loop2 = new MainLoop ();
    private string masseinheit = "";
    private aktion rueckAktion = aktion();
    private Popover popover;
    private Calendar kalender;
    private Calendar kalender2;
    private Dialog dialogpsm;
    private ListBox listboxpsm1;
    private psMittel[] psm = {psMittel(), psMittel(), psMittel()};

    public caktionEdit(){
    //Konstructor
    err(uiVerzeichnis + _("/lask.ui wird von aktionedit.vala geladen"),0);
        window6 = builder.get_object ("window6") as Window;
        window8 = builder.get_object ("window8") as Window;
        button15 = builder.get_object ("button15") as Button;
        button16 = builder.get_object ("button16") as Button;
        button17 = builder.get_object ("button17") as Button;
        button18 = builder.get_object ("button18") as Button;
        button19 = builder.get_object ("button19") as Button;
        button19 = builder.get_object ("button19") as Button;
        buttonpsm1 = builder.get_object ("buttonpsm1") as Button;
        buttonpsm2 = builder.get_object ("buttonpsm2") as Button;
        buttonpsm3 = builder.get_object ("buttonpsm3") as Button;
        buttonpsm4 = builder.get_object ("buttonpsm4") as Button;
        entry5 = builder.get_object ("entry5") as Entry;
        entry8 = builder.get_object ("entry8") as Entry;
        entrypsm1 = builder.get_object ("entrypsm1") as Entry;
        label74 = builder.get_object ("label74") as Label;
        label83 = builder.get_object ("label83") as Label;
        label100 = builder.get_object ("label100") as Label;
        label101 = builder.get_object ("label101") as Label;
        label102 = builder.get_object ("label102") as Label;
        treestore2 = builder.get_object ("treestore2") as TreeStore;
        combobox14 = builder.get_object ("combobox14") as ComboBoxText;
        combobox15 = builder.get_object ("combobox15") as ComboBoxText;
        combobox16 = builder.get_object ("combobox16") as ComboBoxText;
        combobox17 = builder.get_object ("combobox17") as ComboBoxText;
        combobox18 = builder.get_object ("combobox18") as ComboBoxText;
        combobox19 = builder.get_object ("combobox19") as ComboBoxText;
        comboboxpsm1 = builder.get_object ("comboboxpsm1") as ComboBox;
        spinbutton23 = builder.get_object ("spinbutton23") as SpinButton;
        spinbutton24 = builder.get_object ("spinbutton24") as SpinButton;
        spinbutton25 = builder.get_object ("spinbutton25") as SpinButton;
        spinbutton26 = builder.get_object ("spinbutton26") as SpinButton;
        spinbutton27 = builder.get_object ("spinbutton27") as SpinButton;
        spinbutton28 = builder.get_object ("spinbutton28") as SpinButton;
        spinbutton29 = builder.get_object ("spinbutton29") as SpinButton;
        spinbutton30 = builder.get_object ("spinbutton30") as SpinButton;
        spinbutton31 = builder.get_object ("spinbutton31") as SpinButton;
        spinbutton32 = builder.get_object ("spinbutton32") as SpinButton;
        spinbutton33 = builder.get_object ("spinbutton33") as SpinButton;
        spinbutton39 = builder.get_object ("spinbutton39") as SpinButton;
        spinbutton40 = builder.get_object ("spinbutton40") as SpinButton;
        spinbutton41 = builder.get_object ("spinbutton41") as SpinButton;
        spinbutton42 = builder.get_object ("spinbutton42") as SpinButton;
        spinbutton43 = builder.get_object ("spinbutton43") as SpinButton;
        spinbutton44 = builder.get_object ("spinbutton44") as SpinButton;
        spinbutton45 = builder.get_object ("spinbutton45") as SpinButton;
        spinbutton46 = builder.get_object ("spinbutton46") as SpinButton;
        spinbutton47 = builder.get_object ("spinbutton47") as SpinButton;
        spinbutton48 = builder.get_object ("spinbutton48") as SpinButton;
        spinbutton54 = builder.get_object ("spinbutton54") as SpinButton;
        spinbuttonpsm1 = builder.get_object ("spinbuttonpsm1") as SpinButton;
        spinbuttonpsm2 = builder.get_object ("spinbuttonpsm2") as SpinButton;
        textview2 = builder.get_object ("textview2") as TextView;
        checkbutton1 = builder.get_object ("checkbutton1") as CheckButton;
        notebook2 = builder.get_object ("notebook2") as Notebook;
        dialogpsm = builder.get_object ("dialogpsm") as Dialog;
        listboxpsm1 = builder.get_object ("listboxpsm1") as ListBox;

        this.window6.set_modal(true);
        this.window8.set_modal(true);
        this.window8.set_transient_for(this.window6);
        //Kalender Zeugs
        this.popover = new Popover(this.entry5);
        this.popover.add(kalender = new Calendar());
        this.popover.set_modal(false);
        this.popover.show_all();
        entry5.button_release_event.connect(()=>{this.popover.popup();this.entry5.select_region(0, -1);return true;});
        entry5.focus_out_event.connect(()=>{entry5.set_text(datum_berechnen(entry5.get_text()));this.entry5.select_region(0, 0);return false;});
        this.kalender.day_selected_double_click.connect(()=>{entry5.set_text(datum_berechnen(kalender.day.to_string() + "." + (kalender.month + 1).to_string() + "." + kalender.year.to_string()));this.popover.popdown();});
        this.popover.popdown();
        //Ereignisse verbinden
        button15.clicked.connect(()=>{this.loop.quit();});
        button16.clicked.connect(()=>{this.window6.destroy();});
        button17.clicked.connect(()=>{this.label83.set_text(_("Speichern"));this.loop2.quit();});
        button18.clicked.connect(()=>{this.label83.set_text(_("Abbrechen"));this.loop2.quit();});
        buttonpsm1.clicked.connect(()=>{psmHinzufuegen(new Button());});
        buttonpsm2.clicked.connect(()=>{this.dialogpsm.response(0);});
        buttonpsm3.clicked.connect(()=>{this.dialogpsm.response(2);});
        buttonpsm4.clicked.connect(()=>{this.dialogpsm.response(1);});
        button19.clicked.connect(kostenAendern);
        combobox15.changed.connect(sortenLesen);
        combobox16.changed.connect(sorteLesen);
        combobox17.changed.connect(geraeteLesen);
        combobox18.changed.connect(duengerLesen);
    }
    
    private void fensterBauen(string art){
        this.notebook2.set_show_tabs(false);
        this.entry5.set_text(new DateTime.now_local().format("%d.%m.%Y"));
        this.entry5.select_region(0, -1);
        //Bei mehreren Feldern Teilfläche ausschalten
        if (aktFelderListe.length > 1){
            this.spinbutton23.set_visible(false);
            this.window6.set_title(art + _(" bei ")+ aktFelderListe.length.to_string() + _(" Feldern hinzufügen"));
        }else{
            this.spinbutton23.set_value(aktFeld.groesse);
            this.spinbutton23.get_adjustment().set_upper(aktFeld.groesse);
            this.window6.set_title(art + _(" bei ")+ aktFeld.name + _(" hinzufügen"));
        }
        if (art == "Saat"){
            //Saat
            this.notebook2.set_current_page(0);
        }else if (art == "Bodenbearbeitung"){
            //Bodenbearbeitung
            this.notebook2.set_current_page(3);
        }else if (art == "Pflanzenschutz"){
            //Pflanzenschutz
            this.notebook2.set_current_page(2);
        }else if (art == "Organische Düngung"){
            //Organische Düngung
            this.notebook2.set_current_page(4);
        }else if (art == "Mineralische Düngung"){
            //Mineralische Düngung
            this.notebook2.set_current_page(4);
        }else if (art == "Ernte"){
            //Ernte
            this.notebook2.set_current_page(1);
            foreach (mittel m in laskDb.mittelLesen(erntejahr)){
                if (m.art == 6 && m.name == aktFeld.vorfrucht){
                    spinbutton28.set_value(m.par4);
                    spinbutton29.set_value(m.par5);
                    spinbutton30.set_value(m.par6);
                    spinbutton31.set_value(m.par7);
                    spinbutton32.set_value(m.par8);
                    spinbutton33.set_value(m.par9);
                }
            }
        }else if (art == "Weidegang"){
            this.notebook2.set_current_page(5);
            this.entry8.set_text(new DateTime.now_local().format("%d.%m.%Y"));
            //Kalender Zeugs
            Popover popover2 = new Popover(this.entry8);
            popover2.add(kalender2 = new Calendar());
            popover2.show_all();
            entry8.button_release_event.connect(()=>{popover2.popup();this.entry8.select_region(0, -1);return true;});
            entry8.focus_out_event.connect(()=>{entry8.set_text(datum_berechnen(entry8.get_text()));this.entry8.select_region(0, 0);return false;});
            kalender2.day_selected_double_click.connect(()=>{entry8.set_text(datum_berechnen(kalender2.day.to_string() + "." + (kalender2.month + 1).to_string() + "." + kalender2.year.to_string()));popover2.popdown();});
            popover2.popdown();
        }
        this.mittelLesen(art);
    }
    
    public void mittelLesen(string art){
    //Betriebsmittel in entspr. Combobox eintragen
        TreeIter iter =  TreeIter();    
        TreeIter iter2 = TreeIter();   
        string[] arten = {};
        string tmp = "";

        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 5){
            //Personen in Combobox14 eintragen
                this.combobox14.append_text(m.name);
            }else if (m.art == 1){
            //Ps. Mittel in liststore2 eintragen
                    if (m.par3 in arten){
                    }else{
                        arten += m.par3;
                        this.treestore2.append(out iter, null);
                        this.treestore2.set (iter, 0,m.par3, -1);
                    }
                    this.treestore2.append(out iter2, iter);
                    this.treestore2.set (iter2, 0,m.name, -1);
            }else if (m.art == 0){
            //Fruchtart in Combobox15 eintragen
                if (m.par3 in tmp){}
                else{
                    this.combobox15.append_text(m.par3);
                    tmp = tmp + (m.par3);
                }
            }else if (m.art == 4){
            //Geräte in Combobox17 eintragen
                this.combobox17.append_text(m.name);
            }else if (m.art == 2 || m.art == 3){
                if(art == "Organische Düngung" && m.art == 2){
                    //min. Dünger in Combobox18 eintragen
                    this.combobox18.append_text(m.name);
                }else if(art == "Mineralische Düngung" && m.art == 3){
                    //org. Dünger in liststoreOrgDung eintragen
                     this.combobox18.append_text(m.name);
                }
            }
       }
    }
    
    private void sortenLesen(){
    //Sorten in Combobox16 eintragen wenn Fruchtart in Combobox15 gewählt wurde
        this.combobox16.remove_all();
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 0 && m.par3 == combobox15.get_active_text()){
                this.combobox16.append_text(m.name);
            }
        }
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 6 && m.name == combobox15.get_active_text()){
                //Saatkosten in psm[1] eintragen
                rueckAktion.psMittel[1].preis = m.jahr / 100;
            }
        }
    }

    private void sorteLesen(){
    //Sorte lesen, wenn Combobox16 gewählt wurde
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 0 && m.name == combobox16.get_active_text()){
                //Kosten in psm[0] eintragen
                rueckAktion.psMittel[0].preis = m.jahr / 100;
            }
        }
    }

    private void geraeteLesen(){
    //Gerätedaten lesen, wenn in Combobox17 ausgewählt
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 4 && m.name == combobox17.get_active_text()){
                //Kosten in psm[0] eintragen
                rueckAktion.psMittel[0].preis = m.jahr / 100;
            }
        }
    }

    private void duengerLesen(){
    //Maßeinheit in Label74 eintragen wenn Dünger in Combobox18 gewählt wurde
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 2 || m.art == 3){
                if (m.name == combobox18.get_active_text()){
                    this.label74.set_text(_("Menge (") + m.par3 + "/ha)");
                    this.masseinheit = m.par3;
                    this.spinbutton40.set_value(m.par4);
                    this.spinbutton41.set_value(m.par5);
                    this.spinbutton42.set_value(m.par6);
                    this.spinbutton43.set_value(m.par7);
                    this.spinbutton44.set_value(m.par8);
                    this.spinbutton45.set_value(m.par9);
                    //Kosten für Düngemittel in psm[0] eintragen, weil sonst nirgends Platz ist
                    rueckAktion.psMittel[0].preis = m.jahr / 100;
                }
            }
        }
    }

    public void aktionFuellen(){
    //Neue Aktion anlegen
        string art = rueckAktion.aktion;
        
        rueckAktion.datum = new DateTime.utc (int.parse(this.entry5.get_text()[6:10]), int.parse(this.entry5.get_text()[3:5]), int.parse(this.entry5.get_text()[0:2]),0,0,0);
        rueckAktion.bbch = -1;
        rueckAktion.flaeche=this.spinbutton23.get_value();
        rueckAktion.kommentar=this.textview2.get_buffer().text;
        rueckAktion.anwender=this.combobox14.get_active_text();
        if (art == "Saat"){
            //Saat
            rueckAktion.par1=this.combobox16.get_active_text();
            rueckAktion.par2=this.combobox15.get_active_text();
            if (this.combobox19.get_active() == 1){
            //Wenn kg/ha
                rueckAktion.par3 = 0 - this.spinbutton24.get_value();
            }else{
            //Wenn Kö/m²
                rueckAktion.par3 = this.spinbutton24.get_value();
            }
            if(this.checkbutton1.get_active()){
                rueckAktion.par4=1;
            }
        }else if (art == "Bodenbearbeitung"){
            //Bodenbearbeitung
            rueckAktion.par1=this.combobox17.get_active_text();
        }else if (art == "Pflanzenschutz"){
            //Pflanzenschutz
            psm = {};
            rueckAktion.kosten[0] = 0;
            foreach (Widget b in this.listboxpsm1.get_children()){
                ListBoxRow lbr = b as ListBoxRow;
                Button tmpButton = lbr.get_child() as Button;
                psm += psMittel.from_string(tmpButton.get_tooltip_text());
                rueckAktion.kosten[0] += (psm[psm.length-1].preis * psm[psm.length-1].menge);
            }
            rueckAktion.psMittel = psm;

        }else if (art == "Organische Düngung"){
            //Organische Düngung
            rueckAktion.par1=this.combobox18.get_active_text();
            if(this.masseinheit == ""){this.masseinheit = "m³";}
            rueckAktion.par2=this.masseinheit;
            rueckAktion.par3=this.spinbutton40.get_value();
            rueckAktion.par4=this.spinbutton41.get_value();
            rueckAktion.par5=this.spinbutton42.get_value();
            rueckAktion.par6=this.spinbutton43.get_value();
            rueckAktion.par7=this.spinbutton44.get_value();
            rueckAktion.par8=this.spinbutton45.get_value();
            rueckAktion.par9=this.spinbutton39.get_value();
        }else if (art == "Mineralische Düngung"){
            //Mineralische Düngung
            rueckAktion.par1=this.combobox18.get_active_text();
            if(this.masseinheit == ""){this.masseinheit = "kg";}
            rueckAktion.par2=this.masseinheit;
            rueckAktion.par3=this.spinbutton40.get_value();
            rueckAktion.par4=this.spinbutton41.get_value();
            rueckAktion.par5=this.spinbutton42.get_value();
            rueckAktion.par6=this.spinbutton43.get_value();
            rueckAktion.par7=this.spinbutton44.get_value();
            rueckAktion.par8=this.spinbutton45.get_value();
            rueckAktion.par9=this.spinbutton39.get_value();
        }else if (art == "Ernte"){
            //Ernte
            rueckAktion.par3=this.spinbutton25.get_value();
            rueckAktion.par4=this.spinbutton27.get_value();
            rueckAktion.par5=this.spinbutton26.get_value();
            rueckAktion.par1=doubleparse(this.spinbutton28.get_value()) + ";" + doubleparse(this.spinbutton29.get_value()) + ";" + doubleparse(this.spinbutton30.get_value()) + ";" + doubleparse(this.spinbutton31.get_value()) + ";" + doubleparse(this.spinbutton32.get_value()) + ";" + doubleparse(this.spinbutton33.get_value()); 
            
        }else if (art == "Weidegang"){
            //Ernte
            rueckAktion.par1 = this.entry8.get_text();
            rueckAktion.par3=this.spinbutton54.get_value();
        }
    }

    public void aktionAnlegen(string art){
    //Neue Aktion anlegen
        this.fensterBauen(art);
        this.rueckAktion = aktion();
        this.rueckAktion.id = -1;
        this.rueckAktion.aktion = art;
        this.rueckAktion.psMittel = psm;
        //rueckAktion bauen
        this.loop.run();
        aktionFuellen();
        //Wenn keine Kosten dann Vorgabewerte
        if (this.rueckAktion.kosten.length <= 2){
            this.rueckAktion.kosten = this.rueckAktion.get_kosten();
        }
        //Aktion in jedes Feld eintragen        
        foreach(feld a in aktFelderListe){
            this.rueckAktion.feld=a.id;
            if (aktFelderListe.length > 1){
            //Wenn mehrere Felder dann keine Teilfläche
                rueckAktion.flaeche=a.groesse;
            }
            laskDb.aktionHinzufuegen(rueckAktion, erntejahr);
        }
        hauptFenster.aktionenLesen();
        this.window6.destroy();
    }
    
    public void aktionAendern(aktion ak){
    //Aktion ändern
        Entry entry;

        this.rueckAktion = ak;
        this.rueckAktion.psMittel = psm;
        //Werte eintragen
        this.fensterBauen(ak.aktion);
        this.window6.set_title(ak.aktion + _(" ändern"));       
        this.button15.set_label(_("Ändern"));
        this.entry5.set_text(ak.datum.format("%d.%m.%Y"));
        this.kalender.select_month(int.parse(entry5.get_text()[3:5]) - 1, int.parse(entry5.get_text()[6:10]));
        this.kalender.select_day(int.parse(entry5.get_text()[0:2]));
        this.spinbutton23.set_value(ak.flaeche);
        this.textview2.get_buffer().set_text(ak.kommentar);
        entry = this.combobox14.get_child() as Entry;
        entry.set_text(ak.anwender);
        if (ak.aktion == "Saat"){
        //Wenn Saat
            entry = this.combobox15.get_child() as Entry;
            entry.set_text(ak.par2);
            entry = this.combobox16.get_child() as Entry;
            entry.set_text(ak.par1);
            if (ak.par3 <= 0) {
                this.spinbutton24.set_value(0 - ak.par3);
                this.combobox19.set_active(1);
            }else{
                this.spinbutton24.set_value(ak.par3);
                this.combobox19.set_active(0);
            }
            if (ak.par4 == 1){
                this.checkbutton1.set_active(true);
            }else{
                this.checkbutton1.set_active(false);
            }
        }else if (ak.aktion == "Bodenbearbeitung") {
        //Wenn Bodenbearbeitung
            entry = this.combobox17.get_child() as Entry;
            entry.set_text(ak.par1);
        }else if (ak.aktion == "Pflanzenschutz") {
        //Wenn Pflanzenschutz
            foreach (psMittel tmpMittel in ak.psMittel){
                Button tmpButton = new Button.with_label(tmpMittel.mittel + " (" + doubleparse(tmpMittel.menge, 3) + " Liter/ha)");
                tmpButton.set_tooltip_text(tmpMittel.to_string());
                tmpButton.has_tooltip = false;
                tmpButton.clicked.connect(()=>{this.psmHinzufuegen(tmpButton, 1);});
                listboxpsm1.add(tmpButton);
                tmpButton.show();
            }
        this.rueckAktion.psMittel = ak.psMittel;
        }else if (ak.aktion == "Organische Düngung" || ak.aktion == "Mineralische Düngung"){
        //Wenn Düngung
            entry = this.combobox18.get_child() as Entry;
            entry.set_text(ak.par1);
            this.label74.set_text(_("Menge ") + ak.par2 + "/ha:");
            this.spinbutton40.set_value(ak.par3);
            this.spinbutton41.set_value(ak.par4);
            this.spinbutton42.set_value(ak.par5);
            this.spinbutton43.set_value(ak.par6);
            this.spinbutton44.set_value(ak.par7);
            this.spinbutton45.set_value(ak.par8);
            this.spinbutton39.set_value(ak.par9);
        }else if (ak.aktion == "Ernte"){
        //Wenn Ernte
            this.spinbutton25.set_value(ak.par3);
            this.spinbutton26.set_value(ak.par5);
            this.spinbutton27.set_value(ak.par4);
            this.spinbutton28.set_value(int.parse(ak.par1.split(";")[0]));
            this.spinbutton29.set_value(int.parse(ak.par1.split(";")[1]));
            this.spinbutton30.set_value(int.parse(ak.par1.split(";")[2]));
            this.spinbutton31.set_value(int.parse(ak.par1.split(";")[3]));
            this.spinbutton32.set_value(int.parse(ak.par1.split(";")[4]));
            this.spinbutton33.set_value(int.parse(ak.par1.split(";")[5]));
        }else if (ak.aktion == "Weidegang"){
        //Wenn Weide
            this.spinbutton54.set_value(ak.par3);
            this.entry8.set_text(ak.par1);
            this.kalender2.select_month(int.parse(entry8.get_text()[3:5]) - 1, int.parse(entry8.get_text()[6:10]));
            this.kalender2.select_day(int.parse(entry8.get_text()[0:2]));
        }
        this.button19.set_label(doubleparse(this.rueckAktion.kosten[0] + this.rueckAktion.kosten[1] + (this.rueckAktion.kosten[2] / this.rueckAktion.flaeche)) + " €/ha");
        //rueckAktion bauen
        this.loop.run();
        aktionFuellen();
        laskDb.aktionAendern(rueckAktion, ak.id, erntejahr);
        hauptFenster.aktionenLesen();
        this.window6.destroy();
    }
    
    
    private void kostenAendern(){
    //Kostenfenster öffnen
        double[] tmpKosten = {0,0,0};

        this.window8.show();
        this.label83.show();
        this.label100.show();
        this.spinbutton46.show();
        aktionFuellen();
        tmpKosten = rueckAktion.get_kosten();
        if (this.rueckAktion.kosten.length <= 2){
            this.rueckAktion.kosten = {0, 0, 0};
            this.spinbutton46.set_value(tmpKosten[0]);
            this.spinbutton47.set_value(tmpKosten[1]);
            this.spinbutton48.set_value(tmpKosten[2]);
        }else{
            this.spinbutton46.set_value(this.rueckAktion.kosten[0]);
            this.spinbutton47.set_value(this.rueckAktion.kosten[1]);
            this.spinbutton48.set_value(this.rueckAktion.kosten[2]);
        }
        
        if(this.rueckAktion.aktion == "Saat"){
            this.label83.set_text(_("Saatgut:"));
            if (this.combobox19.get_active() == 0){
            //Wenn Kö/m²
                this.label100.set_text("(" + doubleparse(tmpKosten[0]) + _(" €/10000 Kö.):"));
            }else{
            //Wemm kg/ha
                this.label100.set_text("(" + doubleparse(tmpKosten[0]) + " €/kg):");
            }
        }else if(this.rueckAktion.aktion == "Pflanzenschutz"){
            this.label83.set_text(_("Pflanzenschutzmittel:"));
            this.label100.set_text("(" + doubleparse(tmpKosten[0]) + " €/ha):");
        }else if (this.rueckAktion.aktion == "Organische Düngung" || this.rueckAktion.aktion == "Mineralische Düngung"){
            this.label83.set_text(_("Dünger:"));
            this.label100.set_text("(" + doubleparse(tmpKosten[0]) + " €/" + masseinheit + "):");
        }else{
            this.label83.hide();
            this.label100.hide();
            this.spinbutton46.hide();
        }
        this.label101.set_text("(" + doubleparse(tmpKosten[1]) + " €/ha):");
        this.label102.set_text("(" + doubleparse(tmpKosten[2]) + " €):");
        
        //Kosten übernehmen
        this.loop2.run();
        if (this.label83.get_text() != "Abbrechen"){
            this.rueckAktion.kosten[0] = this.spinbutton46.get_value();
            this.rueckAktion.kosten[1] = this.spinbutton47.get_value();
            this.rueckAktion.kosten[2] = this.spinbutton48.get_value();
        }
        this.button19.set_label(doubleparse(this.rueckAktion.kosten[0] + this.rueckAktion.kosten[1] + (this.rueckAktion.kosten[2] / this.rueckAktion.flaeche)) + " €/ha");
        this.window8.hide();
    }

    private void psmHinzufuegen(Button btn, int aendern = 0){
        //einzelnes PSM bauen und als Button in Liste legen
        Entry tmpEntry = this.comboboxpsm1.get_child() as Entry;
        psMittel tmpMittel = psMittel();
        string art = "";
        this.comboboxpsm1.changed.connect(()=>{foreach (mittel m in laskDb.mittelLesen(erntejahr)){
                                                    if (m.art == 1 && m.name == tmpEntry.get_text()){
                                                        art = m.par3;
                                                        entrypsm1.set_text(m.par2);
                                                        spinbuttonpsm2.set_value(m.jahr/100);
                                                        break;
                                                    }
                                                }
                                        });
        //Fenster bauen
        buttonpsm2.set_label("Hinzufügen");
        if (aendern == 1){
            buttonpsm2.set_label("Ändern");
        }
        if (btn.get_tooltip_text() != null){
            tmpMittel = psMittel.from_string(btn.get_tooltip_text());
        }
        tmpEntry.set_text(tmpMittel.mittel);
        entrypsm1.set_text(tmpMittel.id.to_string());
        spinbuttonpsm1.set_value(tmpMittel.menge);
        spinbuttonpsm2.set_value(tmpMittel.preis);
        
        int i = this.dialogpsm.run();
        if (i == 0 && tmpEntry.get_text() != ""){
            //PSM Knopf in aktionEdit-Fenster erzeugen
            tmpMittel = psMittel.from_string(tmpEntry.get_text()+"|"+art+"|"+spinbuttonpsm1.get_value().to_string()+"|"+spinbuttonpsm2.get_value().to_string()+"|"+entrypsm1.get_text());
            btn.set_label(tmpMittel.mittel + " (" + doubleparse(spinbuttonpsm1.get_value(), 3) + " Liter/ha)");
            btn.set_tooltip_text(tmpMittel.to_string());
            btn.has_tooltip = false;
            if (aendern == 0){
                btn.clicked.connect(()=>{this.psmHinzufuegen(btn, 1);});
                listboxpsm1.add(btn);
                btn.show();
            }
        }else if(i ==1){
            //PSM Knopf in aktionEdit-Fenster löschen
            btn.get_parent().destroy();
            btn.destroy();
        }
        this.dialogpsm.hide();
    }
}
