using Sqlite;
using Gtk;
using lask;

public class canlegenFenster : GLib.Object{
    public Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Assistant assistant1;
    public Entry entry6;
    public TextView textview3;
    public Entry entry7;
    private int anlegenForm = 1;
    private RadioButton radiobutton1;
    private RadioButton radiobutton2;
    private RadioButton radiobutton3;
    
    public canlegenFenster() {
    //Konstruktor       
    err(uiVerzeichnis + _("lask.ui wird von anlegenfenster.vala geladen"),0);
        builder.set_translation_domain ("lask");
        assistant1 = builder.get_object ("assistant1") as Assistant;
        entry6 = builder.get_object ("entry6") as Entry;
        textview3 = builder.get_object ("textview3") as TextView;
        entry7 = builder.get_object ("entry7") as Entry;
        radiobutton1 = builder.get_object ("radiobutton1") as RadioButton;
        radiobutton2 = builder.get_object ("radiobutton2") as RadioButton;
        radiobutton3 = builder.get_object ("radiobutton3") as RadioButton;
        builder.connect_signals (this);
        assistant1.set_page_complete(assistant1.get_nth_page(1), true);
        //Signale verbinden
        this.assistant1.cancel.connect(on_cancel);
        this.entry6.changed.connect(on_focus_out);
        this.entry7.changed.connect(on_focus_out);
        this.radiobutton1.toggled.connect(radiobuttonToggled);
        this.assistant1.apply.connect(on_apply);
        
    }
    
    public void on_cancel() {
    //Bei abbrechen Fenster schließen
        startFenster.window1.show();
        this.assistant1.destroy();
    }
    public void on_focus_out() {
    //Eingaben überprüfen und weiterknopf freischalten
        this.entry7.set_text("0" + long.parse(this.entry7.get_text()).to_string());
        if (this.entry6.get_text() != ""){
            this.assistant1.set_page_complete(this.assistant1.get_nth_page(0), true);
        }else{
            this.assistant1.set_page_complete(this.assistant1.get_nth_page(0), false);
        }
    }
    public void on_apply() {
    //Wenn Fertig, dann ...
        feld rueckFeld = feld();
        feld[] felder = {};
        FileFilter filter = new Gtk.FileFilter ();
        cimport import = new cimport();
        
        if (this.anlegenForm == 1){
        //leeren Betrieb anlegen
            laskDb.betriebAnlegen(datenVerzeichnis + this.entry6.get_text() + ".lask");
            //Namen mit id 0 einfügen
            rueckFeld.id = 0;
            rueckFeld.name=this.entry6.get_text();
            rueckFeld.art=this.textview3.get_buffer().text;
            rueckFeld.groesse=double.parse(this.entry7.get_text());
            laskDb.feldHinzufuegen(rueckFeld);
        }
        else if (this.anlegenForm == 2){
        //bestehenden Betrieb importieren
            import.datei();
        }
        else if (this.anlegenForm == 3){
        //Betrieb aus MFA importieren
            //Namen mit id 0 einfügen
            rueckFeld.id = 0;
            rueckFeld.name=this.entry6.get_text();
            rueckFeld.art=this.textview3.get_buffer().text;
            rueckFeld.groesse=double.parse(this.entry7.get_text());
            //Felder holen
            var dialog = new FileChooserDialog (_("Shape-Datei öffnen"), anlegenFenster.assistant1,
                                                FileChooserAction.OPEN,
                                                _("Abbrechen"), ResponseType.CANCEL,
                                                _("Öffnen"), ResponseType.OK,
                                                null);
            dialog.set_filter (filter);
            filter.add_pattern("*.zip");
            if (dialog.run() == ResponseType.OK) {
                dialog.hide();
                if(dialog.get_filename()!= ""){
                    felder = import.iBalis(dialog.get_filename());
                }
                dialog.close();
            }else{dialog.close();}
            //Wenn alles passt Betrieb schreiben
            if(felder.length >= 1){
                laskDb.betriebAnlegen(datenVerzeichnis + this.entry6.get_text() + ".lask");
                laskDb.feldHinzufuegen(rueckFeld);
                foreach(feld f in felder){
                    laskDb.feldHinzufuegen(f);
                }
                laskDb.erntejahrHinzufuegen(felder[0].jahr);
            }else{
                MessageDialog mdialog = new MessageDialog(null,
                                        DialogFlags.MODAL,
                                        MessageType.INFO,
                                        ButtonsType.OK,
                                        _("Shape-Datei konnte nicht gelesen werden!"));
                mdialog.run();
                mdialog.destroy();
            }
        }
        this.assistant1.hide();
        startFenster.window1.show();
        startFenster.update();
    }
    
    public void radiobuttonToggled(){
        if(radiobutton1.get_active()){
            this.anlegenForm = 1;
        }else if(radiobutton2.get_active()){
            this.anlegenForm = 2;
        }else if(radiobutton3.get_active()){
            this.anlegenForm = 3;
        }
    }

}
