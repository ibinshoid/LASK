using Gtk;
using Cairo;
using lask;
using Pango;

public class chilfe{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window11;
    private ToolButton toolbutton21;
    private ToolButton toolbutton22;
    private ToolButton toolbutton23;
    private Box box2;
    private Box box3;
    private GLib.File pfad;
    private Label tmpLabel;
    private string[] alterName;
    private string[] tmpalles;
    private string alles;
    
    public chilfe(string name){
    //Konstructor
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        window11 = builder.get_object("window11") as Window;
        box2 = builder.get_object("box2") as Box;
        box3 = builder.get_object("box3") as Box;
        toolbutton21 = builder.get_object("toolbutton21") as ToolButton;
        toolbutton22 = builder.get_object("toolbutton22") as ToolButton;
        toolbutton23 = builder.get_object("toolbutton23") as ToolButton;
        //Signale verbinden
        toolbutton21.clicked.connect(zurueck);
        toolbutton22.clicked.connect(()=>{link("Home");});
        toolbutton23.clicked.connect(()=>{this.window11.destroy();});
        if(GLib.File.new_for_path(config.INSTALL_PATH + "/share/lask/help/" + Environment.get_variable("LANG")[0:2]).query_file_type(0) == GLib.FileType.DIRECTORY){
            pfad = GLib.File.new_for_path(config.INSTALL_PATH + "/share/lask/help/" + Environment.get_variable("LANG")[0:2]);
        }else{
            pfad = GLib.File.new_for_path(config.INSTALL_PATH + "/share/lask/help/d");
        }
//        pfad = GLib.File.new_for_path("../../help/de/");
        if(pfad.get_child("_Sidebar.md").query_exists()){
            tmpLabel = new Label("");
            tmpLabel.set_halign(Align.START);
            tmpLabel.set_xalign(0);
            tmpLabel.set_markup(markdownParse("_Sidebar.md"));
            tmpLabel.activate_link.connect((t, s) => {link(s);return true;});
            box2.add(tmpLabel);
            box2.show_all();
        }
        ladeSeite(name);
    }
    private void ladeSeite(string name){
        box3.foreach ((element) => box3.remove (element));
        //Überschrift
        tmpLabel = new Label("");
        tmpLabel.set_halign(Align.CENTER);
        tmpLabel.set_xalign((float)0.5);
        tmpLabel.set_line_wrap(false);
        tmpLabel.set_markup("""<span size = "xx-large">""" + name[0:-3] + "</span>");
        box3.add(tmpLabel);
        tmpalles = markdownParse(name).split("!IMG");
        //Text
        tmpLabel = new Label("");
        tmpLabel.set_halign(Align.START);
        tmpLabel.set_xalign(0);
        tmpLabel.set_line_wrap(true);
        tmpLabel.set_markup(tmpalles[0]);
        tmpLabel.activate_link.connect((t, s) => {link(s);return true;});
        box3.add(tmpLabel);
        if (tmpalles.length >= 2){
        //Bild dann Text
            box3.add(new Image.from_file(pfad.get_child(tmpalles[1]).get_path()));
            tmpLabel = new Label("");
            tmpLabel.set_halign(Align.START);
            tmpLabel.set_xalign(0);
            tmpLabel.set_line_wrap(true);
            tmpLabel.set_markup(tmpalles[2]);
            tmpLabel.activate_link.connect((t, s) => {link(s);return true;});
            box3.add(tmpLabel);
        }
        box3.show_all();
    }
    
    private string markdownParse(string name){
    //Rechte Seite malen
        int i = 0;
        string[] tmpalles2;
        
        FileUtils.get_contents(pfad.get_child(name).get_path(), out alles);
        alles = alles.replace("    \n", "\n\n");    //Absatz
        //Link oder Bild
        tmpalles = alles.split("](");
        if (tmpalles.length >= 2){                    
            i = 0;
            alles = "";
            foreach(string s in tmpalles){
                if (tmpalles[i].contains("![")){
                //Bild
                    if (i == 0){
                        alles = tmpalles[i].split("![")[0];
                    }else{
                        alles += tmpalles[i].split("![")[0].split(")")[1];
                    }
                    alles += "!IMG" + tmpalles[i+1].split(")")[0] + "!IMG";
                 }else if (tmpalles[i].contains("[") && tmpalles[i+1].contains(")")){
                //Link
                    if (i == 0){
                        alles = tmpalles[i].split("[")[0];
                    }else{
                        alles += tmpalles[i].split("[")[0].split(")")[1];
                    }
                    alles +=  """<a href = """" + tmpalles[i+1].split(")")[0] + """">""" + tmpalles[i].split("[")[1] + "</a>";
                }                    
                i += 1;
            }
            alles += tmpalles[i-1].split(")")[1];
        }
        //Zeilen formatieren
        tmpalles = alles.split("\n");
        i = 0;
        alles = "";
        foreach(string s in tmpalles){
            //Überschrift
            if(s[0:4] == "### "){
                tmpalles[i] = """<span size="large">""" + s[4:-1] + "</span>";
            }else if(s[0:3] == "## "){
                tmpalles[i] = """<span size = "x-large">""" + s[3:-1] + "</span>";
            }else if(s[0:2] == "# "){
                tmpalles[i] = """<span size = "xx-large">""" + s[2:-1] + "</span>";
            //Liste
            }else if(s[0:2] == "* "){
                tmpalles[i] = "● " + s[2:-1];
            }else if(s[0:4] == "  * "){
                tmpalles[i] = "  ● " + s[6:-1];
            //Codeblock
            }else if(s[0:4] == "    "){
                tmpalles[i] = """<tt><span background = "light gray">""" + s[4:-1] + "</span></tt>";
            }else if(s[0:1] == "    "){
                tmpalles[i] = """<tt><span background = "light gray">""" + s[1:-1] + "</span></tt>";
            }
            //Fett
            tmpalles2 = tmpalles[i].split("**");
            if (tmpalles2.length >= 3){
                tmpalles[i] = tmpalles2[0] + "<b>" + tmpalles2[1] + "</b>" + tmpalles2[2];
            }
            //Cursiv
            tmpalles2 = tmpalles[i].split("*");
            if (tmpalles2.length >= 3){
                tmpalles[i] = tmpalles2[0] + "<i>" + tmpalles2[1] + "</i>" + tmpalles2[2];
            }
            //Durchgestrichen
            tmpalles2 = tmpalles[i].split("~~");
            if (tmpalles2.length >= 3){
                tmpalles[i] = tmpalles2[0] + "<s>" + tmpalles2[1] + "</s>" + tmpalles2[2];
            }
            //Inline Code
            tmpalles[i] = tmpalles[i].replace("`", "'");
            tmpalles[i] = tmpalles[i].replace("´", "'");
            tmpalles2 = tmpalles[i].split("'");
            if (tmpalles2.length >= 3){
                tmpalles[i] = tmpalles2[0] + """<tt><span background = "light gray">""" + tmpalles2[1] + "</span></tt>" + tmpalles2[2];
            }
            alles += tmpalles[i] + "\n";
            i += 1;
        }        
    return alles;
    }
    
    private void link(string s){
        if (s[0:4].down() == "http"){
           show_uri_on_window(null, s, -1); 
        }else if (pfad.get_child(s.replace(" ", "-") + ".md").query_exists()) {
            alterName += s;
            ladeSeite(s.replace(" ", "-") + ".md");
        }
    }
    private void zurueck(){
        if (alterName.length >= 2){
            ladeSeite(alterName[alterName.length - 2].replace(" ", "-") + ".md");
            alterName.resize(alterName.length - 1); 
        }
    }
}
