using Sqlite;
using Gtk;
using lask;
using Cairo;
using Pango;

public class causwertungKosten : causwertung{
    private CheckButton checkbutton1;
    private CheckButton checkbutton2;
    private CheckButton checkbutton3;
    private ComboBoxText combobox20;

    private druckdaten[] druckfelder = {};
    private druckdaten druckfeld = druckdaten();
    
    public causwertungKosten(){
    //Konstructor
        this.window7.set_title(_("Auswertung Kosten"));
        seitenLeiste();
        aendern();
        //Ereignisse verbinden
        checkbutton1.toggled.connect(aendern);
        checkbutton2.toggled.connect(aendern);
        checkbutton3.toggled.connect(aendern);
        combobox20.changed.connect(aendern);
    }

    public void seitenLeiste(){
    //rechte Seitenleiste bauen
        combobox20 = new ComboBoxText();
        combobox20.append_text(_("Alle"));
        combobox20.append_text(_("Ausgewählte"));
        combobox20.set_active(0);
        grid33.attach(new Label(_("Felder:")),0 ,0, 1, 1);
        grid33.get_child_at(0, 0).set_halign(Align.END);
        grid33.get_child_at(0, 0).set_hexpand(false);
        grid33.attach(new Label(_("Drucken:")),0 ,1, 1, 1);
        grid33.get_child_at(0, 1).set_halign(Align.END);
        grid33.get_child_at(0, 1).set_hexpand(false);
        grid33.attach(checkbutton1 = new CheckButton.with_label(_("Deckblatt")), 1, 1, 1, 1);
        checkbutton1.set_active(true);
        grid33.attach(new Gtk.Separator(Gtk.Orientation.HORIZONTAL), 0, 2, 2, 1);
        grid33.attach(checkbutton2 = new CheckButton.with_label(_("Felder")), 1, 3, 1, 1);
        checkbutton2.set_active(true);
        grid33.attach(checkbutton3 = new CheckButton.with_label(_("Summe")), 1, 4, 1, 1);
        checkbutton3.set_active(true);
    }
    
    public override void aendern(){
    //Auswertung neu malen
        feld[] felder = {};
        aktion[] aktionen = {};
        string art = "";
        
        druckfelder.length = 0;
        this.druckfelder.resize(0);
        this.seiten = 0;

        //Wenn deckblatt=0 Deckblatt malen sonst nicht
        if (this.checkbutton2.get_active() == false && this.checkbutton3.get_active() == false){
            this.checkbutton1.set_active(true);
        }
        if (this.checkbutton1.get_active()){
            this.deckblatt = 0;
        } else {
           this.deckblatt = 1;
           if (this.seite == 0){
                this.seite = 1;
            }
        }
        //Welche Felder sollen verwendet werden?
        if (this.combobox20.get_active() == 0){
            felder = felderliste;
        }else if  (this.combobox20.get_active() == 1){
            felder = aktFelderListe;
        }
        //Sollen einzelne Felder gedruckt werden?
        if (this.checkbutton2.get_active() == true){
            foreach (feld f in felder) {
                //Fruchtart ermitteln
                aktionen.resize(0);
                foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                    if(f.art == "Grünland"){
                        art = _("Grünland");
                    }else if(a.aktion == "Saat"){
                        if(a.par4 == 1){
                            f.frucht = a.par2;
                        }
                        art = a.par2;
                    }
                    if (a.aktion == "Ernte"){
                        a.par2 = art;
                        art = "";
                    }
                    aktionen += a;
                }
                this.druckfeld.druckfeld = f;
                this.druckfeld.druckaktionen = aktionen;
                this.druckfelder += this.druckfeld;
            }
        }
        //Sollen Summen gedruckt werden?
        if (this.checkbutton3.get_active() == true){
            aktionen.resize(0);
            aktionen += aktion();
            aktionen[0].kosten = {0, 0, 0};
            aktionen += aktion();
            aktionen[1].kosten = {0, 0, 0};
            this.druckfeld.druckfeld = feld();
            this.druckfeld.druckfeld.name = _("Gesamt");
            foreach (feld f in felder) {
                this.druckfeld.druckfeld.groesse += f.groesse;
                foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                    if(a.aktion == "Mineralische Düngung" || a.aktion == "Organische Düngung"){
                        if (a.par2 == "dt"){
                            a.par9 = a.par9 * 100;
                        }
                        aktionen[0].kosten[0] += a.kosten[0] * a.par9;
                    }else if(a.aktion == "Saat"){
                        if(a.par3 <= 0){
                            a.par3 = 0 - a.par3;
                        }
                        aktionen[0].kosten[0] += a.kosten[0] * a.par3 * a.flaeche;
                    }else{
                        aktionen[0].kosten[0] += a.kosten[0];
                    }
                    aktionen[0].aktion = "Kosten";
                    aktionen[0].flaeche = -1 ;
                    aktionen[0].kosten[1] += a.kosten[1] * a.flaeche;
                    aktionen[0].kosten[2] += a.kosten[2];
                    
                    if(a.aktion == "Ernte"){
                        aktionen[1].aktion = "Erlös";
                        aktionen[1].flaeche = -1 ;
                        aktionen[1].par3 = 1;
                        aktionen[1].par5 = a.par3 * a.par5;
                    }
                }
            }
            this.druckfeld.druckaktionen = aktionen;
            this.druckfelder += this.druckfeld;
        }
        this.seiten = this.druckfelder.length;
        if (this.seite > this.seiten){
            this.seite = this.seiten;
        }
		this.drawingarea1.queue_draw();
    }

    public override Cairo.Context draw_page(int breite, int page_nr, Cairo.Context context){
    //Seite zusammenbauen Cairo.Context
        int width = breite;
        int heigh = int.parse(doubleparse(width * 1.41, 0));
        int hoehe = 80;
        int zeilen = 0;
        int zeilenhoehe = 12;
        int dpi = 1024;
        string name = "";
        string adresse = "";
        string betriebsnr = "";
        var cr = context;
        var layout1 = Pango.cairo_create_layout (cr);
        var layout2 = Pango.cairo_create_layout (cr);
        double[] kosten = {0, 0, 0};
        double erloes = 0;
        
        //Betriebsname lesen
        name = betrieb.name.to_string();
        adresse = betrieb.art.replace("\n", ", ");
        betriebsnr = "0" + betrieb.groesse.to_string()[0:11];
        //Seite weiß malen
        cr.set_source_rgb (1, 1, 1);
        cr.rectangle(0, 0, width, heigh);
        cr.fill();
        //Layouts setzen
        cr.set_source_rgb (0, 0, 0);
        layout1.set_alignment(Pango.Alignment.CENTER);
        layout1.set_width(breite*dpi);
        layout2.set_width(breite*dpi);
        layout2.set_wrap(Pango.WrapMode.WORD);
        layout2.set_font_description(FontDescription.from_string("sans 10"));
        if (page_nr == 0){
        // Deckblatt drucken
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup(_("<b><u>Kostenrechnung</u></b>"), -1);
            cr.move_to(0, 30);
            cairo_layout_path (cr, layout1);
            layout1.set_markup("<b><u>" + _("für ") + name + "</u></b>", -1);
            cr.move_to(0, 80);
            cairo_layout_path (cr, layout1);
            layout1.set_markup("<b><u>" + _("Erntejahr ") + felderliste[0].jahr.to_string() + "</u></b>", -1);
            cr.move_to(0, 130);
            cairo_layout_path (cr, layout1);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_markup(_("Erstellt mit LASK ") + config.LASK_VERSION, -1);
            cr.move_to(0,  heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.fill();
            
        }else{
            //Adresse und erntejahr oben anzeigen   
            cr.set_source_rgb(0, 0, 0);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            layout1.set_alignment(Pango.Alignment.LEFT);
            layout1.set_markup(betriebsnr + "         <b><u>" + name + "</u></b>, " + adresse + _("        <b><u>Erntejahr: ") + felderliste[0].jahr.to_string() + "</u></b>", -1);
            cr.move_to(30, 10);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Seitennummer unten anzeigen
            layout1.set_markup(_("Seite ") + (page_nr).to_string() + " / " + druckfelder.length.to_string(), -1);
            layout1.set_alignment(Pango.Alignment.CENTER);
            layout1.set_font_description(FontDescription.from_string("sans 10"));
            cr.move_to(0, heigh - 20);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0.0, 0);
            cr.fill();
            //Feldname und größe anzeigen
            layout1.set_font_description(FontDescription.from_string("sans 25"));
            layout1.set_markup("<b><u>" + druckfelder[page_nr -1].druckfeld.name + "</u></b> (" + doubleparse(druckfelder[page_nr -1].druckfeld.groesse) + " ha)" + " " + druckfelder[page_nr -1].druckfeld.frucht, -1);
            cr.move_to(0, 35);
            cairo_layout_path (cr, layout1);
            cr.set_source_rgb(0, 0, 0);
            cr.fill();
            layout1.set_font_description(FontDescription.from_string("sans 20"));
            
            //Kosten malen
            layout1.set_markup(_("<b>Kosten</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            zeilen = zeilen + 2;        
            hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
            foreach (aktion ak in druckfelder[page_nr -1].druckaktionen){
            //Aktionen mit Kosten malen
                if (ak.aktion != "Kosten" && ak.aktion != "Erlös"){
                //Wenn echte Aktion
                    zeilen = 0;
                    layout2.set_markup(ak.aktion + _(" am ") + ak.datum.format("%d.%m.%Y"), -1);
                    cr.move_to(100 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 2;        
                    if(ak.aktion == "Mineralische Düngung" ||ak.aktion ==  "Organische Düngung"){
                        if (ak.par2 == "dt"){
                            ak.par9 = ak.par9 * 100;
                        }
                        kosten[0] += ak.kosten[0] * ak.par9;
                        layout2.set_markup(doubleparse(ak.kosten[0] * ak.par9) + " €/ha Dünger", -1);
                        cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        layout2.set_markup(doubleparse((ak.kosten[0] * ak.par9) + ak.kosten[1] + (ak.kosten[2] / ak.flaeche)) + " €/ha Gesamt", -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                    }else if(ak.aktion == "Saat"){
                        if(ak.par3 <= 0){
                            ak.par3 = 0 - ak.par3;
                        }
                        kosten[0] += ak.kosten[0] * ak.par3;
                        layout2.set_markup(doubleparse(ak.kosten[0] * ak.par3) + _(" €/ha Saatgut"), -1);
                        cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        layout2.set_markup(doubleparse((ak.kosten[0] * ak.par3) + ak.kosten[1] + (ak.kosten[2] / ak.flaeche)) + " €/ha Gesamt", -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                    }else if(ak.aktion == "Pflanzenschutz"){
                       kosten[0] += ak.kosten[0];
                        layout2.set_markup(doubleparse(ak.kosten[0]) + _(" PSM €/ha"), -1);
                        cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        layout2.set_markup(doubleparse(ak.kosten[0] + ak.kosten[1] + (ak.kosten[2] / ak.flaeche)) + " €/ha Gesamt", -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                    }else if(ak.aktion == "Bodenbearbeitung"){
                        //Bodenbearbeitung
                        layout2.set_markup(ak.par1, -1);
                        cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                        layout2.set_markup(doubleparse(ak.kosten[1] + (ak.kosten[2] / ak.flaeche)) + _(" €/ha Gesamt"), -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                    }else{
                        //Ernte
                        layout2.set_markup(doubleparse(ak.kosten[1] + (ak.kosten[2] / ak.flaeche)) + _(" €/ha Gesamt"), -1);
                        cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                        cairo_layout_path (cr, layout2);
                    }
                    kosten[1] += ak.kosten[1];
                    kosten[2] += ak.kosten[2] / ak.flaeche;
                    layout2.set_markup(doubleparse(ak.kosten[1]) + _(" €/ha Arbeit"), -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                    //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe += (zeilen + 1)* zeilenhoehe;
                }else if (ak.aktion == "Kosten"){
                //Wenn Auswertung
                    zeilen = 2;
                    layout2.set_markup(doubleparse(ak.kosten[0]) + " €", -1);
                    cr.move_to(300 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                    //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
                }
            }
            //Summe unter Aktionen
            layout2.set_markup(_("<b><u>Summe:</u></b>"), -1);
            cr.move_to(30 ,hoehe + zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup("<b><u>" + doubleparse(kosten[0]) + _(" €/ha Betriebsmittel") + "</u></b>", -1);
            cr.move_to(100 ,hoehe + zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup("<b><u>" + doubleparse(kosten[1]) + _(" €/ha Arbeit") + "</u></b>", -1);
            cr.move_to(300 ,hoehe + zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup("<b><u>" + doubleparse(kosten[0] + kosten[1] + kosten[2]) + _(" €/ha Gesamt") + "</u></b>", -1);
            cr.move_to(500 ,hoehe + zeilenhoehe);
            cairo_layout_path (cr, layout2);
            zeilen = zeilen + 1;        
            cr.fill();
            //Rechteck um Summe malen
            if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
            cr.rectangle(20, hoehe, width -40, zeilen * zeilenhoehe);
            cr.stroke();
            hoehe = (zeilen + 1)* zeilenhoehe + hoehe;

            //Erlöse malen
            layout1.set_markup(_("<b>Erlöse</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            hoehe = (zeilen + 3)* zeilenhoehe + hoehe;
            foreach (aktion ak in druckfelder[page_nr -1].druckaktionen){
                if (ak.aktion == "Ernte"){
                    zeilen = 0;
                    layout2.set_markup(_("Ernte(") + ak.par2 + _(") am ") + ak.datum.format("%d.%m.%Y"), -1);
                    cr.move_to(100 ,hoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 2;        
                    erloes += ak.par3 * ak.par4;
                    layout2.set_markup(doubleparse(ak.par3) + " dt x " + doubleparse(ak.par4) + " €", -1);
                    cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    layout2.set_markup("<b><u>" + doubleparse(ak.par3 * ak.par4) + " €/ha</u></b>", -1);
                    cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                    //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
                }else if(ak.aktion == "Erlös"){
                    zeilen = 2;
                    layout2.set_markup(doubleparse(ak.par3 * ak.par5) + " €", -1);
                    cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
                    cairo_layout_path (cr, layout2);
                    zeilen = zeilen + 1;        
                    cr.fill();
                     //Rechteck um Aktion malen
                    if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
                    cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
                    cr.stroke();
                    hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
                }
            }
            //Bilanz malen
            layout1.set_markup(_("<b>Überschuss</b>"), -1);
            cr.move_to(0 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout1);
            hoehe = (zeilen + 3)* zeilenhoehe + hoehe;
            layout2.set_markup(doubleparse(erloes) + "€ - " + doubleparse(kosten[0] + kosten[1] + kosten[2]) + " €", -1);
            cr.move_to(100 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            layout2.set_markup("<b><u>" + doubleparse(erloes - (kosten[0] + kosten[1] + kosten[2])) + " €/ha</u></b>", -1);
            cr.move_to(500 ,hoehe + zeilen * zeilenhoehe);
            cairo_layout_path (cr, layout2);
            zeilen = zeilen + 1;        
            cr.fill();
            //Rechteck um Aktion malen
            if (layout2.get_line_count() + 1 > zeilen){zeilen = layout2.get_line_count() +2;}
            cr.rectangle(20, hoehe, width -40, (zeilen + 1)* zeilenhoehe);
            cr.stroke();
            hoehe = (zeilen + 1)* zeilenhoehe + hoehe;
        }
        return cr;
    }
}
