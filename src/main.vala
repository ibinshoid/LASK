using Sqlite;
using Gtk;
using lask;


const string localePfad = config.INSTALL_PATH + "/share/locale";
const string GETTEXT_PACKAGE = "lask";

TreeIter iter;
string datenVerzeichnis;
string uiVerzeichnis;
string dbDatei;
int erntejahr=1;
int debuging=0;
cstartFenster startFenster;
chauptFenster hauptFenster;
cmittelFenster mittelFenster;
cfelderFenster felderFenster;
canlegenFenster anlegenFenster;
cbetriebEdit betriebEdit;
cmittelEdit mittelEdit;
cfeldEdit feldEdit;
caktionEdit aktionEdit;
causwertungUebersicht auswertungUebersicht;
causwertungNaehrstoff auswertungNaehrstoff;
causwertungKosten auswertungKosten;
claskDb laskDb;
cmobilGeraet mobilGeraet;
ceinstellungen einstellungen;
chilfe hilfe;

feld betrieb;
feld aktFeld;
aktion aktAktion;
feld[] felderliste;
feld[] aktFelderListe;

public static int main (string[] args) {

    foreach (string arg in args){
        if(arg == "--debug"){debuging=1;}
        if(arg == "--help"){
            stdout.printf(_(
"""Aufruf: lask [Option]
Ackerschlagkartei für Linux

Optionen:
  --debug     gibt Debugmeldungen aus
  --version   zeigt die aktuelle Versionsnummer an
  --help      zeigt diese Hilfeseite an
""") + "Pfad:" + args[0]+"\n");
    return 0;
        }
        if(arg == "--version"){
            print(_("LASK Version = " + config.LASK_VERSION + "\n"));
            return 0;
        }
    }

    //i18n Zeugs
    Intl.setlocale(LocaleCategory.MESSAGES, "");
    Intl.textdomain(GETTEXT_PACKAGE); 
    Intl.bind_textdomain_codeset(GETTEXT_PACKAGE, "utf-8");
    Intl.bindtextdomain(GETTEXT_PACKAGE, localePfad);
    aktFeld = feld();
    aktAktion = aktion();

    //Wenn Datenverzeichnis nicht da ist, dann erzeugen
    datenVerzeichnis = Environment.get_user_config_dir() + "/LASK/";
    if(GLib.DirUtils.create_with_parents(datenVerzeichnis, -1) != 0){
        err(datenVerzeichnis + _(" nicht verfügbar"));  
    }else{
        //Wenn Datenverzeichnis leer ist, dann Musterbetrieb reinkopieren
        if(Dir.open(datenVerzeichnis, 0).read_name() == null){
            if(GLib.FileUtils.test(config.INSTALL_PATH + "/share/lask/Musterbetrieb.lask", FileTest.IS_REGULAR)){
                GLib.File.new_for_path(config.INSTALL_PATH + "/share/lask/Musterbetrieb.lask").copy(GLib.File.new_for_path(datenVerzeichnis + "/Musterbetrieb.lask"), 0, null);
                err(_("Musterbetrieb wurde angelegt"), 0);
            }else{
                err(_("Vorlage für Musterbetrieb nicht gefunden"), 1);
            }
        }
    }

    Gtk.init (ref args);
//    mobilGeraet = new cmobilGeraet();
    startFenster = new cstartFenster();
    laskDb = new claskDb();
    startFenster.window1.show_all();
    Gtk.main ();
    return 0;
}


public void feldInfo2(){
    TreeSelection selection;
    GLib.Value wert;
    TreeModel model;
    
    aktFeld.groesse = 0;
    aktFeld.id = -1;
    felderliste.resize(0);
    aktFelderListe.resize(0);
    selection = hauptFenster.treeview2.get_selection();
    var rows = selection.get_selected_rows(out model);
    felderliste = laskDb.felderLesen(erntejahr);
    foreach (feld f in felderliste){
        foreach(TreePath a in rows){
            model.get_iter(out iter, a);
            model.get_value(iter, 0, out wert);
            if (wert.get_int() == f.id){
                aktFelderListe += f;
                aktFeld.groesse += f.groesse;
            }
        }   
        if (aktFelderListe.length == 1){aktFeld = aktFelderListe[0];}
    }
}

public aktion aktionInfo(int feld){
    aktion rueckAktion = aktion();
    TreeSelection selection;
    GLib.Value wert;
    TreeModel model;
    
    rueckAktion.id = -1;
    selection = hauptFenster.treeview3.get_selection();
    selection.get_selected(out model, out iter);
    model.get_value(iter, 0, out wert);
    foreach (aktion a in laskDb.aktionLesen(feld.to_string(), erntejahr)){
            if (wert.get_int() == a.id){
                rueckAktion = a;
            }
    }
    return rueckAktion;
}

