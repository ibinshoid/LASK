using Gtk;
using lask;

public class ceinstellungen : GLib.Object{
//Betriebsdaten bearbeiten    
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window10;
    private Button button22;
    private Button button23;
    private SpinButton spinbutton22;
    private SpinButton spinbutton50;
    private SpinButton spinbutton51;
    private SpinButton spinbutton52;
    private SpinButton spinbutton53;
    private MainLoop loop = new MainLoop ();
    
    public ceinstellungen(){
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von einstellungen.vala geladen"),0);
        window10 = builder.get_object ("window10") as Window;
        button22 = builder.get_object ("button22") as Button;
        button23 = builder.get_object ("button23") as Button;
        spinbutton22 = builder.get_object ("spinbutton22") as SpinButton;
        spinbutton50 = builder.get_object ("spinbutton50") as SpinButton;
        spinbutton51 = builder.get_object ("spinbutton51") as SpinButton;
        spinbutton52 = builder.get_object ("spinbutton52") as SpinButton;
        spinbutton53 = builder.get_object ("spinbutton53") as SpinButton;
        this.window10.set_modal(true);
        //Ereignisse verbinden
        button22.clicked.connect(()=>{loop.quit();});
        button23.clicked.connect(()=>{this.window10.destroy();});
    }
    
    public void aendern(){
        mittel rueckMittel = mittel();
        
        //Standartwerte setzen
        rueckMittel.id = -1;
        rueckMittel.art = 7;
        rueckMittel.name = "kosten";
        //Werte laden
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 7 && m.name == "kosten"){
                rueckMittel = m;
                this.spinbutton22.set_value(m.par4);
                this.spinbutton50.set_value(m.par5);
                this.spinbutton51.set_value(m.par6);
                this.spinbutton52.set_value(m.par7);
                this.spinbutton53.set_value(m.par8);
            }   
        }
        loop.run();
        //rueckMittel bauen
        rueckMittel.par4 = this.spinbutton22.get_value();
        rueckMittel.par5 = this.spinbutton50.get_value();
        rueckMittel.par6 = this.spinbutton51.get_value();
        rueckMittel.par7 = this.spinbutton52.get_value();
        rueckMittel.par8 = this.spinbutton53.get_value();
        if (rueckMittel.id == -1){
            laskDb.mittelHinzufuegen(rueckMittel);
        }else{
            laskDb.mittelAendern(rueckMittel);
        }
        config.kosten = rueckMittel;
        this.window10.destroy();
    }
}

