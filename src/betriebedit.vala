using Gtk;
using lask;

public class cbetriebEdit : GLib.Object{
//Betriebsdaten bearbeiten    
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window3;
    private Button button7;
    private Button button8;
    private Entry entry1;
    private Entry entry2;
    private TextView textview1;
    
    public cbetriebEdit(){
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von betriebedit.vala geladen"),0);
        window3 = builder.get_object ("window3") as Window;
        button7 = builder.get_object ("button7") as Button;
        button8 = builder.get_object ("button8") as Button;
        entry1 = builder.get_object ("entry1") as Entry;
        entry2 = builder.get_object ("entry2") as Entry;
        textview1 = builder.get_object ("textview1") as TextView;
        this.window3.set_modal(true);
        //Ereignisse verbinden
        button8.clicked.connect(()=>{this.window3.destroy();});
    }
    
    public void aendern(){
        feld rueckFeld = feld();;
        
        this.window3.set_title(_("LASK Betrieb ändern"));
		MainLoop loop = new MainLoop ();
        
        this.entry1.set_text(betrieb.name);
        this.entry2.set_text("0" + betrieb.groesse.to_string()[0:11]);
        this.textview1.get_buffer().set_text(betrieb.art.replace(",", "\n"), -1);
        this.button7.clicked.connect(()=>{loop.quit();});
        loop.run();

        rueckFeld.id = 0;
        rueckFeld.name = entry1.get_text();
        rueckFeld.art=textview1.get_buffer().text;
        rueckFeld.groesse=double.parse(entry2.get_text());
        laskDb.feldAendern(rueckFeld);
        betrieb = rueckFeld;
        this.window3.destroy();
    }
}

