using Sqlite;
using Gtk;
using lask;

public class cfelderFenster : GLib.Object{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window5;
    private Button button11;
    private Button button12;
    private Button button13;
    private Button button14;
    private TreeStore treestore1;
    private TreeView treeview4;
    private ComboBoxText combobox6;
    private feld tmpFeld;

    public cfelderFenster() {
    //Konstructor
    err(uiVerzeichnis + _("lask.ui wird von felder.vala geladen"),0);
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        window5 = builder.get_object ("window5") as Window;
        button11 = builder.get_object ("button11") as Button;
        button12 = builder.get_object ("button12") as Button;
        button13 = builder.get_object ("button13") as Button;
        button14 = builder.get_object ("button14") as Button;
        treestore1 = builder.get_object ("treestore1") as TreeStore;
        treeview4 = builder.get_object ("treeview4") as TreeView;
        combobox6  = builder.get_object ("combobox6") as ComboBoxText;
        this.window5.set_modal(true);
        tmpFeld.id = -1;
        //Ereignisse verbinden
        treeview4.row_activated.connect(aendern);
        button11.clicked.connect(hinzufuegen);
        button12.clicked.connect(aendern);
        button13.clicked.connect(entfernen);
        button14.clicked.connect(schliessen);
        treeview4.cursor_changed.connect(treeview4_changed);
    }
    
    public void felderLaden(){
        TreeIter iter = TreeIter(); 
        TreeIter iterFeld = TreeIter();   
        TreeIter iterStillegung = TreeIter();   
        TreeIter iterGruenland = TreeIter();   

        this.window5.set_title(_("Felder"));
        this.combobox6.destroy();
        this.treestore1.clear();
        this.treestore1.append(out iterFeld, null);
        this.treestore1.set (iterFeld, 1, _("Feld"), -1);
        this.treestore1.append(out iterStillegung, null);
        this.treestore1.set (iterStillegung, 1, _("Stillegung"), -1);
        this.treestore1.append(out iterGruenland, null);
        this.treestore1.set (iterGruenland, 1, _("Grünland"), -1);
        foreach(feld f in laskDb.felderLesen(erntejahr)){
            if (f.art == "Feld"){
                this.treestore1.append(out iter, iterFeld);
            }else if (f.art == "Stillegung"){
                this.treestore1.append(out iter, iterStillegung);
            }else if (f.art == "Grünland"){
                this.treestore1.append(out iter, iterGruenland);
            }            
            this.treestore1.set (iter, 0,f.id, -1);
            this.treestore1.set (iter, 1,f.name, -1);
            this.treestore1.set (iter, 2,f.art, -1);
        }
        
    }

    public void treeview4_changed(){
    //Feld wurde in Listbox2 gewählt
        TreeModel model;
        GLib.Value wert;
        this.treeview4.get_selection().get_selected(out model, out iter);
        model.get_value(iter, 0, out wert);
        foreach(feld f in laskDb.felderLesen(erntejahr)){
            if (f.id == wert.get_int()){
                this.tmpFeld = f;
            }   
        }
    }

    public void entfernen(){
    //Feld entfernen
       if(tmpFeld.id >= 0){
            Dialog dialog = new Dialog.with_buttons(_("Frage"),this.window5,Gtk.DialogFlags.MODAL,
                                                            _("Abbrechen"), Gtk.ResponseType.CANCEL,
                                                            _("OK"), Gtk.ResponseType.OK);
            dialog.get_content_area().add(new Label(_("Feld wirklich Löschen? \nAlle Daten gehen verloren\n")));         
            dialog.show_all();
            
            if (dialog.run() == Gtk.ResponseType.OK) {
                laskDb.feldEntfernen(this.tmpFeld.id);
                this.felderLaden();
                dialog.close();
            }else{dialog.close();}
        }
    }


    public void hinzufuegen(){
        //Feld hinzufügen Fenster
        if (erntejahr != 0){
           feldEdit = new cfeldEdit();
           feldEdit.window4.set_transient_for(this.window5);
           feldEdit.window4.show_all();
           laskDb.feldHinzufuegen(feldEdit.feldNeu());
           this.felderLaden();
        }
        else{
            Dialog dialog = new Dialog.with_buttons(_("Hinweis"),
                                                    this.window5,Gtk.DialogFlags.MODAL,
                                                    _("OK"), Gtk.ResponseType.OK);
            dialog.get_content_area().add(new Label(_("Sie müssen erst ein Erntejahr hinzufügen,\nbevor Sie Felder anlegen können!\n")));            
            dialog.show_all();
            if (dialog.run() == Gtk.ResponseType.OK) {dialog.close();}
        }
    }


    public void aendern(){
       //Feld ändern
       if(tmpFeld.id >= 0){
           feldEdit = new cfeldEdit();
           feldEdit.window4.set_transient_for(this.window5);
           feldEdit.window4.show_all();
           laskDb.feldAendern(feldEdit.feldAendern(tmpFeld));
           this.felderLaden();
       }
    }
    
    public void schliessen(){
        //Felder neu einlesen aber Filter behalten
        hauptFenster.filterAendern();
        this.window5.destroy();
    }

    
}
