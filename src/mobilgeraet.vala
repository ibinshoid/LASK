using Sqlite;
using Gtk;
using lask;

public class cmobilGeraet{
    private Database dbMobile;
    private int rc = 0;
    private string handyName = "";
    private GLib.File handyFile;
    private MessageDialog dialog;
    private GLib.File tmpFile = GLib.File.new_for_path(Environment.get_tmp_dir() + "/tmplask.db");

    public void cmobilGeraet(){
    //Konstructor 
    }
    
    public void anlegen(){
    //Immer wieder nach Handy schauen
        dialog = new MessageDialog(null,Gtk.DialogFlags.MODAL, MessageType.INFO, ButtonsType.CANCEL, _("Suche nach Mobilgerät..."));
        dialog.response.connect ((response_id) =>  {switch (response_id){
                                                    case Gtk.ResponseType.CANCEL:
                                                    rc = 1;
                                                    dialog.hide();
                                                    break;}});               
        dialog.show_all();
        handyFile = this.findeMobilGeraet();
        if(handyFile != null){    
            handyName = handyFile.get_uri();
            handyFile.copy(tmpFile, FileCopyFlags.OVERWRITE);
            if(this.open(tmpFile.get_path()) == 1){
                dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb existiert bereits!"));
            }else{
                MessageDialog frage = new MessageDialog(null,Gtk.DialogFlags.DESTROY_WITH_PARENT, MessageType.QUESTION, ButtonsType.YES_NO, _("Betrieb auf Mobilgerät laden?\n\n Daten des vorhandenen Betriebs gehen verloren!"));
                if(frage.run() == ResponseType.YES){
                    frage.destroy();
                    //Datenbankdatei aufs Handy kopieren
                    if(GLib.File.new_for_path(datenVerzeichnis + dbDatei + ".lask").copy(GLib.File.new_for_uri(handyName), FileCopyFlags.OVERWRITE) == false){
                        dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb konnte nicht auf Mobilgerät geladen werden!"));
                    }else{
                        dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb wurde auf Mobilgerät geladen."));
                    }
                }else{
                    frage.destroy();
                    dialog.set_property("text", _("LASKmobile gefunden.\n\nBetrieb auf Mobilgerät laden abgebrochen!"));
                }
            }
        }
        //Aufräumen
        tmpFile.delete();
    }
    
    public void abgleichen(){
    //Daten abgleichen
        dialog = new MessageDialog(null,Gtk.DialogFlags.MODAL, MessageType.INFO, ButtonsType.CANCEL, _("Suche nach Mobilgerät..."));
        dialog.response.connect ((response_id) =>  {switch (response_id){
                                                    case Gtk.ResponseType.CANCEL:
                                                    rc = 1;
                                                    dialog.hide();
                                                    break;}});               
        dialog.show_all();
        handyFile = this.findeMobilGeraet();
        if(handyFile != null){    
            handyName = handyFile.get_uri();
            handyFile.copy(tmpFile, FileCopyFlags.OVERWRITE);
            if(this.open(tmpFile.get_path()) == 1){
                dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb passt!"));
                dialog.hide();
                //Datenbankzeugs macht import.mobilGeraet
                cimport import = new cimport();
                if (import.mobilGeraet(dbMobile) == true){
                    //Datenbankdatei aufs Handy kopieren
                    if(GLib.File.new_for_path(datenVerzeichnis + dbDatei + ".lask").copy(GLib.File.new_for_uri(handyName), FileCopyFlags.OVERWRITE) == false){
                        dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb konnte nicht auf Mobilgerät geladen werden!"));
                    }else{
                        dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb passt.\n\nDatenabgleich abgeschlossen."));
                    }
                }else{
                    dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nBetrieb passt!\n\nDatenabgleich wurde abgebrochen!"));
                }
            dialog.show();
            }else{
                dialog.set_property("text", _("Mobilgerät gefunden.\n\nLASKmobile gefunden.\n\nFalscher Betrieb!"));
            }
        }
        //Aufräumen
        tmpFile.delete();
    }
    
    private GLib.File findeMobilGeraet(){
    //Nach Handy schauen
        GLib.File file = null;
        GLib.File laskMobileDb = null;
        GLib.FileInfo info = null;
        
        //Gemountete Geräte finden
        List<Mount> drives = VolumeMonitor.get().get_mounts();
        foreach (Mount drive in drives){
            file = drive.get_root();
	
    print ("\n");
    print ("---------------------------------\n");
	print ("  URL: %s\n", file.get_uri());
	print (" TYPE: %s\n", file.query_filesystem_info("*").get_attribute_as_string(FileAttribute.FILESYSTEM_TYPE));

            if(file.get_uri()[0:4] == "mtp:" || file.get_uri()[0:4] == "file"){
                //Mountpoint finden
                dialog.set_property("text", _("Mobilgerät gefunden: ") + file.get_uri());
                FileEnumerator files = file.enumerate_children ("*", 0);
                while((info = files.next_file())!=null){
                    laskMobileDb = GLib.File.new_for_uri(file.get_uri() + info.get_name() + "/Android/data/com.rfo.LASKmobile/LASKmobile/databases/lask.db");
                    if (laskMobileDb.query_exists()){
                        dialog.set_property("text", _("Mobilgerät gefunden: ") + Uri.unescape_string(drive.get_default_location().get_uri()) + _("\n\nLASKmobile gefunden.\n"));
                        if (info.get_attribute_uint64("filesystem::free") >= laskMobileDb.query_info("*", 0).get_attribute_uint64("standard::size")*2){
                            return(laskMobileDb);
                        }else{
                            dialog.set_property("text", _("Mobilgerät gefunden: ") + Uri.unescape_string(file.get_uri()) + _("\n\nLASKmobile gefunden!\n\nZu wenig Speicherplatz!\n"));
                        }
                    }else{
                        dialog.set_property("text", _("Mobilgerät gefunden: ") + Uri.unescape_string(file.get_uri()) + _("\n\nLASKmobile nicht gefunden!\n"));
                    }
                }
            }else{
                dialog.set_property("text", _("Kein Mobilgerät gefunden!\n"));
            }
        }
    return(null);
    }

    public int open(string dbdatei) {
    //Datenbank öffnen
    err(_("DB Datei wird geladen: ") + dbdatei, 0);
        Statement stmt;

        rc=dbMobile.open (dbdatei, out dbMobile);
        if (rc == 1){
            err(_("Kann Datenbank nicht öffnen: ") + dbMobile.errmsg(), 1);
        }
        //Schauen ob richtiger Betrieb
        rc = dbMobile.prepare_v2("SELECT * FROM 'feldjahr' WHERE id = 0", -1, out stmt, null);
        if ( rc == 1 ) {
            err(_("SQL Fehler felderLesen: ") + dbMobile.errmsg(), 0);
            rc = 0;
        }
        rc = stmt.step();
        if(stmt.column_double(4) == betrieb.groesse && stmt.column_text(2) == betrieb.name){
            return 1; 
        }else{
            return 0;
        }
        return 0;
    }
    


}
