using Gtk;
using Sqlite;
using lask;
using Pango;

public class chauptFenster : GLib.Object{
    private Builder builder = new Builder.from_string(config.UI_FILE, -1);
    public Window window2;
    private Gtk.MenuItem menuitem2;
    private Gtk.MenuItem menuitem41;
    private Gtk.MenuItem menuitem42;
    private Gtk.MenuItem menuitem5;
    private Gtk.MenuItem menuitem6;
    private Gtk.MenuItem menuitem7;
    private Gtk.MenuItem menuitem9;
    private Gtk.MenuItem menuitem10;
    private Gtk.MenuItem menuitem11;
    private Gtk.MenuItem menuitem12;
    private Gtk.MenuItem menuitem14;
    private Gtk.MenuItem menuitem15;    
    private Gtk.MenuItem menuitem17;    
    private Gtk.Menu popup1;    
    private Gtk.MenuItem popup11;    
    private Gtk.MenuItem popup12;    
    private Gtk.MenuItem popup13;    
    public TreeView treeview2;
    public TreeView treeview3;
    private ComboBoxText combobox1;
    public ComboBox combobox8;
    public ComboBox combobox4;
    private Gtk.ListStore liststore4;
    private Gtk.ListStore jahrListe;
    private Gtk.ListStore felderListe;
    private Gtk.ListStore aktionListe = new Gtk.ListStore(18, typeof(int), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(float), typeof(float), typeof(float), typeof(float), typeof(float), typeof(float), typeof(float), typeof(int), typeof(float), typeof(string), typeof(string));
    private Notebook notebook1;
    private Label label12;
    private Label label13;
    public Label label14;
    private Gtk.Alignment alignment1;
    private Button button5;
    private Button button6;
    private Label label1;
    private Label label19;
    private Label label21;
    private Label label23;
    private Label label25;
    private Frame frame1;
    private Gtk.Clipboard clipboard;
    
    public chauptFenster() {    
    //Hauptfenster zusammenbauen
    err(uiVerzeichnis + _("/lask.ui wird von hauptfenster.vala geladen"),0);
        builder.set_translation_domain ("lask");
        builder.connect_signals (this);
        window2 = builder.get_object ("window2") as Window;
        menuitem2 = builder.get_object ("menuitem2") as Gtk.MenuItem;
        menuitem41 = builder.get_object ("menuitem41") as Gtk.MenuItem;
        menuitem42 = builder.get_object ("menuitem42") as Gtk.MenuItem;
        menuitem5 = builder.get_object ("menuitem5") as Gtk.MenuItem;
        menuitem6 = builder.get_object ("menuitem6") as Gtk.MenuItem;
        menuitem7 = builder.get_object ("menuitem7") as Gtk.MenuItem;
        menuitem9 = builder.get_object ("menuitem9") as Gtk.MenuItem;
        menuitem10 = builder.get_object ("menuitem10") as Gtk.MenuItem;
        menuitem11 = builder.get_object ("menuitem11") as Gtk.MenuItem;
        menuitem12 = builder.get_object ("menuitem12") as Gtk.MenuItem;
        menuitem14 = builder.get_object ("menuitem14") as Gtk.MenuItem;
        menuitem15 = builder.get_object ("menuitem15") as Gtk.MenuItem;        
        menuitem17 = builder.get_object ("menuitem17") as Gtk.MenuItem;        
        popup1 = builder.get_object ("popup1") as Gtk.Menu;        
        popup11 = builder.get_object ("popup11") as Gtk.MenuItem;        
        popup12 = builder.get_object ("popup12") as Gtk.MenuItem;        
        popup13 = builder.get_object ("popup13") as Gtk.MenuItem;        
        treeview2 = builder.get_object ("treeview2") as TreeView;
        treeview3 = builder.get_object ("treeview3") as TreeView;
        combobox1 = builder.get_object ("combobox1") as ComboBoxText;
        combobox8 = builder.get_object ("combobox8") as ComboBox;
        combobox4 = builder.get_object ("combobox4") as ComboBox;
        jahrListe = builder.get_object ("liststore1") as Gtk.ListStore;
        liststore4 = builder.get_object ("liststore4") as Gtk.ListStore;
        felderListe = builder.get_object("felderListe") as Gtk.ListStore;
        alignment1 = builder.get_object ("alignment1") as Gtk.Alignment;
        notebook1 = builder.get_object ("notebook1") as Notebook;
        label12 = builder.get_object ("label12") as Label;
        label13 = builder.get_object ("label13") as Label;
        label14 = builder.get_object ("label14") as Label;
        button5 = builder.get_object ("button5") as Button;
        button6 = builder.get_object ("button6") as Button;
        label1 = builder.get_object ("label1") as Label;
        label19 = builder.get_object ("label19") as Label;
        label21 = builder.get_object ("label21") as Label;
        label23 = builder.get_object ("label23") as Label;
        label25 = builder.get_object ("label25") as Label;
        frame1 = builder.get_object ("frame1") as Frame;
        clipboard = Gtk.Clipboard.get_default (Gdk.Display.get_default());
        //treeview2.get_selection selbst verbinden weil glade das nicht kann
        treeview2.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE);
        treeview3.set_model(aktionListe);
//        treeview3.insert_column_with_attributes(-1, _("ID"), new CellRendererText (), "text", 0);
        treeview3.insert_column_with_attributes(-1, _("Name"), new CellRendererText (), "text", 2);
        treeview3.insert_column_with_attributes(-1, _("Datum"), new CellRendererText (), "text", 3);
    
        //Ereignisse verbinden
        window2.destroy.connect(Gtk.main_quit);
        menuitem2.activate.connect(auswertungN);
        menuitem41.activate.connect(()=>{hilfe = new chilfe("Home.md");hilfe.window11.show();});
        menuitem42.activate.connect(info);
        menuitem5.activate.connect(Gtk.main_quit);
        menuitem6.activate.connect(erntejahrHinzufuegen);
        menuitem7.activate.connect(auswertung);
        menuitem9.activate.connect(mittelVerwalten);
        menuitem10.activate.connect(felderVerwalten);
        menuitem11.activate.connect(betriebAendern);
        menuitem12.activate.connect(auswertungK);
        menuitem14.activate.connect(()=>{mobilGeraet = new cmobilGeraet(); mobilGeraet.abgleichen();});
        menuitem15.activate.connect(()=>{mobilGeraet = new cmobilGeraet(); mobilGeraet.anlegen();});        
        menuitem17.activate.connect(()=>{einstellungen = new ceinstellungen();einstellungen.window10.show_all();einstellungen.aendern();});        
        combobox8.changed.connect(aktionAnlegen);
        treeview2.get_selection().changed.connect(this.aktionenLesen);
        treeview2.row_activated.connect(()=>{feldEdit = new cfeldEdit();
                                                feldEdit.window4.set_transient_for(this.window2);
                                                feldEdit.window4.show_all();
                                                laskDb.feldAendern(feldEdit.feldAendern(aktFeld));
                                                this.felderLesen();
                                            });
        treeview3.cursor_changed.connect(aktionWaehlen);
        treeview3.row_activated.connect(aktionAendern);
        treeview3.button_release_event.connect((event)=>{if (event.button == 3) {
                                                            popup1.popup (null, null, null, event.button, event.time);
                                                            }return false;
                                                        });
        popup11.activate.connect(aktionKopieren);
        popup12.activate.connect(aktionEinfuegen);
        popup13.activate.connect(aktionLoeschen);
        combobox1.changed.connect(erntejahrWaehlen);
        combobox4.changed.connect(filterAendern);
        button5.clicked.connect(aktionAendern);
        button6.clicked.connect(aktionLoeschen);
    }

    public void betriebLesen(){
    //Felder in felderListe eintragen
        this.combobox8.set_sensitive(false);
        if(laskDb.jahreLesen().length == 0){
            //Wenn kein Erntejahr da ist
            this.erntejahrHinzufuegen();
            this.betriebLesen();
        }else{
            foreach(feld f in laskDb.felderLesen(0)){
                if (f.id == 0) {betrieb = f;}
                this.window2.set_title("LASK - " + betrieb.name);
            }
            this.jahreLesen();
            this.filterLesen();
            this.einstellungenLesen();
        }
    }

     public void einstellungenLesen(){
    //Standartwerte laden
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 7 && m.name == "kosten"){
                config.kosten = m;
            }
        }
     }
   
    public void filterLesen(){
    //Fruchtarten in Filterliste eintragen
        Gtk.ListStore model;
        TreeIter iter;
        string tmp = "";
        this.combobox4.set_row_separator_func((model, iter) =>{
                                                       string content = "";
                                                       bool boole = false;
                                                       model.get(iter, 0, out content);
                                                       if(content == "nix"){boole = true;}
                                                       return boole;});
        foreach (mittel m in laskDb.mittelLesen(erntejahr)){
            if (m.art == 0){
                if (m.par3 in tmp){}
                else{
                    model = this.combobox4.get_model() as Gtk.ListStore;
                    model.insert_with_values(out iter, -1, 0, m.par3, 1, m.par3);
                    tmp = tmp + (m.par3);
                }
            }
        }
    }
    
    public void jahreLesen(){
    //Jahre in combobox1 eintragen
        int rc = 0;
        
        Gtk.ListStore liststore1 = combobox1.get_model() as Gtk.ListStore;
        //Combobox von liststore trennen, sonst viele Events und Crash
        combobox1.set_model(null);
        liststore1.clear();
        foreach (int jahr in laskDb.jahreLesen()){
            liststore1.insert_with_values(null, -1, 0, 0, 0, jahr.to_string(), -1);
            erntejahr = jahr;
            rc += 1;
        }
        combobox1.set_model(liststore1);
        combobox1.set_active(rc - 1);
    }
        
    public void felderLesen(string was = "Alles"){
    //Felder in felderListe eintragen wenn Filter passt
        TreeIter iter;
        
        this.combobox8.set_sensitive(false);
        if (was == "Alles"){this.combobox4.set_active(0);} 
        //Treeview von liststore trennen, sonst viele Events und Crash
        this.treeview2.set_model(null);
        this.felderListe.clear();
        foreach(feld f in laskDb.felderLesen(erntejahr)){
            if (was == f.art || was == "Alles"){
                this.felderListe.append (out iter);
                this.felderListe.set (iter, 0, f.id);
                this.felderListe.set (iter, 1, f.jahr);
                this.felderListe.set (iter, 2, f.name);
                this.felderListe.set (iter, 3, f.art);
                this.felderListe.set (iter, 4, f.groesse);
                this.felderListe.set (iter, 5, f.pacht);
                this.felderListe.set (iter, 6, f.dungn);
                this.felderListe.set (iter, 7, f.dungp);
                this.felderListe.set (iter, 8, f.dungk);
                this.felderListe.set (iter, 9, f.dungm);
                this.felderListe.set (iter, 10, f.dungs);
                this.felderListe.set (iter, 11, f.dungc);
                this.combobox8.set_sensitive(true);
            }else{
                foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                    if(f.art == "Feld" && a.aktion == "Saat" && a.par2 == was){
                        this.felderListe.append (out iter);
                        this.felderListe.set (iter, 0, f.id);
                        this.felderListe.set (iter, 1, f.jahr);
                        this.felderListe.set (iter, 2, f.name);
                        this.felderListe.set (iter, 3, f.art);
                        this.felderListe.set (iter, 4, f.groesse);
                        this.felderListe.set (iter, 5, f.pacht);
                        this.felderListe.set (iter, 6, f.dungn);
                        this.felderListe.set (iter, 7, f.dungp);
                        this.felderListe.set (iter, 8, f.dungk);
                        this.felderListe.set (iter, 9, f.dungm);
                        this.felderListe.set (iter, 10, f.dungs);
                        this.felderListe.set (iter, 11, f.dungc);
                        this.combobox8.set_sensitive(true);
                    }
                }
            }
        }
        this.treeview2.set_model(this.felderListe);
        err(_("Felder in felderListe eingetragen"), 0);
        //ersten Eintrag markieren
        this.treeview2.get_model().get_iter_first(out iter);
        this.treeview2.get_selection().select_iter(iter);
        feldInfo2();
    }
    
    
    public void aktionenLesen(){
    //Feld wurde ausgewählt
        TreeIter iter;
        this.label14.set_text("");
        this.label1.set_text("");
        this.button5.set_sensitive(false);
        this.button6.set_sensitive(false);
        if (this.frame1.get_child() != null){this.frame1.remove(this.frame1.get_child());}

        this.treeview3.set_model(null);
        this.aktionListe.clear();
        this.treeview3.set_model(this.aktionListe);
        feldInfo2();
        if (aktFelderListe.length != 1){
            this.label12.set_text(aktFelderListe.length.to_string() + _(" Felder"));
            this.label13.set_text(doubleparse(aktFeld.groesse) + " ha");
        }else if (aktFelderListe.length == 1){
            this.label12.set_text(aktFelderListe[0].name);
            this.label13.set_text(doubleparse(aktFelderListe[0].groesse) + " ha");
            //Wenn Weide dann Eintrag in combobox8
            if (aktFelderListe[0].art == "Grünland" && aktFelderListe[0].vorfrucht == "true"){
                if (combobox8.get_model().iter_n_children(null) == 7){
                    this.liststore4.insert_with_values(out iter, -1, 0, _("Weidegang"), 1, "Weidegang");
                }
            }else if (combobox8.get_model().iter_n_children(null) >= 8){
                combobox8.get_model().iter_nth_child(out iter, null, combobox8.get_model().iter_n_children(null) - 1);
                this.liststore4.remove(ref iter);
            }
            //Aktionen zum aktuellen Feld in aktionListe eintragen
            foreach (aktion a in laskDb.aktionLesen(aktFeld.id.to_string(), erntejahr)){
                this.aktionListe.append (out iter);
                this.aktionListe.set (iter, 0, a.id);
//Nächste Zeile macht evtl. Crash
//                this.aktionListe.set (iter, 1, felderliste[a.feld]);
                //Wegen Übersetzung
                if (a.aktion == "Saat"){
                    this.aktionListe.set (iter, 2, _("Saat") + " (" + a.par2 + ")");
                    this.label14.set_text(a.par2);
                    aktFeld.vorfrucht = a.par2;
                }else if (a.aktion == "Bodenbearbeitung"){
                    this.aktionListe.set (iter, 2, _("Bodenbearbeitung") + " (" + a.par1 + ")");
                }else if (a.aktion == "Mineralische Düngung"){
                    this.aktionListe.set (iter, 2, _("Mineralische Düngung") + " (" + a.par1 + ")");
                }else if (a.aktion == "Organische Düngung"){
                    this.aktionListe.set (iter, 2, _("Organische Düngung") + " (" + a.par1 + ")");
                }else if (a.aktion == "Pflanzenschutz"){
                    this.aktionListe.set (iter, 2, _("Pflanzenschutz"));
                }else if (a.aktion == "Ernte"){
                    this.aktionListe.set (iter, 2, _("Ernte"));
                }else if (a.aktion == "Weidegang"){
                    this.aktionListe.set (iter, 2, _("Weidegang"));
                }
                this.aktionListe.set (iter, 3, a.datum.format("%d.%m.%Y"));
                this.aktionListe.set (iter, 4, doubleparse(a.kosten[0]));
                this.aktionListe.set (iter, 5, a.par1);
                this.aktionListe.set (iter, 6, a.par2);
                this.aktionListe.set (iter, 7, a.par3);
                this.aktionListe.set (iter, 8, a.par4);
                this.aktionListe.set (iter, 9, a.par5);
                this.aktionListe.set (iter, 10, a.par6);
                this.aktionListe.set (iter, 11, a.par7);
                this.aktionListe.set (iter, 12, a.par8);
                this.aktionListe.set (iter, 13, a.par9);
                this.aktionListe.set (iter, 14, a.bbch);
                this.aktionListe.set (iter, 15, a.flaeche);
                this.aktionListe.set (iter, 16, a.kommentar);
                this.aktionListe.set (iter, 17, a.anwender);
            }
        }
        err(_("Aktionen in aktionListe eingetragen"), 0);
    }
    
    public void aktionWaehlen(){
    //Aktion wurde ausgewählt   
        string menge = "";
        Label kommentar;
        Grid table1 = new Grid();
        int i = 0;
        
        table1.set_column_homogeneous(false);
        table1.set_hexpand(false);
        table1.set_column_spacing(5);
        if (this.frame1.get_child() != null){this.frame1.remove(this.frame1.get_child());}
        this.frame1.add(table1);
        aktAktion = aktionInfo(aktFeld.id);
        if (aktAktion.id != -1){
            this.button5.set_sensitive(true);
            this.button6.set_sensitive(true);
            //Informationen zu Aktion anzeigen
            table1.attach(new Label("     "), 3, 0, 1, 9);
            table1.attach(new Label(aktAktion.anwender), 4, 0, 1, 1);
            table1.attach(new Label("     "), 4, 1, 1, 1);
            table1.get_child_at(4,0).set_halign(Align.START);
            if(aktAktion.kommentar != ""){
                table1.attach(new Label(_("Kommentar:")), 4, 2, 1, 1);
                table1.get_child_at(4, 2).set_halign(Align.START);
                kommentar = new Label(aktAktion.kommentar);
                kommentar.set_line_wrap(true);
                kommentar.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
                kommentar.set_width_chars(10);
                table1.attach(kommentar, 4, 3, 1, 5);
                table1.get_child_at(4, 3).set_halign(Align.START);
            }
            if (aktAktion.aktion == "Saat"){
            //Wenn Saat
                if (aktAktion.par3 < 0){
                    menge = doubleparse(0 - aktAktion.par3) + " kg/ha";
                }else{menge = doubleparse(aktAktion.par3) + _(" Körner/m²");} 
                this.frame1.set_label(_("Saat"));
                table1.attach(new Label(_("Fruchtart: ")), 0, 0, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                table1.attach(new Label(aktAktion.par2), 1, 0, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
                table1.attach(new Label(_("Sorte: ")), 0, 1, 1, 1);
                table1.get_child_at(0, 1).set_halign(Align.END);
                table1.attach(new Label(aktAktion.par1), 1, 1, 1, 1);
                table1.get_child_at(1, 1).set_halign(Align.START);
                table1.attach(new Label(_("Saatmenge: ")), 0, 2, 1, 1);
                table1.get_child_at(0, 2).set_halign(Align.END);
                table1.attach(new Label(menge), 1, 2, 1, 1);
                table1.get_child_at(1, 2).set_halign(Align.START);
                table1.attach(new Label(_("Hauptfrucht: ")), 0, 3, 1, 1);
                table1.get_child_at(0, 3).set_halign(Align.END);
                table1.attach(new Label(aktAktion.par4.to_string()), 1, 3, 1, 1);
                table1.get_child_at(1, 3).set_halign(Align.START);
                this.label19.set_text(aktAktion.par2);
                this.label21.set_text(aktAktion.par1);
                this.label23.set_text(menge);
            }else if (aktAktion.aktion == "Bodenbearbeitung"){
            //Wenn Bodenbearbeitung
                this.frame1.set_label(_("Bodenbearbeitung"));
                table1.attach(new Label(_("Gerät: ")), 0, 0, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                table1.attach(new Label(aktAktion.par1), 1, 0, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
            }else if (aktAktion.aktion == "Pflanzenschutz"){
           //Wenn Pflanzenschutz
                this.frame1.set_label(_("Pflanzenschutz"));
                foreach (psMittel psm in aktAktion.psMittel){
                    table1.attach(new Label(psm.mittel + ": "), 0, 0 + i, 1, 1);
                    table1.get_child_at(0, 0 + i).set_halign(Align.END);
                    table1.attach(new Label(doubleparse(psm.menge, 3) + " l/ha"), 1, 0 + i, 1, 1);
                    table1.get_child_at(1, 0 + i).set_halign(Align.START);
                    i += 1;
                 }
            }else if (aktAktion.aktion == "Organische Düngung" || aktAktion.aktion == "Mineralische Düngung"){
            //Wenn Düngung
                this.frame1.set_label(_("Düngung"));
                table1.attach(new Label(_("Dünger: ")), 0, 0, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                table1.attach(new Label(aktAktion.par1), 1, 0, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
                table1.attach(new Label(_("Menge: ")), 0, 1, 1, 1);
                table1.get_child_at(0, 1).set_halign(Align.END);
                table1.attach(new Label(doubleparse(aktAktion.par9) + " " + aktAktion.par2 + "/ha"), 1, 1, 1, 1);
                table1.get_child_at(1, 1).set_halign(Align.START);
            }else if (aktAktion.aktion == "Ernte"){
            //Wenn Ernte
                this.frame1.set_label(_("Ernte"));
                table1.attach(new Label(_("Fruchtart: ")), 0, 0, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                table1.attach(new Label(this.label14.get_text()), 1, 0, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
                table1.attach(new Label(_("Ertrag: ")), 0, 1, 1, 1);
                table1.get_child_at(0, 1).set_halign(Align.END);
                table1.attach(new Label(doubleparse(aktAktion.par3) + " dt/ha"), 1, 1, 1, 1);
                table1.get_child_at(1, 1).set_halign(Align.START);
                table1.attach(new Label(_("Feuchte: ")), 0, 2, 1, 1);
                table1.get_child_at(0, 2).set_halign(Align.END);
                table1.attach(new Label(doubleparse(aktAktion.par5) + " %"), 1, 2, 1, 1);
                table1.get_child_at(1, 2).set_halign(Align.START);
                table1.attach(new Label(_("Entzug je ha: ")), 0, 3, 1, 1);
                table1.get_child_at(0, 3).set_halign(Align.END);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[0]).to_string() + " kg N"), 1, 3, 1, 1);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[1]).to_string() + " kg P"), 1, 4, 1, 1);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[2]).to_string() + " kg K"), 1, 5, 1, 1);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[3]).to_string() + " kg M"), 1, 6, 1, 1);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[4]).to_string() + " kg S"), 1, 7, 1, 1);
                table1.attach(new Label(int.parse(aktAktion.par1.split(";")[5]).to_string() + " kg C"), 1, 8, 1, 1);
            }else if (aktAktion.aktion == "Weidegang"){
            //Wenn Ernte
                this.frame1.set_label(_("Weidegang"));
                table1.attach(new Label(_("GVE: ")), 0, 0, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                table1.attach(new Label(doubleparse(aktAktion.par3)), 1, 0, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
                table1.attach(new Label(_("Weidetage: ")), 0, 1, 1, 1);
                table1.get_child_at(0, 0).set_halign(Align.END);
                Date tmpDate1 = Date();
                Date tmpDate2 = Date();
                tmpDate1.set_dmy((DateDay)aktAktion.datum.get_day_of_month(), aktAktion.datum.get_month(), (DateYear)aktAktion.datum.get_year());
                tmpDate2.set_parse(aktAktion.par1);
                table1.attach(new Label(tmpDate1.days_between(tmpDate2).to_string() + " Tage"), 1, 1, 1, 1);
                table1.get_child_at(1, 0).set_halign(Align.START);
             }
            this.frame1.show_all();
        }
    }
    
    public void aktionLoeschen(){
    //Aktion löschen
        Dialog dialog = new Dialog.with_buttons(_("Frage"),
                                                    this.window2,Gtk.DialogFlags.MODAL,
                                                    _("Abbrechen"), Gtk.ResponseType.CANCEL,
                                                    _("OK"), Gtk.ResponseType.OK);
        dialog.get_content_area().add(new Label(_("Aktion '") + aktAktion.aktion + _("' wirklich Löschen? \nAlle Daten gehen verloren\n")));            
        dialog.show_all();
        
        if (dialog.run() == Gtk.ResponseType.OK) {
            laskDb.aktionEntfernen(aktAktion.id);
            this.aktionenLesen();         
            dialog.close();
        }else{dialog.close();}
    }

    public void erntejahrHinzufuegen(){
    //Erntejahr hinzufügen
        int i = 0;
        SpinButton spinbutton1 = new SpinButton.with_range(0,3000,1);
        CheckButton checkbutton1 = new CheckButton.with_label(_("Felder aus aktuellem Erntejahr übernehmen"));
        
        Dialog dialog = new Gtk.Dialog.with_buttons(_("Erntejahr hinzufuegen"), this.window2,
                                                    DialogFlags.MODAL,
                                                    _("Abbrechen"), Gtk.ResponseType.CANCEL,
                                                    _("Ok"), Gtk.ResponseType.OK);
        dialog.get_content_area().add(new Label(_("Wählen Sie das Erntejahr, dass Sie hinzufuegen möchten \n")));            
        dialog.get_content_area().add(spinbutton1);           
        dialog.get_content_area().add(checkbutton1);          
        spinbutton1.set_value(int.parse(new DateTime.now_local().format("%Y"))+1);
        checkbutton1.set_active(true);
        dialog.show_all();
        
        if (dialog.run() == ResponseType.OK) {
        //Wenn Jahr noch nicht existiert anlegen, sonst Fehlermeldung
            i = 0;
            foreach (int jahr in laskDb.jahreLesen()){
                if (jahr == int.parse(spinbutton1.get_text())){
                    i = 1;
                }
            }
            if (i != 1){
                laskDb.erntejahrHinzufuegen(int.parse(spinbutton1.get_text()));
                //Felder aus Vorjahr hinzufügen
                if (checkbutton1.get_active() == true){
                    foreach (feld f in laskDb.felderLesen(erntejahr)){
                        //Vorfrucht ermitteln
                        f.vorfrucht = "";
                        foreach (aktion a in laskDb.aktionLesen(f.id.to_string(), erntejahr)){
                            if (a.aktion == "Saat" && a.par4 == 1){
                                f.vorfrucht = a.par2;
                            }
                        }
                        f.jahr = int.parse(spinbutton1.get_text());
                        f.id = -1;
                        
                        laskDb.feldHinzufuegen(f);
                    }
                }
                this.jahreLesen();
                dialog.close();
            }else{
                dialog.close();
                var dialog2 = new MessageDialog(null,
                                                Gtk.DialogFlags.MODAL,
                                                Gtk.MessageType.INFO,
                                                Gtk.ButtonsType.OK,
                                                _("Dieses Erntejahr existiert bereits"));
                dialog2.run();
                dialog2.destroy();
                dialog.destroy();
  
            }
        }else{dialog.close();}
    }

    public void erntejahrWaehlen(){
    //Erntejahr wurde ausgewählt
        erntejahr=int.parse(this.combobox1.get_active_text());
        this.felderLesen();
        err(_("Erntejahr ") + erntejahr.to_string() +_(" wurde gewählt"), 0);
    }
    
    public void aktionAnlegen(){
    //Aktion anlegen Fenster
        TreeIter iter;
        GLib.Value val;
        if (this.combobox8.get_active() != 0){
            aktionEdit = new caktionEdit();
            aktionEdit.window6.set_transient_for(this.window2);
            aktionEdit.window6.show_all();
            this.combobox8.get_active_iter(out iter);
            this.combobox8.get_model().get_value(iter, 1, out val);
            this.combobox8.set_active(0);
            aktionEdit.aktionAnlegen(val.get_string());
        }
    }

    
    public void aktionAendern(){
    //Aktion ändern Knopf wurde gedrückt
        if (aktAktion.id > -1){
            //Es ist eine aktion ausgewählt
            aktionEdit = new caktionEdit();
            aktionEdit.window6.set_transient_for(this.window2);
            aktionEdit.window6.show_all();
            aktionEdit.aktionAendern(aktAktion);
            this.combobox8.set_active(0);
        }
    }

    public void betriebAendern(){
    //Betrieb ändern
        betriebEdit = new cbetriebEdit();
        betriebEdit.window3.set_transient_for(this.window2);
        betriebEdit.window3.show_all();
        betriebEdit.aendern();
    }
    
    
    
    
    
    
    
    public void info(){
	//Infodialog
        var dialog = new AboutDialog();
        string[] dank =  {_("openclipart.org für das Logo")
						 ,_("alle Entwickler von GTK3, Vala, cmake, ...")
						 ,_("Pavel Fric für tschechische Überstzung")
						 };
        
        dialog.set_version(config.LASK_VERSION);
        dialog.set_copyright(_("© 2019 Andreas Strasser"));
        dialog.set_logo_icon_name("lask");
        dialog.set_website("https://pages.codeberg.org/ibinshoid/lask.html");
        dialog.set_website_label("https://pages.codeberg.org/ibinshoid/lask.html");
        dialog.authors = {"Andreas Strasser"};
        dialog.add_credit_section(_("Vielen Dank an:"), dank);
        dialog.run();
        dialog.destroy();
    }

    public void mittelVerwalten(){
        //Mittel verwalten
        mittelFenster= new cmittelFenster();
        mittelFenster.window5.set_transient_for(this.window2);
        mittelFenster.window5.show_all();
        mittelFenster.combobox6_changed();
    }
 
    public void auswertungN(){
        //Auswertung NBilanz öffnen 
        auswertungNaehrstoff = new causwertungNaehrstoff();
        auswertungNaehrstoff.window7.set_transient_for(this.window2);
        auswertungNaehrstoff.window7.show_all();
    }
    
    public void auswertung(){
        //Auswertung öffnen
        auswertungUebersicht = new causwertungUebersicht();
        auswertungUebersicht.window7.set_transient_for(this.window2);
        auswertungUebersicht.window7.show_all();
    }
    
    public void auswertungK(){
        //Auswertung Kosten öffnen
        auswertungKosten = new causwertungKosten();
        auswertungKosten.window7.set_transient_for(this.window2);
        auswertungKosten.window7.show_all();
    }
    
    public void felderVerwalten(){
        //Felder bearbeiten
        felderFenster= new cfelderFenster();
        felderFenster.window5.set_transient_for(this.window2);
        felderFenster.window5.show_all();
        felderFenster.felderLaden();
    }
        
    public void filterAendern(){
        //Filter für Felderliste wurde gesetzt
        TreeIter iter;
        GLib.Value val;
        this.combobox4.get_active_iter(out iter);
        this.combobox4.get_model().get_value(iter, 1, out val);
        this.felderLesen(val.get_string());
    }

    public void aktionKopieren(){
        //aktion in Zwischenablage kopieren
        err("Kopiere " + aktAktion.aktion + " in Zwischenablage", 0);
        clipboard.set_text(aktAktion.to_string(), -1);
    }

    public void aktionEinfuegen(){
        //aktion aus Zwischenablage einfügen
        string zwa = clipboard.wait_for_text();
        if (zwa.split("¶").length == 20){
            aktion tmpAktion = aktion.from_string(zwa);
            tmpAktion.id = -1;
            foreach(feld a in aktFelderListe){
                tmpAktion.feld=a.id;
                tmpAktion.flaeche=a.groesse;
                laskDb.aktionHinzufuegen(tmpAktion, erntejahr);
            }
            err(tmpAktion.aktion + _(" aus Zwischenablage eingefügt"), 0);
            this.aktionenLesen();
        }else{
            err(_("Keine Aktion in der Zwischenablage"), 1);
        }
    }
}
